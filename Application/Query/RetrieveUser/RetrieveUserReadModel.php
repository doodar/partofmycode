<?php

namespace Eplane\Payment\Application\Query\RetrieveUser;

class RetrieveUserReadModel
{
    /**
     * @var string
     */
    private $userId;

    /**
     * @var bool
     */
    private $isActive;

    /**
     * @var bool
     */
    private $platformPay;

    /**
     * @var string
     */
    private $createdAt;

    /**
     * @var string
     */
    private $lastModifyAt;

    /**
     * @param string $userId
     * @param bool $isActive
     * @param bool $platformPay
     * @param string $createdAt
     * @param string $lastModifyAt
     */
    public function __construct(
        string $userId,
        bool $isActive,
        bool $platformPay,
        string $createdAt,
        string $lastModifyAt
    ) {
        $this->userId       = $userId;
        $this->isActive     = $isActive;
        $this->platformPay  = $platformPay;
        $this->createdAt    = $createdAt;
        $this->lastModifyAt = $lastModifyAt;
    }

    /**
     * @return string
     */
    public function getUserId(): string
    {
        return $this->userId;
    }

    /**
     * @return bool
     */
    public function isActive(): bool
    {
        return $this->isActive;
    }

    /**
     * @return bool
     */
    public function isPlatformPay(): bool
    {
        return $this->platformPay;
    }

    /**
     * @return string
     */
    public function getCreatedAt(): string
    {
        return $this->createdAt;
    }

    /**
     * @return string
     */
    public function getLastModifyAt(): string
    {
        return $this->lastModifyAt;
    }
}
