<?php

namespace Eplane\Payment\Application\Query\RetrieveUser;

interface RetrieveUserHandlerInterface
{
    /**
     * @param RetrieveUserQuery $query
     * @return RetrieveUserReadModel
     */
    public function handle($query);

}
