<?php

namespace Eplane\Payment\Application\Query\RetrieveUser;

use Eplane\Payment\Application\Exception\QueryInvalidParameters;

class RetrieveUserQuery
{
    /**
     * @var string
     */
    private $userId;

    /**
     * @param string $userId
     * @throws QueryInvalidParameters
     */
    public function __construct(string $userId)
    {
        if(empty($userId)){
            throw new QueryInvalidParameters(
                sprintf('Parameters passed to %s is invalid', self::class)
            );
        }

        $this->userId = $userId;
    }

    /**
     * @return string
     */
    public function getUserId(): string
    {
        return $this->userId;
    }

}
