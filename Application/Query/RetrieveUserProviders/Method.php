<?php

namespace Eplane\Payment\Application\Query\RetrieveUserProviders;



class Method
{
    /**
     * @var string
     */
    protected $code;

    /**
     * @var string
     */
    protected $title;

    /**
     * @var bool
     */
    protected $is_assigned;

    /**
     * @param string $code
     * @param string $title
     * @param bool $is_assigned
     */
    public function __construct(string $code, string $title, bool $is_assigned)
    {
        $this->code = $code;
        $this->title = $title;
        $this->is_assigned = $is_assigned;
    }

    /**
     * @return bool
     */
    public function isIsAssigned(): bool
    {
        return $this->is_assigned;
    }


    /**
     * @return string
     */
    public function getCode(): string
    {
        return $this->code;
    }

    /**
     * @return string
     */
    public function getTitle(): string
    {
        return $this->title;
    }
}
