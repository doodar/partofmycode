<?php
namespace Eplane\Payment\Application\Query\RetrieveUserProviders;

use Eplane\Payment\Domain\ValueObject\Fee;

class RetrieveUserProvidersItemReadModel
{
    /**
     * @var string
     */
    protected $code;

    /**
     * @var string
     */
    protected $title;

    /**
     * @var Fee
     */
    protected $fee;

    /**
     * @var Fee
     */
    protected $defaultFee;

    /**
     * @var bool
     */
    protected $connected;

    /**
     * @var string
     */
    protected $connectLink;

    /**
     * @var string
     */
    protected $lastModify;

    /**
     * @var Method[]
     */
    protected $methods;


    /**
     * @param string $code
     * @param string $title
     * @param Fee|null $fee
     * @param Fee $defaultFee
     * @param bool $connected
     * @param string|null $connectLink
     * @param string|null $lastModify
     * @param Method[]|null $methods
     */
    public function __construct(
        string $code,
        string $title,
        $fee,
        $defaultFee,
        bool $connected,
        $connectLink,
        $lastModify,
        $methods
    ) {
        $this->code        = $code;
        $this->title       = $title;
        $this->fee         = $fee;
        $this->defaultFee  = $defaultFee;
        $this->connected   = $connected;
        $this->connectLink = $connectLink;
        $this->lastModify  = $lastModify;
        $this->methods  = $methods;
    }

    /**
     * @return string
     */
    public function getCode(): string
    {
        return $this->code;
    }

    /**
     * @return string
     */
    public function getTitle(): string
    {
        return $this->title;
    }

    /**
     * @return Fee|null
     */
    public function getFee()
    {
        return $this->fee;
    }

    /**
     * @return Fee
     */
    public function getDefaultFee()
    {
        return $this->defaultFee;
    }

    /**
     * @return bool
     */
    public function isConnected(): bool
    {
        return $this->connected;
    }

    /**
     * @return string|null
     */
    public function getConnectLink()
    {
        return $this->connectLink;
    }

    /**
     * @return string|null
     */
    public function getLastModify()
    {
        return $this->lastModify;
    }

    /**
     * @return Method[]|null
     */
    public function getMethods()
    {
        return $this->methods;
    }
}
