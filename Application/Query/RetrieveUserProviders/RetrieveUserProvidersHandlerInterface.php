<?php

namespace Eplane\Payment\Application\Query\RetrieveUserProviders;

use Eplane\Ddd\Application\QueryHandlerInterface;

interface RetrieveUserProvidersHandlerInterface extends QueryHandlerInterface
{
    /**
     * @param RetrieveUserProvidersQuery $query
     * @return RetrieveUserProvidersItemReadModel[]
     */
    public function handle($query);
}
