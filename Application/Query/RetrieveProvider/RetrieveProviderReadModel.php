<?php
namespace Eplane\Payment\Application\Query\RetrieveProvider;

use Eplane\Payment\Application\Query\RetrieveAllProviders\Fee;

class RetrieveProviderReadModel
{
    /**
     * @var string
     */
    protected $code;

    /**
     * @var string
     */
    protected $title;

    /**
     * @var bool
     */
    protected $enabled;

    /**
     * @var Fee
     */
    protected $defaultFee;

    /**
     * @param string $code
     * @param string $title
     * @param bool $enabled
     * @param Fee $defaultFee
     */
    public function __construct(string $code, string $title, bool $enabled, Fee $defaultFee)
    {
        $this->code = $code;
        $this->title = $title;
        $this->enabled = $enabled;
        $this->defaultFee = $defaultFee;
    }


    /**
     * @return string
     */
    public function getCode(): string
    {
        return $this->code;
    }

    /**
     * @return string
     */
    public function getTitle(): string
    {
        return $this->title;
    }

    /**
     * @return bool
     */
    public function getEnabled(): bool
    {
        return $this->enabled ?? false;
    }

    /**
     * @return Fee
     */
    public function getDefaultFee(): Fee
    {
        return $this->defaultFee;
    }
}
