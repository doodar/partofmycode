<?php
namespace Eplane\Payment\Application\Query\RetrieveProvider;

use Eplane\Ddd\Application\QueryHandlerInterface;

interface RetrieveProviderHandlerInterface extends QueryHandlerInterface
{
    /**
     * @param RetrieveProviderQuery $query
     * @return RetrieveProviderReadModel
     */
    public function handle($query);
}
