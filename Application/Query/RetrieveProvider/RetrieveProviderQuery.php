<?php
namespace Eplane\Payment\Application\Query\RetrieveProvider;

use Eplane\Payment\Application\Exception\QueryInvalidParameters;

class RetrieveProviderQuery
{
    /**
     * @var string
     */
    private $providerId;

    /**
     * @param string $providerId
     * @throws QueryInvalidParameters
     */
    public function __construct(
        string $providerId
    ) {
        if(empty($providerId)){
            throw new QueryInvalidParameters(
                sprintf('Parameters passed to %s is invalid', self::class)
            );
        }

        $this->providerId = $providerId;
    }

    /**
     * @return string
     */
    public function getProviderId(): string
    {
        return $this->providerId;
    }
}
