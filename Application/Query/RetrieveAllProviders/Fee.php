<?php

namespace Eplane\Payment\Application\Query\RetrieveAllProviders;

class Fee
{
    private $amount;

    private $currency;

    private $isPercent;

    /**
     * @param string $amount
     * @param bool $isPercent
     * @param string $currency
     */
    public function __construct(string $amount, bool $isPercent, string $currency)
    {
        $this->amount    = $amount;
        $this->isPercent = $isPercent;
        $this->currency  = $currency;
    }

    public function getAmount(): string
    {
        return $this->amount;
    }

    public function getCurrency(): string
    {
        return $this->currency;
    }

    public function getIsPercent(): bool
    {
        return $this->isPercent;
    }
}
