<?php

namespace Eplane\Payment\Application\Query\RetrieveAllProviders;



use Eplane\RestV2\Model\Data\Response\Me;

class RetrieveAllProvidersItemReadModel
{
    /**
     * @var string
     */
    protected $code;

    /**
     * @var string
     */
    protected $title;

    /**
     * @var bool
     */
    protected $enabled;

    /**
     * @var Fee
     */
    protected $defaultFee = null;

    /**
     * @var Method[]
     */
    protected $methods;
    
    

    /**
     * @param string $code
     * @param string $title
     * @param bool $enabled
     * @param Fee $defaultFee
     * @param Method[] $methods
     */
    public function __construct(string $code, string $title, bool $enabled, Fee $defaultFee, array $methods)
    {
        $this->code = $code;
        $this->title = $title;
        $this->enabled = $enabled;
        $this->defaultFee = $defaultFee;
        $this->methods = $methods;
    }


    /**
     * @return string
     */
    public function getCode(): string
    {
        return $this->code;
    }

    /**
     * @return string
     */
    public function getTitle(): string
    {
        return $this->title;
    }

    /**
     * @return bool
     */
    public function getEnabled(): bool
    {
        return $this->enabled ?? false;
    }

    /**
     * @return Fee
     */
    public function getDefaultFee(): Fee
    {
        return $this->defaultFee;
    }

    /**
     * @return Method[]
     */
    public function getMethods(): array
    {
        return $this->methods;
    }
}
