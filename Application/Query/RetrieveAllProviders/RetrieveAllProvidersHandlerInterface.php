<?php

namespace Eplane\Payment\Application\Query\RetrieveAllProviders;

use Eplane\Ddd\Application\QueryHandlerInterface;

interface RetrieveAllProvidersHandlerInterface extends QueryHandlerInterface
{
    /**
     * @return RetrieveAllProvidersItemReadModel[]
     */
    public function handle($query = null);
}
