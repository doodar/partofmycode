<?php

namespace Eplane\Payment\Application\Query\RetrieveAllProviders;



class Method
{
    /**
     * @var string
     */
    protected $code;

    /**
     * @var string
     */
    protected $title;

    /**
     * @var bool
     */
    protected $enabled;
    

    /**
     * @param string $code
     * @param string $title
     * @param bool $enabled
     * @param Fee $defaultFee
     */
    public function __construct(string $code, string $title, bool $enabled)
    {
        $this->code = $code;
        $this->title = $title;
        $this->enabled = $enabled;
    }


    /**
     * @return string
     */
    public function getCode(): string
    {
        return $this->code;
    }

    /**
     * @return string
     */
    public function getTitle(): string
    {
        return $this->title;
    }

    /**
     * @return bool
     */
    public function getEnabled(): bool
    {
        return $this->enabled ?? false;
    }
}
