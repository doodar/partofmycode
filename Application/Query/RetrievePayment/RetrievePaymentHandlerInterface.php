<?php

namespace Eplane\Payment\Application\Query\RetrievePayment;

use Eplane\Ddd\Application\QueryHandlerInterface;

interface RetrievePaymentHandlerInterface extends QueryHandlerInterface
{
    /**
     * @param RetrievePaymentQuery $query
     * @return RetrievePaymentReadModel
     */
    public function handle($query);

}
