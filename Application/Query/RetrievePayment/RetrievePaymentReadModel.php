<?php

namespace Eplane\Payment\Application\Query\RetrievePayment;

use Eplane\Base\Php\BigNumber\Decimal;

class RetrievePaymentReadModel
{
    /**
     * @var string
     */
    private $id;
    /**
     * @var string
     */
    private $orderId;
    /**
     * @var string
     */
    private $requestId;
    /**
     * @var string
     */
    private $sellerId;
    /**
     * @var string
     */
    private $buyerId;
    /**
     * @var string
     */
    private $customerId;
    /**
     * @var string
     */
    private $chatMessageId;
    /**
     * @var Decimal
     */
    private $price;
    /**
     * @var float
     */
    private $fee;
    /**
     * @var string
     */
    private $providerCode;
    /**
     * @var string
     */
    private $methodCode;
    /**
     * @var string
     */
    private $paymentData;
    /**
     * @var string
     */
    private $currency;
    /**
     * @var bool
     */
    private $isPlatform;

    /**
     * RetrievePaymentReadModel constructor.
     * @param string $id
     * @param string $orderId
     * @param string $requestId
     * @param string $sellerId
     * @param string $buyerId
     * @param string $customerId
     * @param string $chatMessageId
     * @param Decimal $price
     * @param float $fee
     * @param string $providerCode
     * @param string $methodCode
     * @param string $paymentData
     * @param string $currency
     * @param bool $isPlatform
     */
    public function __construct(
        string $id,
        string $orderId,
        string $requestId,
        string $sellerId,
        string $buyerId,
        string $customerId,
        string $chatMessageId,
        Decimal $price,
        float $fee,
        string $providerCode,
        string $methodCode,
        string $paymentData,
        string $currency,
        bool $isPlatform = false
    )
    {
        $this->id = $id;
        $this->orderId = $orderId;
        $this->requestId = $requestId;
        $this->sellerId = $sellerId;
        $this->buyerId = $buyerId;
        $this->customerId = $customerId;
        $this->chatMessageId = $chatMessageId;
        $this->price = $price;
        $this->fee = $fee;
        $this->providerCode = $providerCode;
        $this->methodCode = $methodCode;
        $this->paymentData = $paymentData;
        $this->currency = $currency;
        $this->isPlatform = $isPlatform;
    }

    /**
     * @return string
     */
    public function getId(): string
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getOrderId(): string
    {
        return $this->orderId;
    }

    /**
     * @return string
     */
    public function getRequestId(): string
    {
        return $this->requestId;
    }

    /**
     * @return string
     */
    public function getSellerId(): string
    {
        return $this->sellerId;
    }

    /**
     * @return string
     */
    public function getBuyerId(): string
    {
        return $this->buyerId;
    }

    /**
     * @return string
     */
    public function getCustomerId(): string
    {
        return $this->customerId;
    }

    /**
     * @return string
     */
    public function getChatMessageId(): string
    {
        return $this->chatMessageId;
    }

    /**
     * @return Decimal
     */
    public function getPrice(): Decimal
    {
        return $this->price;
    }

    /**
     * @return float
     */
    public function getFee(): float
    {
        return (float)$this->fee;
    }

    /**
     * @return string
     */
    public function getProviderCode(): string
    {
        return $this->providerCode;
    }

    /**
     * @return string
     */
    public function getMethodCode(): string
    {
        return $this->methodCode;
    }

    /**
     * @return string
     */
    public function getPaymentData(): string
    {
        return $this->paymentData;
    }

    /**
     * @return string
     */
    public function getCurrency(): string
    {
        return $this->currency;
    }

    /**
     * @return bool
     */
    public function getIsPlatform(): bool
    {
        return $this->isPlatform;
    }
}
