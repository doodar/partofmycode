<?php

namespace Eplane\Payment\Application\Query\RetrievePayment;

use Eplane\Payment\Application\Exception\QueryInvalidParameters;

class RetrievePaymentQuery
{
    /**
     * @var string
     */
    private $paymentId;

    /**
     * @param string $paymentId
     * @throws QueryInvalidParameters
     */
    public function __construct(string $paymentId)
    {
        if(empty($paymentId)){
            throw new QueryInvalidParameters(
                sprintf('Parameters passed to %s is invalid', self::class)
            );
        }

        $this->paymentId = $paymentId;
    }

    /**
     * @return string
     */
    public function getPaymentId(): string
    {
        return $this->paymentId;
    }

}
