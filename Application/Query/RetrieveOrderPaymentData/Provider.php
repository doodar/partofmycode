<?php

namespace Eplane\Payment\Application\Query\RetrieveOrderPaymentData;

use Eplane\Payment\Domain\ValueObject\Fee;

class Provider
{
    /**
     * @var string
     */
    protected $code;

    /**
     * @var string
     */
    protected $title;

    /**
     * @var Fee
     */
    protected $fee;

    /**
     * @var Method[]
     */
    protected $methods;

    /**
     * Provider constructor.
     * @param string $code
     * @param string $title
     * @param Fee $fee
     * @param Method[] $methods
     */
    public function __construct(string $code, string $title, Fee $fee, array $methods)
    {
        $this->code    = $code;
        $this->title   = $title;
        $this->fee     = $fee;
        $this->methods = $methods;
    }

    /**
     * @return string
     */
    public function getCode(): string
    {
        return $this->code;
    }

    /**
     * @return string
     */
    public function getTitle(): string
    {
        return $this->title;
    }

    /**
     * @return Fee
     */
    public function getFee(): Fee
    {
        return $this->fee;
    }

    /**
     * @return Method[]
     */
    public function getMethods(): array
    {
        return $this->methods;
    }
}
