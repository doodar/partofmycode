<?php
namespace Eplane\Payment\Application\Query\RetrieveOrderPaymentData;

use Eplane\Payment\Domain\ValueObject\Fee;

class RetrieveOrderPaymentDataReadModel
{
    /**
     * @var Provider[]
     */
    protected $providers;

    /**
     * @param Provider[] $providers
     */
    public function __construct(array $providers)
    {
        $this->providers = $providers;
    }

    /**
     * @return Provider[]
     */
    public function getProviders(): array
    {
        return $this->providers;
    }
}
