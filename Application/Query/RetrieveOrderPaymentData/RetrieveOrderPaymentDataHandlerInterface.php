<?php

namespace Eplane\Payment\Application\Query\RetrieveOrderPaymentData;

use Eplane\Ddd\Application\QueryHandlerInterface;

interface RetrieveOrderPaymentDataHandlerInterface extends QueryHandlerInterface
{
    /**
     * @param RetrieveOrderPaymentDataQuery $query
     * @return RetrieveOrderPaymentDataReadModel
     */
    public function handle($query);
}
