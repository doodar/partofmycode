<?php

namespace Eplane\Payment\Application\Query\RetrieveOrderPaymentData;



class Method
{
    /**
     * @var string
     */
    protected $code;

    /**
     * @var string
     */
    protected $title;

    /**
     * @param string $code
     * @param string $title
     */
    public function __construct(string $code, string $title)
    {
        $this->code = $code;
        $this->title = $title;
    }


    /**
     * @return string
     */
    public function getCode(): string
    {
        return $this->code;
    }

    /**
     * @return string
     */
    public function getTitle(): string
    {
        return $this->title;
    }
}
