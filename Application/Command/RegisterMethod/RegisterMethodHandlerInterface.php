<?php

namespace Eplane\Payment\Application\Command\RegisterMethod;

use Eplane\Ddd\Application\CommandHandlerInterface;

interface RegisterMethodHandlerInterface extends CommandHandlerInterface
{
    /**
     * @param RegisterMethodCommand $command
     * @return bool
     */
    public function handle($command);
}
