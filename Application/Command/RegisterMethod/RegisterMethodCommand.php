<?php

namespace Eplane\Payment\Application\Command\RegisterMethod;

use Eplane\Payment\Application\Exception\CommandInvalidParameters;
use Eplane\Payment\Domain\ValueObject\Fee;

class RegisterMethodCommand
{
    /** @var string */
    private $code;

    /** @var string */
    private $title;

    /** @var string */
    private $providerCode;

    /** @var bool */
    private $enabled;

    /**
     * @param string $code
     * @param string $title
     * @param string $providerId
     * @param bool $enabled
     * @throws \Exception
     */
    public function __construct(string $code, string $title, string $providerCode, bool $enabled)
    {
        if(empty($code) || empty($providerCode)){
            throw new CommandInvalidParameters(
                sprintf('Parameters passed to %s is invalid', self::class)
            );
        }

        $this->code = $code;
        $this->title = $title;
        $this->providerCode = $providerCode;
        $this->enabled = $enabled;
    }

    /**
     * @return string
     */
    public function getCode(): string
    {
        return $this->code;
    }

    /**
     * @return string
     */
    public function getTitle(): string
    {
        return $this->title;
    }

    /**
     * @return string
     */
    public function getProviderCode(): string
    {
        return $this->providerCode;
    }

    /**
     * @return bool
     */
    public function isEnabled(): bool
    {
        return $this->enabled;
    }
}
