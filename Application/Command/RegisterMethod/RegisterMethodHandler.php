<?php

namespace Eplane\Payment\Application\Command\RegisterMethod;

use Eplane\Payment\Domain\Model\Method\Exception\NoSuchMethodException;
use Eplane\Payment\Domain\Model\Method\Method;
use Eplane\Payment\Domain\Model\Method\MethodRepositoryInterface;
use Eplane\Payment\Domain\Model\Provider\ProviderRepositoryInterface;
use Eplane\Ddd\Application\Service\TransactionInterface;

class RegisterMethodHandler implements RegisterMethodHandlerInterface
{
    /**
     * @var MethodRepositoryInterface
     */
    private $methodRepository;

    /**
     * @var ProviderRepositoryInterface
     */
    private $providerRepository;

    /**
     * @var TransactionInterface
     */
    private $transaction;

    public function __construct(
        MethodRepositoryInterface $methodRepository,
        ProviderRepositoryInterface $providerRepository,
        TransactionInterface $transaction
    ) {
        $this->methodRepository = $methodRepository;
        $this->providerRepository = $providerRepository;
        $this->transaction = $transaction;
    }

    /**
     * @param RegisterMethodCommand $command
     * @return string $methodId
     * @throws \Exception
     */
    public function handle($command)
    {
        $provider = $this->providerRepository->getByCode($command->getProviderCode());

        try {
            $method = $this->methodRepository->getByProviderAndCode(
                $provider->getId(),
                $command->getCode()
            );

            $methodId = $method->getId();

            $method->changeTitle($command->getTitle());
            $command->isEnabled() ? $method->enable() : $method->disable();

            $save = function ($method){
                $this->methodRepository->save($method);
            };
        } catch (NoSuchMethodException $e) {
            $methodId = $this->methodRepository->nextIdentity();

            $method = new Method(
                $methodId,
                $command->getCode(),
                $command->getTitle(),
                $provider->getId()
            );

            $command->isEnabled() ? $method->enable() : $method->disable();

            $save = function ($method){
                $this->methodRepository->add($method);
            };
        }

        $this->transaction->execute(
            function() use ($save, $method) {
                $save($method);
            }
        );

        return $methodId->getValue();
    }
}
