<?php

namespace Eplane\Payment\Application\Command\AssignMethodToUser;

use Eplane\Ddd\Application\CommandHandlerInterface;

interface AssignMethodToUserHandlerInterface extends CommandHandlerInterface
{
    /**
     * @param AssignMethodToUserCommand $command
     * @return bool
     */
    public function handle($command);
}
