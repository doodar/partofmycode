<?php

namespace Eplane\Payment\Application\Command\ChangeUserProviderFee;

use Eplane\Payment\Application\Exception\CommandInvalidParameters;
use Eplane\Payment\Domain\Model\User\UserId;

class ChangeUserProviderFeeCommand
{
    /** @var float */
    private $amount;

    /** @var bool */
    private $isPercent;

    /** @var string */
    private $currency;

    /** @var string */
    private $providerCode;

    /** @var UserId */
    private $userId;

    /**
     * @param string $providerCode
     * @param string $userId
     * @param float $amount
     * @param bool $isPercent
     * @param string $currency
     * @throws CommandInvalidParameters
     */
    public function __construct(string $providerCode, string $userId, float $amount, bool $isPercent, string $currency)
    {
        if (empty($providerCode) || empty($userId) || empty($currency)) {
            throw new CommandInvalidParameters(
                sprintf('Parameters passed to %s is invalid', self::class)
            );
        }

        $this->providerCode = $providerCode;
        $this->userId = $userId;
        $this->amount = $amount;
        $this->isPercent = $isPercent;
        $this->currency = $currency;
    }

    public function getProviderCode(): string
    {
        return $this->providerCode;
    }

    public function getUserId(): string
    {
        return $this->userId;
    }

    public function getAmount(): float
    {
        return $this->amount;
    }

    public function getIsPercent(): bool
    {
        return $this->isPercent;
    }

    public function getCurrency(): string
    {
        return $this->currency;
    }
}
