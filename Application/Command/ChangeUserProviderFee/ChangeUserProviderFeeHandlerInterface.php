<?php

namespace Eplane\Payment\Application\Command\ChangeUserProviderFee;

use Eplane\Ddd\Application\CommandHandlerInterface;

interface ChangeUserProviderFeeHandlerInterface extends CommandHandlerInterface
{
    /**
     * @param ChangeUserProviderFeeCommand $command
     * @return bool
     */
    public function handle($command);
}
