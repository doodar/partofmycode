<?php

namespace Eplane\Payment\Application\Command\RegisterProvider;

use Eplane\Payment\Application\Exception\CommandInvalidParameters;

class RegisterProviderCommand
{
    /** @var float */
    private $feeAmount;

    /** @var bool */
    private $feeIsPercent;

    /** @var string */
    private $feeCurrency;

    /** @var string */
    private $code;

    /** @var string */
    private $title;

    /** @var bool */
    private $enabled;

    /**
     * @param string $code
     * @param string $title
     * @param bool $enabled
     * @param float $feeAmount
     * @param bool $feeIsPercent
     * @param string $feeCurrency
     * @throws CommandInvalidParameters
     */
    public function __construct(string $code, string $title, bool $enabled, float $feeAmount, bool $feeIsPercent, string $feeCurrency)
    {
        if(empty($code)){
            throw new CommandInvalidParameters(
                sprintf('Parameters passed to %s is invalid', self::class)
            );
        }

        $this->code = $code;
        $this->title = $title;
        $this->enabled = $enabled;
        $this->feeAmount = $feeAmount;
        $this->feeIsPercent = $feeIsPercent;
        $this->feeCurrency = $feeCurrency;
    }

    /**
     * @return string
     */
    public function getCode(): string
    {
        return $this->code;
    }

    /**
     * @return string
     */
    public function getTitle(): string
    {
        return $this->title;
    }

    /**
     * @return bool
     */
    public function isEnabled(): bool
    {
        return $this->enabled;
    }

    /**
     * @return float
     */
    public function getFeeAmount(): float
    {
        return $this->feeAmount;
    }

    /**
     * @return bool
     */
    public function getFeeIsPercent(): bool
    {
        return $this->feeIsPercent;
    }

    /**
     * @return string
     */
    public function getFeeCurrency(): string
    {
        return $this->feeCurrency;
    }
}
