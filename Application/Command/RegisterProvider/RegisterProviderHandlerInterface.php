<?php

namespace Eplane\Payment\Application\Command\RegisterProvider;

use Eplane\Ddd\Application\CommandHandlerInterface;

interface RegisterProviderHandlerInterface extends CommandHandlerInterface
{
    /**
     * @param RegisterProviderCommand $command
     * @return bool
     */
    public function handle($command);
}
