<?php

namespace Eplane\Payment\Application\Command\RegisterProvider;

use Eplane\Ddd\Application\Service\TransactionInterface;
use Eplane\Payment\Domain\Model\Provider\Exception\NoSuchProviderException;
use Eplane\Payment\Domain\Model\Provider\Provider;
use Eplane\Payment\Domain\Model\Provider\ProviderRepositoryInterface;
use Eplane\Payment\Domain\ValueObject\Fee;

class RegisterProviderHandler implements RegisterProviderHandlerInterface
{
    /**
     * @var ProviderRepositoryInterface
     */
    private $providerRepository;

    /**
     * @var TransactionInterface
     */
    private $transaction;

    public function __construct(
        ProviderRepositoryInterface $providerRepository,
        TransactionInterface $transaction
    ) {
        $this->providerRepository = $providerRepository;
        $this->transaction = $transaction;
    }

    /**
     * @param RegisterProviderCommand $command
     * @return string $providerId
     * @throws \Exception
     */
    public function handle($command)
    {
        try{
            $provider = $this->providerRepository->getByCode($command->getCode());

            $providerId = $provider->getId();

            $provider->changeTitle($command->getTitle());
            $provider->changeDefaultFee(new Fee(
                $command->getFeeAmount(),
                $command->getFeeIsPercent(),
                $command->getFeeCurrency()
            ));

            if ($command->isEnabled() !== $provider->isEnabled()) {
                $command->isEnabled() ? $provider->enable() : $provider->disable();
            }

            $save = function ($provider){
                $this->providerRepository->save($provider);
            };
        } catch (NoSuchProviderException $e) {
            $providerId = $this->providerRepository->nextIdentity();

            $provider = new Provider(
                $providerId,
                $command->getCode(),
                $command->getTitle(),
                $command->isEnabled(),
                new Fee(
                    $command->getFeeAmount(),
                    $command->getFeeIsPercent(),
                    $command->getFeeCurrency()
                )
            );

            $save = function ($provider){
                $this->providerRepository->add($provider);
            };
        }

        $this->transaction->execute(
            function() use ($save, $provider) {
                $save($provider);
            }
        );

        return $providerId->getValue();
    }
}
