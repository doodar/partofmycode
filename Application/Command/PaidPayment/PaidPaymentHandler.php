<?php

namespace Eplane\Payment\Application\Command\PaidPayment;

use Eplane\Ddd\Application\Service\TransactionInterface;
use Eplane\Ddd\Domain\Event\EventDispatcherInterface;
use Eplane\Payment\Domain\Model\Payment\PaymentId;
use Eplane\Payment\Domain\Model\Payment\PaymentRepositoryInterface;

class PaidPaymentHandler implements PaidPaymentHandlerInterface
{

    /**
     * @var PaymentRepositoryInterface
     */
    private $paymentRepository;
    /**
     * @var TransactionInterface
     */
    private $transaction;
    /**
     * @var EventDispatcherInterface
     */
    private $eventDispatcher;

    public function __construct(
        PaymentRepositoryInterface $paymentRepository,
        TransactionInterface $transaction,
        EventDispatcherInterface $eventDispatcher
    )
    {
        $this->paymentRepository = $paymentRepository;
        $this->transaction       = $transaction;
        $this->eventDispatcher   = $eventDispatcher;
    }

    /**
     * @param PaidPaymentCommand $command
     * @return mixed|void
     * @throws \Exception
     */
    public function handle($command)
    {
        $payment = $this->paymentRepository->get(new PaymentId($command->getPaymentId()));
        $payment->paid();

        $this->transaction->execute(
            function () use ($payment) {
                $this->paymentRepository->save($payment);
                $this->eventDispatcher->dispatch($payment->releaseEvents());
            }
        );
    }
}
