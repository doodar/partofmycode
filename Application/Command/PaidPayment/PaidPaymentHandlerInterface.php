<?php

namespace Eplane\Payment\Application\Command\PaidPayment;

use Eplane\Ddd\Application\CommandHandlerInterface;

interface PaidPaymentHandlerInterface extends CommandHandlerInterface
{

    /**
     * @param PaidPaymentCommand $command
     * @return mixed
     */
    public function handle($command);
}
