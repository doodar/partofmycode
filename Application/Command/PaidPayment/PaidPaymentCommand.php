<?php

namespace Eplane\Payment\Application\Command\PaidPayment;

use Eplane\Payment\Application\Exception\CommandInvalidParameters;

class PaidPaymentCommand
{
    /**
     * @var string
     */
    private $paymentId;

    /**
     * PaidPaymentCommand constructor.
     * @param string $paymentId
     * @throws CommandInvalidParameters
     */
    public function __construct(string $paymentId)
    {
        if(empty($paymentId)) {
            throw new CommandInvalidParameters(
                sprintf('Parameters passed to %s is invalid', self::class)
            );
        }

        $this->paymentId = $paymentId;
    }

    /**
     * @return string
     */
    public function getPaymentId(): string
    {
        return $this->paymentId;
    }
}
