<?php

namespace Eplane\Payment\Application\Command\ResetUserProviderFeeToDefault;

use Eplane\Ddd\Application\Service\TransactionInterface;
use Eplane\Ddd\Domain\Event\EventDispatcherInterface;
use Eplane\Payment\Domain\Model\Provider\ProviderRepositoryInterface;
use Eplane\Payment\Domain\Model\User\Exception\NoSuchUserException;
use Eplane\Payment\Domain\Model\User\User;
use Eplane\Payment\Domain\Model\User\UserId;
use Eplane\Payment\Domain\Model\User\UserRepositoryInterface;
use Eplane\Payment\Domain\ValueObject\Fee;

class ResetUserProviderFeeToDefaultHandler implements ResetUserProviderFeeToDefaultHandlerInterface
{
    /**
     * @var ProviderRepositoryInterface
     */
    private $providerRepository;

    /**
     * @var UserRepositoryInterface
     */
    private $userRepository;

    /**
     * @var TransactionInterface
     */
    private $transaction;

    /**
     * @var EventDispatcherInterface
     */
    private $eventDispatcher;

    public function __construct(
        UserRepositoryInterface $userRepository,
        ProviderRepositoryInterface $providerRepository,
        TransactionInterface $transaction,
        EventDispatcherInterface $eventDispatcher
    ) {
        $this->userRepository = $userRepository;
        $this->providerRepository = $providerRepository;
        $this->transaction = $transaction;
        $this->eventDispatcher = $eventDispatcher;
    }

    /**
     * @param ResetUserProviderFeeToDefaultCommand $command
     * @throws \Exception
     */
    public function handle($command)
    {
        $provider = $this->providerRepository->getByCode($command->getProviderCode());

        try {
            $user = $this->userRepository->get(new UserId($command->getUserId()));

            $save = function ($user) {
                $this->userRepository->save($user);
            };
        } catch (NoSuchUserException $e){
            $user = new User(new UserId($command->getUserId()));

            $save = function ($user) {
                $this->userRepository->add($user);
            };
        }

        $user->setFeeToProvider(
            $provider->getId(),
            new Fee(
                null,
                false,
                ''
            ),
            $provider->getCode()
        );

        $this->transaction->execute(
            function () use ($save, $user) {
                $save($user);
                $this->eventDispatcher->dispatch($user->releaseEvents());
            }
        );
    }
}
