<?php

namespace Eplane\Payment\Application\Command\ResetUserProviderFeeToDefault;

use Eplane\Ddd\Application\CommandHandlerInterface;

interface ResetUserProviderFeeToDefaultHandlerInterface extends CommandHandlerInterface
{
    /**
     * @param ResetUserProviderFeeToDefaultCommand $command
     * @return bool
     */
    public function handle($command);
}
