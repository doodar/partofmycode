<?php

namespace Eplane\Payment\Application\Command\ResetUserProviderFeeToDefault;

use Eplane\Payment\Application\Exception\CommandInvalidParameters;
use Eplane\Payment\Domain\Model\User\UserId;

class ResetUserProviderFeeToDefaultCommand
{
    /** @var string */
    private $providerCode;

    /** @var UserId */
    private $userId;

    /**
     * @param string $providerCode
     * @param string $userId
     * @throws CommandInvalidParameters
     */
    public function __construct(string $providerCode, string $userId)
    {
        if (empty($providerCode) || empty($userId)) {
            throw new CommandInvalidParameters(
                sprintf('Parameters passed to %s is invalid', self::class)
            );
        }

        $this->providerCode = $providerCode;
        $this->userId = $userId;
    }

    public function getProviderCode(): string
    {
        return $this->providerCode;
    }

    public function getUserId(): string
    {
        return $this->userId;
    }
}
