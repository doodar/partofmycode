<?php

namespace Eplane\Payment\Application\Command\DetachMethodFromUser;

use Eplane\Payment\Domain\Model\Method\MethodRepositoryInterface;
use Eplane\Payment\Domain\Model\Provider\ProviderRepositoryInterface;
use Eplane\Payment\Domain\Model\User\UserId;
use Eplane\Payment\Domain\Model\User\UserRepositoryInterface;
use Eplane\Ddd\Application\Service\TransactionInterface;

class DetachMethodFromUserHandler implements DetachMethodFromUserHandlerInterface
{
    /**
     * @var MethodRepositoryInterface
     */
    private $methodRepository;

    /**
     * @var ProviderRepositoryInterface
     */
    private $providerRepository;

    /**
     * @var UserRepositoryInterface
     */
    private $userRepository;

    /**
     * @var TransactionInterface
     */
    private $transaction;

    public function __construct(
        MethodRepositoryInterface $methodRepository,
        ProviderRepositoryInterface $providerRepository,
        UserRepositoryInterface $userRepository,
        TransactionInterface $transaction
    ) {
        $this->methodRepository = $methodRepository;
        $this->providerRepository = $providerRepository;
        $this->userRepository = $userRepository;
        $this->transaction = $transaction;
    }

    /**
     * @param DetachMethodFromUserCommand $command
     * @return bool
     * @throws \Exception
     */
    public function handle($command)
    {
        $provider = $this->providerRepository->getByCode($command->getProviderCode());

        $method = $this->methodRepository->getByProviderAndCode($provider->getId(), $command->getMethodCode());

        $aggregate = $this->userRepository->get(new UserId($command->getUserId()));

        $aggregate->detachMethod($method->getId());

        $save = function ($aggregate){
            $this->userRepository->save($aggregate);
        };

        $this->transaction->execute(
            function() use ($save, $aggregate) {
                $save($aggregate);
            }
        );
    }
}
