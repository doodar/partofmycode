<?php

namespace Eplane\Payment\Application\Command\DetachMethodFromUser;

use Eplane\Ddd\Application\CommandHandlerInterface;

interface DetachMethodFromUserHandlerInterface extends CommandHandlerInterface
{
    /**
     * @param DetachMethodFromUserCommand $command
     * @return bool
     */
    public function handle($command);
}
