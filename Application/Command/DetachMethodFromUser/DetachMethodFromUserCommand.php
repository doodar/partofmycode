<?php

namespace Eplane\Payment\Application\Command\DetachMethodFromUser;

use Eplane\Payment\Application\Exception\CommandInvalidParameters;

class DetachMethodFromUserCommand
{
    /** @var string */
    private $providerCode;

    /** @var string */
    private $methodCode;

    /** @var string */
    private $userId;

    /**
     * @param string $providerCode
     * @param string $methodCode
     * @param string $userId
     */
    public function __construct(string $providerCode, string $methodCode, string $userId)
    {
        if(empty($providerCode) || empty($methodCode) || empty($userId)){
            throw new CommandInvalidParameters(
                sprintf('Parameters passed to %s is invalid', self::class)
            );
        }

        $this->providerCode = $providerCode;
        $this->methodCode   = $methodCode;
        $this->userId       = $userId;
    }

    /**
     * @return string
     */
    public function getProviderCode(): string
    {
        return $this->providerCode;
    }

    /**
     * @return string
     */
    public function getMethodCode(): string
    {
        return $this->methodCode;
    }

    /**
     * @return string
     */
    public function getUserId(): string
    {
        return $this->userId;
    }
}
