<?php
namespace Eplane\Payment\Application\Command\ConfirmPaymentRequest;

use Eplane\Base\Php\BigNumber\Decimal;
use Eplane\CurrencyExchange\Model\AllCurrency;
use Eplane\Ddd\Application\Service\TransactionInterface;
use Eplane\Ddd\Domain\Event\EventDispatcherInterface;
use Eplane\Ddd\Domain\Exception\DomainException;
use Eplane\Ddd\Domain\Model\Currency;
use Eplane\Ddd\Domain\Model\Money;
use Eplane\Payment\Domain\Model\Method\MethodRepositoryInterface;
use Eplane\Payment\Domain\Model\PaymentRequest\Exception\NoSuchPaymentRequestException;
use Eplane\Payment\Domain\Model\PaymentRequest\PaymentRequest;
use Eplane\Payment\Domain\Model\PaymentRequest\PaymentRequestRepositoryInterface;
use Eplane\Payment\Domain\Model\User\Exception\NoSuchUserException;
use Eplane\Payment\Domain\Model\User\UserId;
use Eplane\Payment\Domain\Model\User\UserRepositoryInterface;

/** @noinspection PhpUnused */

class ConfirmPaymentRequestHandler implements ConfirmPaymentRequestHandlerInterface
{
    /**
     * @var PaymentRequestRepositoryInterface
     */
    protected $paymentRequestRepository;
    /**
     * @var TransactionInterface
     */
    protected $transaction;
    /**
     * @var EventDispatcherInterface
     */
    protected $eventDispatcher;
    /**
     * @var UserRepositoryInterface
     */
    private $userRepository;

    /**
     * @var MethodRepositoryInterface
     */
    private $methodRepository;

    public function __construct(
        UserRepositoryInterface $userRepository,
        PaymentRequestRepositoryInterface $paymentRequestRepository,
        TransactionInterface $transaction,
        EventDispatcherInterface $eventDispatcher,
        MethodRepositoryInterface $methodRepository
    ) {
        $this->userRepository           = $userRepository;
        $this->paymentRequestRepository = $paymentRequestRepository;
        $this->transaction              = $transaction;
        $this->eventDispatcher          = $eventDispatcher;
        $this->methodRepository         = $methodRepository;
    }

    /**
     * @param ConfirmPaymentRequestCommand $command
     * @return PaymentRequest
     */
    public function handle($command)
    {
        try {
            return $this->paymentRequestRepository->getByChatRequestId($command->getChatRequestId());
        } catch (NoSuchPaymentRequestException $ex) {
            // ok
        }

        $reason = null;
        try {
            $total = new Money($command->getTotalAmount(), new Currency($command->getTotalCurrency()));
        } catch (DomainException $ex) {
            $total  = new Money(Decimal::zero(), new Currency(AllCurrency::USD));
            $reason = 'Unable to validate total amount';

            return $this->createRejected($command, $total, $reason);
        }

        $userId = new UserId($command->getUserId());

        try {
            $user = $this->userRepository->get($userId);
        } catch (NoSuchUserException $ex) {
            $reason = 'User doesn\'t exist';

            return $this->createRejected($command, $total, $reason);
        }

        if (!$user->canReceivePayments()
            && !($user->getPlatformPay() && $this->methodRepository->hasPlatformPaymentMethods())
        ) {
            $reason = 'User is not able to receive payments';

            return $this->createRejected($command, $total, $reason);
        }

        $paymentRequest = PaymentRequest::createConfirmed(
            $this->paymentRequestRepository->nextIdentity(),
            $command->getChatRequestId(),
            $command->getRfqIncrementId(),
            $userId,
            $command->getCustomerId(),
            $total
        );

        $this->transaction->execute(
            function () use ($paymentRequest) {
                $this->paymentRequestRepository->add($paymentRequest);
                $this->eventDispatcher->dispatch($paymentRequest->releaseEvents());
            }
        );

        return $paymentRequest;
    }

    private function createRejected(
        ConfirmPaymentRequestCommand $command,
        Money $total,
        string $reason
    ) {
        $paymentRequest = PaymentRequest::createRejected(
            $this->paymentRequestRepository->nextIdentity(),
            $command->getChatRequestId(),
            $command->getRfqIncrementId(),
            new UserId($command->getUserId()),
            $command->getCustomerId(),
            $total,
            $reason
        );

        $this->transaction->execute(
            function () use ($paymentRequest) {
                $this->paymentRequestRepository->add($paymentRequest);
                $this->eventDispatcher->dispatch($paymentRequest->releaseEvents());
            }
        );

        return $paymentRequest;
    }
}
