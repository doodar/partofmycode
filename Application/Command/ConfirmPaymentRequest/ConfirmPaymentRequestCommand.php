<?php
namespace Eplane\Payment\Application\Command\ConfirmPaymentRequest;

use Eplane\Base\Php\BigNumber\Decimal;

class ConfirmPaymentRequestCommand
{
    /** @var string */
    private $chatRequestId;

    /** @var string */
    private $rfqIncrementId;

    /** @var string */
    private $userId;

    /** @var string */
    private $customerId;

    /** @var Decimal */
    private $totalAmount;

    /** @var string */
    private $totalCurrency;

    /**
     * ConfirmPaymentRequestCommand constructor.
     * @param string $chatRequestId
     * @param string $rfqIncrementId
     * @param string $userId
     * @param string $customerId
     * @param Decimal $totalAmount
     * @param string $totalCurrency
     */
    public function __construct(
        string $chatRequestId,
        string $rfqIncrementId,
        string $userId,
        string $customerId,
        Decimal $totalAmount,
        string $totalCurrency
    ) {
        $this->chatRequestId = $chatRequestId;
        $this->rfqIncrementId = $rfqIncrementId;
        $this->userId = $userId;
        $this->customerId = $customerId;
        $this->totalAmount = $totalAmount;
        $this->totalCurrency = $totalCurrency;
    }

    /**
     * @return string
     */
    public function getChatRequestId(): string
    {
        return $this->chatRequestId;
    }

    /**
     * @return string
     */
    public function getUserId(): string
    {
        return $this->userId;
    }

    /**
     * @return string
     */
    public function getRfqIncrementId(): string
    {
        return $this->rfqIncrementId;
    }

    /**
     * @return string
     */
    public function getCustomerId(): string
    {
        return $this->customerId;
    }

    /**
     * @return Decimal
     */
    public function getTotalAmount(): Decimal
    {
        return $this->totalAmount;
    }

    /**
     * @return string
     */
    public function getTotalCurrency(): string
    {
        return $this->totalCurrency;
    }
}
