<?php
namespace Eplane\Payment\Application\Command\ConfirmPaymentRequest;

use Eplane\Ddd\Application\CommandHandlerInterface;

interface ConfirmPaymentRequestHandlerInterface extends CommandHandlerInterface
{
    /**
     * @param ConfirmPaymentRequestCommand $command
     * @return bool
     */
    public function handle($command);
}
