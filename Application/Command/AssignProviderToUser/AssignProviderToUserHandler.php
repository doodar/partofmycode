<?php

namespace Eplane\Payment\Application\Command\AssignProviderToUser;

use Eplane\Ddd\Domain\Event\EventDispatcherInterface;
use Eplane\Payment\Domain\Model\Method\MethodRepositoryInterface;
use Eplane\Payment\Domain\Model\Provider\ProviderRepositoryInterface;
use Eplane\Payment\Domain\Model\User\Exception\NoSuchUserException;
use Eplane\Payment\Domain\Model\User\User;
use Eplane\Payment\Domain\Model\User\UserId;
use Eplane\Payment\Domain\Model\User\UserRepositoryInterface;
use Eplane\Ddd\Application\Service\TransactionInterface;

/** @noinspection PhpUnused */
class AssignProviderToUserHandler implements AssignProviderToUserHandlerInterface
{
    /**
     * @var EventDispatcherInterface
     */
    private $eventDispatcher;

    /**
     * @var MethodRepositoryInterface
     */
    private $methodRepository;

    /**
     * @var ProviderRepositoryInterface
     */
    private $providerRepository;

    /**
     * @var UserRepositoryInterface
     */
    private $userRepository;

    /**
     * @var TransactionInterface
     */
    private $transaction;

    public function __construct(
        MethodRepositoryInterface $methodRepository,
        ProviderRepositoryInterface $providerRepository,
        UserRepositoryInterface $userRepository,
        TransactionInterface $transaction,
        EventDispatcherInterface $eventDispatcher
    ) {
        $this->methodRepository = $methodRepository;
        $this->providerRepository = $providerRepository;
        $this->userRepository = $userRepository;
        $this->transaction = $transaction;
        $this->eventDispatcher = $eventDispatcher;
    }

    /**
     * @param AssignProviderToUserCommand $command
     * @throws \Exception
=     */
    public function handle($command)
    {
        $provider = $this->providerRepository->getByCode($command->getProviderCode());

        $providerMethods = $this->methodRepository->getByProviderId($provider->getId());

        try {
            $aggregate = $this->userRepository->get(new UserId($command->getUserId()));

            $save = function ($aggregate) {
                $this->userRepository->save($aggregate);
            };
        } catch (NoSuchUserException $e) {
            $aggregate = new User(new UserId($command->getUserId()));

            $save = function ($aggregate) {
                $this->userRepository->add($aggregate);
            };
        }

        $aggregate->assignProvider($provider, $command->getConnectedAt(), ...$providerMethods);

        $this->transaction->execute(
            function() use ($save, $aggregate) {
                $save($aggregate);
                $this->eventDispatcher->dispatch($aggregate->releaseEvents());
            }
        );
    }
}
