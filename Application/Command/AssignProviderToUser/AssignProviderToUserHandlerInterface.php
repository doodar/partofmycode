<?php

namespace Eplane\Payment\Application\Command\AssignProviderToUser;

use Eplane\Ddd\Application\CommandHandlerInterface;

interface AssignProviderToUserHandlerInterface extends CommandHandlerInterface
{
    /**
     * @param AssignProviderToUserCommand $command
     */
    public function handle($command);
}
