<?php

namespace Eplane\Payment\Application\Command\AssignProviderToUser;

use Eplane\Payment\Application\Exception\CommandInvalidParameters;

class AssignProviderToUserCommand
{
    /** @var string */
    private $providerCode;

    /** @var string */
    private $userId;

    /** @var \DateTime */
    private $connectedAt;

    /**
     * @param string $providerCode
     * @param string $userId
     * @param \DateTime $connectedAt
     * @throws CommandInvalidParameters
     */
    public function __construct(string $providerCode, string $userId, \DateTime $connectedAt = null)
    {
        if(empty($providerCode) || empty($userId)){
            throw new CommandInvalidParameters(
                sprintf('Parameters passed to %s is invalid', self::class)
            );
        }

        $this->providerCode  = $providerCode;
        $this->userId        = $userId;
        $this->connectedAt   = $connectedAt;
    }

    /**
     * @return string
     */
    public function getProviderCode(): string
    {
        return $this->providerCode;
    }

    /**
     * @return string
     */
    public function getUserId(): string
    {
        return $this->userId;
    }

    /**
     * @return \DateTime
     */
    public function getConnectedAt(): \DateTime
    {
        return $this->connectedAt;
    }
}
