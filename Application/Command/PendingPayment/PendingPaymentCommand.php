<?php

namespace Eplane\Payment\Application\Command\PendingPayment;

use Eplane\Payment\Application\Exception\CommandInvalidParameters;

class PendingPaymentCommand
{
    /**
     * @var string
     */
    private $paymentDataUpdate;

    /**
     * @var string
     */
    private $paymentId;

    /**
     * PendingPaymentCommand constructor.
     * @param string $paymentId
     * @param string $paymentDataUpdate
     * @throws CommandInvalidParameters
     */
    public function __construct(string $paymentId, string $paymentDataUpdate)
    {
        if(empty($paymentId)) {
            throw new CommandInvalidParameters(
                sprintf('Parameters passed to %s is invalid', self::class)
            );
        }

        $this->paymentId = $paymentId;
        $this->paymentDataUpdate = $paymentDataUpdate;
    }

    /**
     * @return string
     */
    public function getPaymentId(): string
    {
        return $this->paymentId;
    }

    /**
     * @return string
     */
    public function getPaymentDataUpdate(): string
    {
        return $this->paymentDataUpdate;
    }
}
