<?php

namespace Eplane\Payment\Application\Command\PendingPayment;

use Eplane\Ddd\Application\CommandHandlerInterface;

interface PendingPaymentHandlerInterface extends CommandHandlerInterface
{

    /**
     * @param PendingPaymentCommand $command
     * @return mixed
     */
    public function handle($command);
}
