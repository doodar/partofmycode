<?php

namespace Eplane\Payment\Application\Command\FailPayment;


use Eplane\Payment\Application\Exception\CommandInvalidParameters;

class FailPaymentCommand
{
    /**
     * @var string
     */
    private $paymentId;

    /**
     * @var string
     */
    private $reason;

    /**
     * FailPaymentCommand constructor.
     * @param string $paymentId
     * @param string $reason
     * @throws CommandInvalidParameters
     */
    public function __construct(string $paymentId, string $reason)
    {
        if(empty($paymentId)) {
            throw new CommandInvalidParameters(
                sprintf('Parameters passed to %s is invalid', self::class)
            );
        }

        $this->paymentId = $paymentId;
        $this->reason = $reason;
    }

    /**
     * @return string
     */
    public function getPaymentId(): string
    {
        return $this->paymentId;
    }

    /**
     * @return string
     */
    public function getReason(): string
    {
        return $this->reason;
    }
}
