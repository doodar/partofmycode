<?php

namespace Eplane\Payment\Application\Command\FailPayment;

use Eplane\Ddd\Application\CommandHandlerInterface;

interface FailPaymentHandlerInterface extends CommandHandlerInterface
{

    /**
     * @param FailPaymentCommand $command
     * @return mixed
     */
    public function handle($command);
}
