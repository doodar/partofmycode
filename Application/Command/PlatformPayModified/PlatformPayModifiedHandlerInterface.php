<?php

namespace Eplane\Payment\Application\Command\PlatformPayModified;

use Eplane\Ddd\Application\CommandHandlerInterface;

interface PlatformPayModifiedHandlerInterface extends CommandHandlerInterface
{
    /**
     * @param PlatformPayModifiedCommand $command
     */
    public function handle($command);
}
