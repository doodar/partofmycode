<?php

namespace Eplane\Payment\Application\Command\PlatformPayModified;

use Eplane\Payment\Application\Exception\CommandInvalidParameters;

class PlatformPayModifiedCommand
{
    /** @var string */
    private $userId;

    /** @var bool */
    private $platformPay;

    /**
     * @param string $userId
     * @param bool $platformPay
     * @throws CommandInvalidParameters
     */
    public function __construct(string $userId, bool $platformPay)
    {
        if(empty($userId)){
            throw new CommandInvalidParameters(
                sprintf('Parameters passed to %s is invalid', self::class)
            );
        }

        $this->userId                   = $userId;
        $this->platformPay   = $platformPay;
    }

    /**
     * @return string
     */
    public function getUserId()
    {
        return $this->userId;
    }

    /**
     * @return bool
     */
    public function getPlatformPay()
    {
        return $this->platformPay;
    }
}
