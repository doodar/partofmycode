<?php

namespace Eplane\Payment\Application\Command\PlatformPayModified;

use Eplane\Payment\Domain\Model\User\Exception\NoSuchUserException;
use Eplane\Payment\Domain\Model\User\User;
use Eplane\Payment\Domain\Model\User\UserId;
use Eplane\Payment\Domain\Model\User\UserRepositoryInterface;
use Eplane\Ddd\Application\Service\TransactionInterface;

/** @noinspection PhpUnused */
class PlatformPayModifiedHandler implements PlatformPayModifiedHandlerInterface
{
    /**
     * @var UserRepositoryInterface
     */
    private $userRepository;

    /**
     * @var TransactionInterface
     */
    private $transaction;

    public function __construct(
        UserRepositoryInterface $userRepository,
        TransactionInterface $transaction
    ) {
        $this->userRepository = $userRepository;
        $this->transaction = $transaction;
    }

    /**
     * @param PlatformPayModifiedCommand $command
     * @throws \Exception
=     */
    public function handle($command)
    {
        try {
            $aggregate = $this->userRepository->get(new UserId($command->getUserId()));

            $save = function ($aggregate) {
                $this->userRepository->save($aggregate);
            };
        } catch (NoSuchUserException $e) {
            $aggregate = new User(new UserId($command->getUserId()));

            $save = function ($aggregate) {
                $this->userRepository->add($aggregate);
            };
        }

        $aggregate->setPlatformPay($command->getPlatformPay());

        $this->transaction->execute(
            function() use ($save, $aggregate) {
                $save($aggregate);
            }
        );
    }
}
