<?php

namespace Eplane\Payment\Application\Command\DetachProviderFromUser;

use Eplane\Payment\Application\Exception\CommandInvalidParameters;

class DetachProviderFromUserCommand
{
    /** @var string */
    private $providerCode;

    /** @var string */
    private $userId;

    /** @var \DateTime */
    private $disconnectedAt;

    /**
     * @param string $providerCode
     * @param string $userId
     * @param \DateTime $disconnectedAt
     * @throws CommandInvalidParameters
     */
    public function __construct(string $providerCode, string $userId, \DateTime $disconnectedAt = null)
    {
        if(empty($providerCode) || empty($userId)){
            throw new CommandInvalidParameters(
                sprintf('Parameters passed to %s is invalid', self::class)
            );
        }

        $this->providerCode     = $providerCode;
        $this->userId           = $userId;
        $this->disconnectedAt   = $disconnectedAt;
    }

    /**
     * @return string
     */
    public function getProviderCode(): string
    {
        return $this->providerCode;
    }

    /**
     * @return string
     */
    public function getUserId(): string
    {
        return $this->userId;
    }

    /**
     * @return \DateTime
     */
    public function getDisconnectedAt(): \DateTime
    {
        return $this->disconnectedAt;
    }
}
