<?php

namespace Eplane\Payment\Application\Command\DetachProviderFromUser;

use Eplane\Ddd\Domain\Event\EventDispatcherInterface;
use Eplane\Payment\Domain\Model\Method\MethodRepositoryInterface;
use Eplane\Payment\Domain\Model\Provider\ProviderRepositoryInterface;
use Eplane\Payment\Domain\Model\User\UserId;
use Eplane\Payment\Domain\Model\User\UserRepositoryInterface;
use Eplane\Ddd\Application\Service\TransactionInterface;

class DetachProviderFromUserHandler implements DetachProviderFromUserHandlerInterface
{
    /**
     * @var EventDispatcherInterface
     */
    protected $eventDispatcher;
    /**
     * @var MethodRepositoryInterface
     */
    private $methodRepository;

    /**
     * @var ProviderRepositoryInterface
     */
    private $providerRepository;

    /**
     * @var UserRepositoryInterface
     */
    private $userRepository;

    /**
     * @var TransactionInterface
     */
    private $transaction;

    public function __construct(
        MethodRepositoryInterface $methodRepository,
        ProviderRepositoryInterface $providerRepository,
        UserRepositoryInterface $userRepository,
        TransactionInterface $transaction,
        EventDispatcherInterface $eventDispatcher
    ) {
        $this->methodRepository = $methodRepository;
        $this->providerRepository = $providerRepository;
        $this->userRepository = $userRepository;
        $this->transaction = $transaction;
        $this->eventDispatcher = $eventDispatcher;
    }

    /**
     * @param DetachProviderFromUserCommand $command
     * @throws \Exception
     */
    public function handle($command)
    {
        $provider = $this->providerRepository->getByCode($command->getProviderCode());

        $aggregate = $this->userRepository->get(new UserId($command->getUserId()));

        $aggregate->detachProvider($provider->getId(), $command->getDisconnectedAt());

        $save = function ($aggregate){
            $this->userRepository->save($aggregate);
        };

        $this->transaction->execute(
            function() use ($save, $aggregate) {
                $save($aggregate);
                $this->eventDispatcher->dispatch($aggregate->releaseEvents());
            }
        );
    }
}
