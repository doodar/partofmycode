<?php

namespace Eplane\Payment\Application\Command\DetachProviderFromUser;

use Eplane\Ddd\Application\CommandHandlerInterface;

interface DetachProviderFromUserHandlerInterface extends CommandHandlerInterface
{
    /**
     * @param DetachProviderFromUserCommand $command
     */
    public function handle($command);
}
