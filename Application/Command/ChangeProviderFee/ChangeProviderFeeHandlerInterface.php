<?php

namespace Eplane\Payment\Application\Command\ChangeProviderFee;

use Eplane\Ddd\Application\CommandHandlerInterface;

interface ChangeProviderFeeHandlerInterface extends CommandHandlerInterface
{
    /**
     * @param ChangeProviderFeeCommand $command
     * @return bool
     */
    public function handle($command);
}
