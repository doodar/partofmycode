<?php

namespace Eplane\Payment\Application\Command\ChangeProviderFee;

use Eplane\Payment\Domain\Model\Provider\ProviderRepositoryInterface;
use Eplane\Ddd\Application\Service\TransactionInterface;
use Eplane\Payment\Domain\ValueObject\Fee;

class ChangeProviderFeeHandler implements ChangeProviderFeeHandlerInterface
{
    /**
     * @var ProviderRepositoryInterface
     */
    private $providerRepository;

    /**
     * @var TransactionInterface
     */
    private $transaction;

    public function __construct(
        ProviderRepositoryInterface $providerRepository,
        TransactionInterface $transaction
    ) {
        $this->providerRepository = $providerRepository;
        $this->transaction = $transaction;
    }

    /**
     * @param ChangeProviderFeeCommand $command
     * @throws \Exception
     */
    public function handle($command)
    {
        $provider = $this->providerRepository->getByCode($command->getProviderCode());

        $provider->changeDefaultFee(
            new Fee(
                $command->getAmount(),
                $command->getIsPercent(),
                $command->getCurrency()
            )
        );

        $save = function ($provider){
            $this->providerRepository->save($provider);
        };

        $this->transaction->execute(
            function() use ($save, $provider) {
                $save($provider);
            }
        );
    }
}
