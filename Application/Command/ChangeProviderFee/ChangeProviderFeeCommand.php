<?php

namespace Eplane\Payment\Application\Command\ChangeProviderFee;

use Eplane\Payment\Application\Exception\CommandInvalidParameters;

class ChangeProviderFeeCommand
{
    /** @var float */
    protected $amount;

    /** @var bool */
    protected $isPercent;

    /**  @var string */
    protected $currency;

    /** @var string */
    private $providerCode;

    /**
     * @param string $providerCode
     * @param float $amount
     * @param bool $isPercent
     * @param string $currency
     * @throws CommandInvalidParameters
     */
    public function __construct(string $providerCode, float $amount, bool $isPercent, string $currency)
    {
        if (empty($providerCode) || empty($currency)) {
            throw new CommandInvalidParameters(
                sprintf('Parameters passed to %s is invalid', self::class)
            );
        }

        $this->providerCode = $providerCode;
        $this->amount = $amount;
        $this->isPercent = $isPercent;
        $this->currency = $currency;
    }

    public function getAmount(): float
    {
        return $this->amount;
    }

    public function getCurrency(): string
    {
        return $this->currency;
    }

    public function getIsPercent(): bool
    {
        return $this->isPercent;
    }

    public function getProviderCode(): string
    {
        return $this->providerCode;
    }

}
