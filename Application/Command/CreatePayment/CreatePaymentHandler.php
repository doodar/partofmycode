<?php

namespace Eplane\Payment\Application\Command\CreatePayment;

use Eplane\Base\Php\BigNumber\Decimal;
use Eplane\Ddd\Application\Service\TransactionInterface;
use Eplane\Ddd\Domain\Event\EventDispatcherInterface;
use Eplane\Payment\Domain\Model\Method\Method;
use Eplane\Payment\Domain\Model\Method\MethodRepositoryInterface;
use Eplane\Payment\Domain\Model\Payment\Payment;
use Eplane\Payment\Domain\Model\Payment\PaymentRepositoryInterface;
use Eplane\Payment\Domain\Model\PaymentRequest\PaymentRequestRepositoryInterface;
use Eplane\Payment\Domain\Model\Provider\Provider;
use Eplane\Payment\Domain\Model\Provider\ProviderRepositoryInterface;
use Eplane\Payment\Domain\Model\User\User;
use Eplane\Payment\Domain\Model\User\UserId;
use Eplane\Payment\Domain\Model\User\UserRepositoryInterface;

/** @noinspection PhpUnused */
class CreatePaymentHandler implements CreatePaymentHandlerInterface
{
    /**
     * @var PaymentRepositoryInterface
     */
    private $paymentRepository;
    /**
     * @var UserRepositoryInterface
     */
    private $userRepository;
    /**
     * @var MethodRepositoryInterface
     */
    private $methodRepository;
    /**
     * @var ProviderRepositoryInterface
     */
    private $providerRepository;
    /**
     * @var EventDispatcherInterface
     */
    private $eventDispatcher;
    /**
     * @var PaymentRequestRepositoryInterface
     */
    private $paymentRequestRepository;
    /**
     * @var TransactionInterface
     */
    private $transaction;

    public function __construct(
        PaymentRepositoryInterface $paymentRepository,
        UserRepositoryInterface $userRepository,
        ProviderRepositoryInterface $providerRepository,
        MethodRepositoryInterface $methodRepository,
        PaymentRequestRepositoryInterface $paymentRequestRepository,
        TransactionInterface $transaction,
        EventDispatcherInterface $eventDispatcher
    )
    {
        $this->paymentRepository        = $paymentRepository;
        $this->userRepository           = $userRepository;
        $this->providerRepository       = $providerRepository;
        $this->methodRepository         = $methodRepository;
        $this->paymentRequestRepository = $paymentRequestRepository;
        $this->transaction              = $transaction;
        $this->eventDispatcher          = $eventDispatcher;
    }

    /**
     * @param CreatePaymentCommand $command
     * @throws \Exception
     */
    public function handle($command)
    {
        $sellerId = $command->getSellerId();
        $provider = $command->getProvider();
        $method = $command->getMethod();
        $req = $command->getPaymentRequestId();
        $priceCurrency = $this->getPriceCurrency($req);

        $checkUser = $this->userRepository->get(new UserId($sellerId));
        $checkProvider = $this->providerRepository->getByCode($provider);
        $checkMethod = $this->methodRepository->getByProviderAndCode($checkProvider->getId(), $method);

        $fee = $this->getFee($priceCurrency->getAmount(), $checkUser, $checkProvider, $checkMethod);

        if (!$this->ifProviderMethodAssigned($checkUser, $checkMethod, $checkProvider)) {
            $payment = Payment::createFailed(
                $this->paymentRepository->nextIdentity(),
                $command->getChatPaymentId(),
                $req,
                $command->getOrderId(),
                $command->getPaymentDataEncoded(),
                $provider,
                $method,
                $sellerId,
                $command->getBuyerId(),
                $command->getCustomerId(),
                $command->getMessageId(),
                $priceCurrency->getAmount(),
                $priceCurrency->getCurrency()->getValue(),
                $fee,
                'The payment method is not assigned with the user'
            );
        } else {
            $payment = Payment::createCreated(
                $this->paymentRepository->nextIdentity(),
                $command->getChatPaymentId(),
                $req,
                $command->getOrderId(),
                $command->getPaymentDataEncoded(),
                $provider,
                $method,
                $sellerId,
                $command->getBuyerId(),
                $command->getCustomerId(),
                $command->getMessageId(),
                $priceCurrency->getAmount(),
                $priceCurrency->getCurrency()->getValue(),
                $fee,
                $this->isPlatformPayment($checkUser, $checkMethod)
            );
        }
        $this->transaction->execute(
            function () use ($payment) {
                $this->paymentRepository->add($payment);
                $this->eventDispatcher->dispatch($payment->releaseEvents());
            }
        );
    }

    /**
     * @param User $user
     * @param Method $method
     * @param Provider $provider
     * @return bool
     */
    private function ifProviderMethodAssigned(User $user, Method $method, Provider $provider)
    {
        try {
            $isAssigned = $user->isProviderAssigned($provider->getId())
                && $user->isMethodAssigned($method->getId());

            if(!$isAssigned){
                $isAssigned = $this->isPlatformPayment($user, $method);
            }
        } catch (\Exception $e) {
            $isAssigned = false;
        }

        return $isAssigned;
    }

    private function getPriceCurrency(string $requestId)
    {
        $request = $this->paymentRequestRepository->getByChatRequestId($requestId);
        return $request->getTotal();
    }

    private function getFee(Decimal $price, User $user, Provider $provider, Method $method)
    {
        //todo implement currency exchange if fee is not a percented value

        if ($this->isPlatformPayment($user, $method)) {
            return Decimal::zero();
        }

        $fee = $user->getProviderSpecificFee($provider->getId());
        $fee = $fee ?? $provider->getDefaultFee();

        $percent = (float)$fee->getAmount();

        return Decimal::create(ceil($price->toFloat() * $percent))->div(100);
    }

    /**
     * @param User $user
     * @param Method $method
     * @return bool
     */
    private function isPlatformPayment(User $user, Method $method){
        try {
            $isPlatformPayment = $user->getPlatformPay() && $method->isPlatformEnabled();
        } catch (\Exception $e) {
            $isPlatformPayment = false;
        }

        return $isPlatformPayment;
    }
}
