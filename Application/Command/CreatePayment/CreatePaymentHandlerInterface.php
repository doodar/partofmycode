<?php

namespace Eplane\Payment\Application\Command\CreatePayment;

use Eplane\Ddd\Application\CommandHandlerInterface;

interface CreatePaymentHandlerInterface extends CommandHandlerInterface
{

    /**
     * @param $command
     * @return mixed
     */
    public function handle($command);
}
