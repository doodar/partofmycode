<?php

namespace Eplane\Payment\Application\Command\CreatePayment;


use Eplane\Payment\Application\Exception\CommandInvalidParameters;

class CreatePaymentCommand
{
    /**
     * @var string
     */
    private $chatPaymentId;
    /**
     * @var string
     */
    private $paymentRequestId;
    /**
     * @var string
     */
    private $orderId;
    /**
     * @var string
     */
    private $paymentDataEncoded;
    /**
     * @var string
     */
    private $provider;
    /**
     * @var string
     */
    private $method;
    /**
     * @var string
     */
    private $sellerId;
    /**
     * @var string
     */
    private $buyerId;
    /**
     * @var string
     */
    private $customerId;
    /**
     * @var string
     */
    private $messageId;

    /**
     * CreatePaymentCommand constructor.
     * @param string $chatPaymentId
     * @param string $paymentRequestId
     * @param string $orderId
     * @param string $paymentDataEncoded
     * @param string $provider
     * @param string $method
     * @param string $sellerId
     * @param string $buyerId
     * @param string $customerId
     * @param string $messageId
     * @throws CommandInvalidParameters
     */
    public function __construct(
        string $chatPaymentId,
        string $paymentRequestId,
        string $orderId,
        string $paymentDataEncoded,
        string $provider,
        string $method,
        string $sellerId,
        string $buyerId,
        string $customerId,
        string $messageId
    )
    {
        if(
            empty($chatPaymentId)
            || empty($paymentRequestId)
            || empty($orderId)
            || empty($paymentDataEncoded)
            || empty($provider)
            || empty($method)
            || empty($sellerId)
            || empty($buyerId)
            || empty($customerId)
            || empty($messageId)
        ) {
            throw new CommandInvalidParameters(
                sprintf('Parameters passed to %s is invalid', self::class)
            );
        }
        
        $this->chatPaymentId = $chatPaymentId;
        $this->paymentRequestId = $paymentRequestId;
        $this->orderId = $orderId;
        $this->paymentDataEncoded = $paymentDataEncoded;
        $this->provider = $provider;
        $this->method = $method;
        $this->sellerId = $sellerId;
        $this->buyerId = $buyerId;
        $this->customerId = $customerId;
        $this->messageId = $messageId;
    }

    /**
     * @return string
     */
    public function getChatPaymentId(): string
    {
        return $this->chatPaymentId;
    }

    /**
     * @return string
     */
    public function getPaymentRequestId(): string
    {
        return $this->paymentRequestId;
    }

    /**
     * @return string
     */
    public function getOrderId(): string
    {
        return $this->orderId;
    }

    /**
     * @return string
     */
    public function getPaymentDataEncoded(): string
    {
        return $this->paymentDataEncoded;
    }

    /**
     * @return string
     */
    public function getProvider(): string
    {
        return $this->provider;
    }

    /**
     * @return string
     */
    public function getMethod(): string
    {
        return $this->method;
    }

    /**
     * @return string
     */
    public function getSellerId(): string
    {
        return $this->sellerId;
    }

    /**
     * @return string
     */
    public function getBuyerId(): string
    {
        return $this->buyerId;
    }

    /**
     * @return string
     */
    public function getCustomerId(): string
    {
        return $this->customerId;
    }

    /**
     * @return string
     */
    public function getMessageId(): string
    {
        return $this->messageId;
    }
}
