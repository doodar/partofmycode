<?php
namespace Eplane\Payment\Infrastructure\Setup\Upgrade;

use Eplane\Payment\Infrastructure\Domain\Model\Payment\Magento\Payment;
use Eplane\Payment\Infrastructure\Domain\Model\Payment\Magento\ResourceModel\Tables;
use Eplane\Setup\Model\AbstractUpgrade;


class EP6858 extends AbstractUpgrade
{
    public function __invoke()
    {
        $this->connection->query('DROP TABLE IF EXISTS `'. Tables::TABLE_PAYMENT.'`');

            $sql = sprintf(
                '
            CREATE TABLE IF NOT EXISTS  %s(
              `%2$s` BIGINT(20) UNSIGNED NOT NULL,
              `%3$s` BIGINT(20) UNSIGNED NOT NULL,
              `%4$s` BIGINT(20) UNSIGNED NOT NULL,
              `%5$s` BIGINT(20) UNSIGNED NOT NULL,
              `%6$s` TEXT,
              `%7$s` VARCHAR(30) NOT NULL,
              `%8$s` VARCHAR(30) NOT NULL,
              `%9$s` BIGINT(20) UNSIGNED NOT NULL,
              `%10$s` BIGINT(20) UNSIGNED NOT NULL,
              `%11$s` BIGINT(20) UNSIGNED NOT NULL,
              `%12$s` BIGINT(20) UNSIGNED NOT NULL,
              `%13$s` DECIMAL(12,4) UNSIGNED NOT NULL,
              `%14$s` VARCHAR(10),
              `%15$s` DECIMAL(12,4) UNSIGNED NOT NULL,
              `%16$s` VARCHAR(30) NOT NULL,
              `%17$s` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
              `%18$s` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
              PRIMARY KEY (`%2$s`)
            ) ENGINE=InnoDB CHARSET=utf8;
            ',
                Tables::TABLE_PAYMENT,
                Payment::ID,
                Payment::CHAT_PAYMENT_ID,
                Payment::REQUEST_ID,
                Payment::ORDER_ID,
                Payment::PAYMENT_DATA_ENCODED,
                Payment::PROVIDER,
                Payment::METHOD,
                Payment::SELLER_ID,
                Payment::BUYER_ID,
                Payment::CUSTOMER_ID,
                Payment::MESSAGE_ID,
                Payment::PRICE,
                Payment::CURRENCY,
                Payment::FEE,
                Payment::STATUS,
                Payment::CREATED_AT,
                Payment::UPDATED_AT
            );

            $this->connection->query($sql);
        }
}
