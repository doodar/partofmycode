<?php
namespace Eplane\Payment\Infrastructure\Setup\Upgrade;

use Eplane\Payment\Infrastructure\Domain\Model\Payment\Magento\Payment;
use Eplane\Payment\Infrastructure\Domain\Model\Payment\Magento\ResourceModel\Tables as PaymentTables;
use Eplane\Setup\Model\AbstractUpgrade;
use Magento\Framework\DB\Ddl\Table;

class EP7072AddIsPlatformInPayment extends AbstractUpgrade
{
    public function __invoke()
    {
        if (!$this->connection->tableColumnExists(PaymentTables::TABLE_PAYMENT, Payment::IS_PLATFORM)) {
            $this->connection->addColumn(
                PaymentTables::TABLE_PAYMENT,
                Payment::IS_PLATFORM,
                [
                    'nullable' => false,
                    'primary' => false,
                    'default' => 0,
                    'length' => 1,
                    'type' => Table::TYPE_BOOLEAN,
                    'comment' => 'Associated with ePlane',
                    'after' => Payment::STATUS,
                ]
            );
        }
    }
}
