<?php
namespace Eplane\Payment\Infrastructure\Setup\Upgrade;

use Eplane\Payment\Infrastructure\Domain\Model\Method\Magento\ResourceModel\Tables as MethodTables;
use Eplane\Payment\Infrastructure\Domain\Model\Provider\Magento\ResourceModel\Tables as ProviderTables;
use Eplane\Payment\Infrastructure\Domain\Model\Provider\Magento\Provider;
use Eplane\Payment\Infrastructure\Domain\Model\Method\Magento\Method;
use Eplane\Setup\Model\AbstractUpgrade;
use Magento\Framework\DB\Ddl\Table;

class EP7072AddPlatformPayMarkInMethods extends AbstractUpgrade
{
    public function __invoke()
    {
        if (!$this->connection->tableColumnExists(MethodTables::TABLE_PAYMENT_METHODS, Method::PLATFORM_ENABLED)) {
            $this->connection->addColumn(
                MethodTables::TABLE_PAYMENT_METHODS,
                Method::PLATFORM_ENABLED,
                [
                    'nullable' => false,
                    'primary' => false,
                    'default' => 0,
                    'length' => 1,
                    'type' => Table::TYPE_BOOLEAN,
                    'comment' => 'Associated with ePlane',
                    'after' => Method::ENABLED,
                ]
            );
        }

        if ($this->connection->tableColumnExists(MethodTables::TABLE_PAYMENT_METHODS, Method::PLATFORM_ENABLED)) {
            $sql = sprintf(
                '
                UPDATE %1$s
                SET    %1$s.%3$s = %4$s
                WHERE  %1$s.%5$s IN (SELECT %2$s.id
                                     FROM   %2$s
                                     WHERE
                                             %2$s.%6$s = \'%7$s\')
            ',
                MethodTables::TABLE_PAYMENT_METHODS,
                ProviderTables::TABLE_PAYMENT_PROVIDERS,
                Method::PLATFORM_ENABLED,
                true,
                Method::PROVIDER_ID,
                Provider::CODE,
                'stripe'
            );

            $this->connection->query($sql);
        }
    }
}
