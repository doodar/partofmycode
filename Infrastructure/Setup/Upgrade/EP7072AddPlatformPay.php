<?php
namespace Eplane\Payment\Infrastructure\Setup\Upgrade;

use Eplane\Payment\Infrastructure\Domain\Model\User\Magento\ResourceModel\Tables as UserPaymentTables;
use Eplane\Payment\Infrastructure\Domain\Model\User\Magento\UserPayment;
use Eplane\Setup\Model\AbstractUpgrade;
use Magento\Framework\DB\Ddl\Table;

class EP7072AddPlatformPay extends AbstractUpgrade
{
    public function __invoke()
    {
        if (!$this->connection->tableColumnExists(UserPaymentTables::TABLE_PAYMENT_USER, UserPayment::PLATFORM_PAY)) {
            $this->connection->addColumn(
                UserPaymentTables::TABLE_PAYMENT_USER,
                UserPayment::PLATFORM_PAY,
                [
                    'nullable' => false,
                    'primary' => false,
                    'default' => 0,
                    'length' => 1,
                    'type' => Table::TYPE_BOOLEAN,
                    'comment' => 'Associated with ePlane',
                    'after' => UserPayment::IS_ACTIVE,
                ]
            );
        }
    }
}
