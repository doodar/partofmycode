<?php
namespace Eplane\Payment\Infrastructure\Setup\Upgrade;

use Eplane\Payment\Infrastructure\Domain\Model\PaymentRequest\Magento\PaymentRequestInterface;
use Eplane\Payment\Infrastructure\Domain\Model\PaymentRequest\Magento\ResourceModel\Tables;
use Eplane\Setup\Model\AbstractUpgrade;

class EP6845PaymentRequestTable extends AbstractUpgrade
{
    public function __invoke()
    {
        if ($this->connection->isTableExists(Tables::TABLE_PAYMENT_REQUEST)) {
            return;
        }

        $this->connection->query(\sprintf('
            create table `%10$s` (
              `%1$s` bigint (20) UNSIGNED NOT NULL,
              `%2$s` bigint (20) UNSIGNED NOT NULL,
              `%3$s` varchar (32) NOT NULL,
              `%4$s` bigint (20) UNSIGNED NOT NULL,
              `%5$s` int (10) UNSIGNED NOT NULL,
              `%6$s` decimal (12, 4) NOT NULL,
              `%7$s` varchar (3) NOT NULL,
              `%8$s` varchar (32) NOT NULL,
              `%9$s` varchar (255) DEFAULT NULL,
              primary key (`%1$s`),
              UNIQUE index (`%2$s`)
            ) ENGINE = InnoDB charset = utf8 collate = utf8_general_ci;',
            PaymentRequestInterface::ID,
            PaymentRequestInterface::CHAT_REQUEST_ID,
            PaymentRequestInterface::RFQ_INCREMENT_ID,
            PaymentRequestInterface::USER_ID,
            PaymentRequestInterface::CUSTOMER_ID,
            PaymentRequestInterface::TOTAL_AMOUNT,
            PaymentRequestInterface::TOTAL_CURRENCY,
            PaymentRequestInterface::STATUS,
            PaymentRequestInterface::REJECT_REASON,
            Tables::TABLE_PAYMENT_REQUEST
        ));
    }
}
