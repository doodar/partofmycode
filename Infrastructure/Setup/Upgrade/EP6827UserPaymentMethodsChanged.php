<?php
namespace Eplane\Payment\Infrastructure\Setup\Upgrade;

use Eplane\Setup\Model\AbstractUpgrade;
use Eplane\Payment\Infrastructure\Domain\Model\Provider\Magento\ResourceModel\Tables as PaymentProviderTables;
use Eplane\Payment\Infrastructure\Domain\Model\Method\Magento\ResourceModel\Tables as PaymentMethodTables;
use Eplane\Payment\Infrastructure\Domain\Model\User\Magento\ResourceModel\Tables as UserPaymentTables;
use Eplane\Payment\Infrastructure\Domain\Model\Provider\Magento\Provider;
use Eplane\Payment\Infrastructure\Domain\Model\Method\Magento\Method;
use Eplane\Users\Model\ResourceModel\Tables as UsersTables;
use Eplane\Payment\Infrastructure\Domain\Model\User\Magento\UserPayment;
use Eplane\Payment\Infrastructure\Domain\Model\User\Magento\UserPaymentMethods;
use Eplane\Payment\Infrastructure\Domain\Model\User\Magento\UserPaymentProviders;
use Eplane\Users\Api\Data\UserInterface;

class EP6827UserPaymentMethodsChanged extends AbstractUpgrade
{
    /**
     * @throws \Zend_Db_Exception
     */
    public function __invoke()
    {
        if(!$this->connection->isTableExists(PaymentProviderTables::TABLE_PAYMENT_PROVIDERS)) {
            $sql = sprintf(
                '
            CREATE TABLE %s(
              `%2$s` BIGINT(20) UNSIGNED NOT NULL,
              `%3$s` VARCHAR(255),
              `%4$s` VARCHAR(255),
              `%5$s` SMALLINT DEFAULT 1,
              `%6$s` DECIMAL(12,4) UNSIGNED NOT NULL,
              `%7$s` SMALLINT DEFAULT 1,
              `%8$s` VARCHAR(3),
              PRIMARY KEY (`%2$s`)
            ) ENGINE=InnoDB CHARSET=utf8;
            ',
                PaymentProviderTables::TABLE_PAYMENT_PROVIDERS,
                Provider::ID,
                Provider::CODE,
                Provider::TITLE,
                Provider::ENABLED,
                Provider::DEFAULT_FEE,
                Provider::IS_PERCENT,
                Provider::CURRENCY
            );

            $this->connection->query($sql);
        }

        if(!$this->connection->isTableExists(PaymentMethodTables::TABLE_PAYMENT_METHODS)) {
            $sql = sprintf(
                '
            CREATE TABLE %s(
              `%2$s` BIGINT(20) UNSIGNED NOT NULL,
              `%3$s` VARCHAR(255),
              `%4$s` VARCHAR(255),
              `%5$s` BIGINT(20) UNSIGNED NOT NULL,
              `%6$s` SMALLINT DEFAULT 1,
              PRIMARY KEY (`%2$s`),
              FOREIGN KEY (`%5$s`) REFERENCES `%7$s` (`%8$s`) ON DELETE RESTRICT
            ) ENGINE=InnoDB CHARSET=utf8;
            ',
                PaymentMethodTables::TABLE_PAYMENT_METHODS,
                Method::ID,
                Method::CODE,
                Method::TITLE,
                Method::PROVIDER_ID,
                Method::ENABLED,
                PaymentProviderTables::TABLE_PAYMENT_PROVIDERS,
                Provider::ID
            );

            $this->connection->query($sql);
        }

        if(!$this->connection->isTableExists(UserPaymentTables::TABLE_PAYMENT_USER)) {
            $sql = sprintf(
                '
            CREATE TABLE %s(
              `%2$s` BIGINT(20) UNSIGNED NOT NULL,
              `%3$s` SMALLINT DEFAULT 1,
              `%4$s` TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
              `%5$s` TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
              PRIMARY KEY (`%2$s`),
              FOREIGN KEY (`%2$s`) REFERENCES `%6$s` (`%7$s`) ON DELETE RESTRICT
            ) ENGINE=InnoDB CHARSET=utf8;
            ',
                UserPaymentTables::TABLE_PAYMENT_USER,
                UserPayment::USER_ID,
                UserPayment::IS_ACTIVE,
                UserPayment::CREATED_AT,
                UserPayment::LAST_MODIFY_AT,
                UsersTables::TABLE_USER,
                UserInterface::USER_ID
            );

            $this->connection->query($sql);
        }

        if(!$this->connection->isTableExists(UserPaymentTables::TABLE_PAYMENT_USER_METHODS)) {
            $sql = sprintf(
                '
            CREATE TABLE %s(
              `%2$s` BIGINT(20) UNSIGNED NOT NULL AUTO_INCREMENT,
              `%3$s` BIGINT(20) UNSIGNED NOT NULL,
              `%4$s` BIGINT(20) UNSIGNED NOT NULL,
              `%5$s` BIGINT(20) UNSIGNED NOT NULL,
              `%6$s` SMALLINT DEFAULT 1,
              `%7$s` TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
              PRIMARY KEY (`%2$s`),
              UNIQUE INDEX `user_payment_method_unq` (`%3$s`, `%4$s`, `%5$s`),
              FOREIGN KEY (`%3$s`) REFERENCES `%8$s` (`%9$s`) ON DELETE RESTRICT
            ) ENGINE=InnoDB CHARSET=utf8;
            ',
                UserPaymentTables::TABLE_PAYMENT_USER_METHODS,
                UserPaymentMethods::ID,
                UserPaymentMethods::USER_ID,
                UserPaymentMethods::PAYMENT_PROVIDER_ID,
                UserPaymentMethods::PAYMENT_METHOD_ID,
                UserPaymentMethods::IS_ASSIGNED,
                UserPaymentMethods::LAST_MODIFY_AT,
                UsersTables::TABLE_USER,
                UserInterface::USER_ID
            );

            $this->connection->query($sql);
        }

        if(!$this->connection->isTableExists(UserPaymentTables::TABLE_PAYMENT_USER_PROVIDERS)) {
            $sql = sprintf(
                '
            CREATE TABLE %s(
              `%2$s` BIGINT(20) UNSIGNED NOT NULL AUTO_INCREMENT,
              `%3$s` BIGINT(20) UNSIGNED NOT NULL,
              `%4$s` BIGINT(20) UNSIGNED NOT NULL,
              `%5$s` SMALLINT DEFAULT 1,
              `%6$s` DECIMAL(12,4) UNSIGNED NOT NULL DEFAULT 2,
              `%7$s` SMALLINT DEFAULT 1,
              `%8$s` VARCHAR(3),
              `%9$s` TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
              PRIMARY KEY (`%2$s`),
              UNIQUE INDEX `user_payment_provider_unq` (`%3$s`, `%4$s`),
              FOREIGN KEY (`%3$s`) REFERENCES `%10$s` (`%11$s`) ON DELETE RESTRICT
            ) ENGINE=InnoDB CHARSET=utf8;
            ',
                UserPaymentTables::TABLE_PAYMENT_USER_PROVIDERS,
                UserPaymentProviders::ID,
                UserPaymentProviders::USER_ID,
                UserPaymentProviders::PAYMENT_PROVIDER_ID,
                UserPaymentProviders::IS_ASSIGNED,
                UserPaymentProviders::FEE,
                UserPaymentProviders::IS_PERCENT,
                UserPaymentProviders::CURRENCY,
                UserPaymentProviders::LAST_MODIFY_AT,
                UsersTables::TABLE_USER,
                UserInterface::USER_ID
            );

            $this->connection->query($sql);
        }

        $this->connection->query('TRUNCATE TABLE `'. UserPaymentTables::TABLE_PAYMENT_USER_METHODS.'`');
        $this->connection->query('TRUNCATE TABLE `'. UserPaymentTables::TABLE_PAYMENT_USER_PROVIDERS.'`');
        $this->connection->query('TRUNCATE TABLE `'. UserPaymentTables::TABLE_PAYMENT_USER.'`');
    }
}
