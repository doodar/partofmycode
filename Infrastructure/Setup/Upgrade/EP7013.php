<?php
namespace Eplane\Payment\Infrastructure\Setup\Upgrade;

use Eplane\Payment\Infrastructure\Domain\Model\User\Magento\UserPaymentProviders;
use Eplane\Setup\Model\AbstractUpgrade;
use Eplane\Payment\Infrastructure\Domain\Model\User\Magento\ResourceModel\Tables;

class EP7013 extends AbstractUpgrade
{
    public function __invoke()
    {
            $this->connection->changeColumn(
                Tables::TABLE_PAYMENT_USER_PROVIDERS,
                UserPaymentProviders::LAST_MODIFY_AT,
                UserPaymentProviders::LAST_MODIFY_AT,
                'timestamp NULL ON UPDATE CURRENT_TIMESTAMP');
        }
}
