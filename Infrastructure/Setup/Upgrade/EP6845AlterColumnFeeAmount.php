<?php
namespace Eplane\Payment\Infrastructure\Setup\Upgrade;

use Eplane\Payment\Infrastructure\Domain\Model\Payment\Magento\ResourceModel\Tables;
use Eplane\Payment\Infrastructure\Domain\Model\User\Magento\ResourceModel\Tables as UserPaymentTables;
use Eplane\Payment\Infrastructure\Domain\Model\User\Magento\UserPaymentProviders;
use Eplane\Setup\Model\AbstractUpgrade;


class EP6845AlterColumnFeeAmount extends AbstractUpgrade
{
    public function __invoke()
    {
        if($this->connection->isTableExists(UserPaymentTables::TABLE_PAYMENT_USER_PROVIDERS)) {
            $sql = sprintf(
                'alter table %s modify %s decimal(12,4) default NULL',
                UserPaymentTables::TABLE_PAYMENT_USER_PROVIDERS,
                UserPaymentProviders::FEE
            );

            $this->connection->query($sql);
        }
    }
}
