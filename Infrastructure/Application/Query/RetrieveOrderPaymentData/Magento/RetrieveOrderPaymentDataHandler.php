<?php

namespace Eplane\Payment\Infrastructure\Application\Query\RetrieveOrderPaymentData\Magento;

use Eplane\Base\Utils\DbUtils;
use Eplane\Payment\Application\Query\RetrieveOrderPaymentData\Method;
use Eplane\Payment\Application\Query\RetrieveOrderPaymentData\RetrieveOrderPaymentDataHandlerInterface;
use Eplane\Payment\Application\Query\RetrieveOrderPaymentData\RetrieveOrderPaymentDataReadModel;
use Eplane\Payment\Application\Query\RetrieveOrderPaymentData\Provider;
use Eplane\Payment\Application\Query\RetrieveOrderPaymentData\RetrieveOrderPaymentDataQuery;
use Eplane\Payment\Domain\ValueObject\Fee;
use Eplane\Payment\Infrastructure\Domain\Model\User\Magento\UserPaymentProviders;
use Eplane\Payment\Infrastructure\Domain\Model\User\Magento\UserPaymentMethods;
use Eplane\Payment\Infrastructure\Domain\Model\User\Magento\ResourceModel\Tables as UserTables;
use Eplane\Payment\Infrastructure\Domain\Model\Provider\Magento\ResourceModel\Tables as ProviderTables;
use Eplane\Payment\Infrastructure\Domain\Model\Provider\Magento\Provider as PaymentProvider;
use Eplane\Payment\Infrastructure\Domain\Model\Method\Magento\Method as PaymentMethod;
use Eplane\Payment\Infrastructure\Domain\Model\Method\Magento\ResourceModel\Tables as MethodTables;

class RetrieveOrderPaymentDataHandler implements RetrieveOrderPaymentDataHandlerInterface
{
    /**
     * @param RetrieveOrderPaymentDataQuery $query
     * @return RetrieveOrderPaymentDataReadModel
     * @throws \Zend_Db_Statement_Exception
     */
    public function handle($query)
    {
        $connection = DbUtils::getDefaultConnection();

        $providers = [];

        $sql = sprintf(
            'SELECT %1$s FROM %2$s WHERE %3$s = %4$s',
            \Eplane\Payment\Infrastructure\Domain\Model\User\Magento\UserPayment::PLATFORM_PAY,
            \Eplane\Payment\Infrastructure\Domain\Model\User\Magento\ResourceModel\Tables::TABLE_PAYMENT_USER,
            \Eplane\Payment\Infrastructure\Domain\Model\User\Magento\UserPayment::USER_ID,
            $connection->quote($query->getUserId())
        );

        $isPlatformUser = ($connection->query($sql)->fetchAll())[0] ?? null;

        if (($isPlatformUser[\Eplane\Payment\Infrastructure\Domain\Model\User\Magento\UserPayment::PLATFORM_PAY] ?? false)) {
            $sql = sprintf(
                'SELECT *
                        FROM %1$s
                        WHERE %2$s IN (SELECT %3$s FROM %4$s WHERE %5$s = %6$s) AND %7$s = %6$s',
                \Eplane\Payment\Infrastructure\Domain\Model\Provider\Magento\ResourceModel\Tables::TABLE_PAYMENT_PROVIDERS,
                \Eplane\Payment\Infrastructure\Domain\Model\Provider\Magento\Provider::ID,
                \Eplane\Payment\Infrastructure\Domain\Model\Method\Magento\Method::PROVIDER_ID,
                \Eplane\Payment\Infrastructure\Domain\Model\Method\Magento\ResourceModel\Tables::TABLE_PAYMENT_METHODS,
                \Eplane\Payment\Infrastructure\Domain\Model\Method\Magento\Method::PLATFORM_ENABLED,
                true,
                \Eplane\Payment\Infrastructure\Domain\Model\Provider\Magento\Provider::ENABLED
            );

            $platformProviders = $connection->query($sql)->fetchAll();

            foreach ($platformProviders as $platformProvider) {
                $sqlMethods = sprintf(
                    'SELECT * FROM %1$s WHERE %2$s = %3$s AND %4$s = %5$s AND %6$s = %5$s',
                    \Eplane\Payment\Infrastructure\Domain\Model\Method\Magento\ResourceModel\Tables::TABLE_PAYMENT_METHODS,
                    \Eplane\Payment\Infrastructure\Domain\Model\Method\Magento\Method::PROVIDER_ID,
                    $platformProvider[\Eplane\Payment\Infrastructure\Domain\Model\Provider\Magento\Provider::ID],
                    \Eplane\Payment\Infrastructure\Domain\Model\Method\Magento\Method::PLATFORM_ENABLED,
                    true,
                    \Eplane\Payment\Infrastructure\Domain\Model\Method\Magento\Method::ENABLED
                );

                $platformMethods = $connection->query($sqlMethods)->fetchAll();

                $methods = [];

                foreach ($platformMethods as $platformMethod) {
                    $methods[] = new Method(
                        $platformMethod['code'],
                        $platformMethod['title']
                    );
                }

                $providers[] = new Provider(
                    $platformProvider['code'],
                    $platformProvider['title'],
                    new Fee(
                        $platformProvider['default_fee'],
                        $platformProvider['is_percent'],
                        $platformProvider['currency']
                    ),
                    $methods
                );
            }
        } else {
            $sqlProviders = sprintf(
                '
                SELECT %1$s.%2$s,
                       %1$s.%3$s,
                       %4$s.%5$s,
                       %4$s.%6$s,
                       %4$s.%7$s,
                       %1$s.%16$s as `default_fee`,
                       %1$s.%17$s as `default_is_percent`,
                       %1$s.%18$s as `default_currency`,
                       %1$s.%15$s
                FROM %1$s
                LEFT JOIN %4$s ON %4$s.%8$s = %1$s.%9$s
                WHERE %1$s.%10$s = %13$s AND %4$s.%11$s = %13$s AND %4$s.%12$s = %14$s
            ',
                ProviderTables::TABLE_PAYMENT_PROVIDERS,
                PaymentProvider::CODE,
                PaymentProvider::TITLE,
                UserTables::TABLE_PAYMENT_USER_PROVIDERS,
                UserPaymentProviders::FEE,
                UserPaymentProviders::IS_PERCENT,
                UserPaymentProviders::CURRENCY,
                UserPaymentProviders::PAYMENT_PROVIDER_ID,
                PaymentProvider::ID,
                PaymentProvider::ENABLED,
                UserPaymentProviders::IS_ASSIGNED,
                UserPaymentProviders::USER_ID,
                true,
                $connection->quote($query->getUserId()),
                PaymentProvider::ID,
                \Eplane\Payment\Infrastructure\Domain\Model\Provider\Magento\Provider::DEFAULT_FEE,
                \Eplane\Payment\Infrastructure\Domain\Model\Provider\Magento\Provider::IS_PERCENT,
                \Eplane\Payment\Infrastructure\Domain\Model\Provider\Magento\Provider::CURRENCY
            );

            $userProviders = $connection->query($sqlProviders)->fetchAll();

            foreach ($userProviders as $userProvider) {
                $sqlMethods = sprintf(
                    '
                SELECT %1$s.%3$s, %1$s.%4$s
                FROM %1$s
                         LEFT JOIN %2$s ON %2$s.%5$s = %1$s.id
                WHERE %1$s.%6$s = %10$s
                  AND %2$s.%7$s = %10$s
                  AND %1$s.%8$s = %11$s
                  AND %2$s.%9$s = %12$s
            ',
                    MethodTables::TABLE_PAYMENT_METHODS,
                    UserTables::TABLE_PAYMENT_USER_METHODS,
                    PaymentMethod::CODE,
                    PaymentMethod::TITLE,
                    UserPaymentMethods::PAYMENT_METHOD_ID,
                    PaymentMethod::ENABLED,
                    UserPaymentMethods::IS_ASSIGNED,
                    PaymentMethod::PROVIDER_ID,
                    UserPaymentMethods::USER_ID,
                    true,
                    $userProvider['id'],
                    $connection->quote($query->getUserId())
                );

                $userMethods = $connection->query($sqlMethods)->fetchAll();
                $methods     = [];

                foreach ($userMethods as $userMethod) {
                    $methods[] = new Method(
                        $userMethod['code'],
                        $userMethod['title']
                    );
                }

                $providers[] = new Provider(
                    $userProvider['code'],
                    $userProvider['title'],
                    ($userProvider['fee'] === null
                        ? new Fee(
                            $userProvider['default_fee'],
                            $userProvider['default_is_percent'],
                            $userProvider['default_currency']
                        )
                        : new Fee(
                            $userProvider['fee'],
                            $userProvider['is_percent'],
                            $userProvider['currency']
                        )),
                    $methods
                );
            }
        }

        return new RetrieveOrderPaymentDataReadModel($providers);
    }
}

