<?php
namespace Eplane\Payment\Infrastructure\Application\Query\RetrieveProvider\Magento;

use Eplane\Base\Utils\DbUtils;
use Eplane\Payment\Application\Query\RetrieveAllProviders\Fee;
use Eplane\Payment\Application\Query\RetrieveProvider\RetrieveProviderHandlerInterface;
use Eplane\Payment\Application\Query\RetrieveProvider\RetrieveProviderQuery;
use Eplane\Payment\Application\Query\RetrieveProvider\RetrieveProviderReadModel;

class RetrieveProviderHandler implements RetrieveProviderHandlerInterface
{
    /**
     * @param RetrieveProviderQuery $query
     * @return RetrieveProviderReadModel
     * @throws \Zend_Db_Statement_Exception
     */
    public function handle($query)
    {
        $connection = DbUtils::getDefaultConnection();

        $sql = sprintf(
            'SELECT * FROM %1$s WHERE `id` = %2$s',
            \Eplane\Payment\Infrastructure\Domain\Model\Provider\Magento\ResourceModel\Tables::TABLE_PAYMENT_PROVIDERS,
            $query->getProviderId()
        );

        $paymentProvider = $connection->query($sql)->fetchAll(\PDO::FETCH_ASSOC)[0];

        return new RetrieveProviderReadModel(
            $paymentProvider['code'],
            $paymentProvider['title'],
            $paymentProvider['enabled'],
            new Fee(
                $paymentProvider['default_fee'],
                $paymentProvider['is_percent'],
                $paymentProvider['currency']
            )
        );
    }
}
