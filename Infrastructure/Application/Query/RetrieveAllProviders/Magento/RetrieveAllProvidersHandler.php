<?php

namespace Eplane\Payment\Infrastructure\Application\Query\RetrieveAllProviders\Magento;

use Eplane\Base\Utils\DbUtils;
use Eplane\Payment\Application\Query\RetrieveAllProviders\Fee;
use Eplane\Payment\Application\Query\RetrieveAllProviders\RetrieveAllProvidersHandlerInterface;
use Eplane\Payment\Application\Query\RetrieveAllProviders\RetrieveAllProvidersItemReadModel;
use Eplane\Payment\Infrastructure\Domain\Model\Method\Magento\Method;

class RetrieveAllProvidersHandler implements RetrieveAllProvidersHandlerInterface
{
    /**
     * @return RetrieveAllProvidersItemReadModel[]
     * @throws \Zend_Db_Statement_Exception
     */
    public function handle($query = null)
    {
        $connection = DbUtils::getDefaultConnection();

        $sql = sprintf(
            'SELECT * FROM %1$s',
            \Eplane\Payment\Infrastructure\Domain\Model\Provider\Magento\ResourceModel\Tables::TABLE_PAYMENT_PROVIDERS
        );

        $paymentProviders = $connection->query($sql)->fetchAll();

        $result = [];

        foreach ($paymentProviders as $paymentProvider) {
            $methods = [];

            $sql = sprintf(
                'SELECT * FROM %1$s WHERE %1$s.%2$s = %3$s',
                \Eplane\Payment\Infrastructure\Domain\Model\Method\Magento\ResourceModel\Tables::TABLE_PAYMENT_METHODS,
                Method::PROVIDER_ID,
                $paymentProvider['id']
            );

            $paymentMethods = $connection->query($sql)->fetchAll();

            foreach ($paymentMethods as $paymentMethod) {
                $methods[] = new \Eplane\Payment\Application\Query\RetrieveAllProviders\Method(
                    $paymentMethod['code'],
                    $paymentMethod['title'],
                    $paymentMethod['enabled']
                );
            }

            $result[$paymentProvider['code']] = new RetrieveAllProvidersItemReadModel(
                $paymentProvider['code'],
                $paymentProvider['title'],
                $paymentProvider['enabled'],
                new \Eplane\Payment\Application\Query\RetrieveAllProviders\Fee(
                    $paymentProvider['default_fee'],
                    $paymentProvider['is_percent'],
                    $paymentProvider['currency']
                ),
                $methods
            );
        }

        return $result;
    }
}
