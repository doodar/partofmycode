<?php
namespace Eplane\Payment\Infrastructure\Application\Query\RetrieveUser\Magento;

use Eplane\Base\Utils\DbUtils;
use Eplane\Payment\Application\Query\RetrieveUser\RetrieveUserHandlerInterface;
use Eplane\Payment\Application\Query\RetrieveUser\RetrieveUserQuery;
use Eplane\Payment\Application\Query\RetrieveUser\RetrieveUserReadModel;

class RetrieveUserHandler implements RetrieveUserHandlerInterface
{
    /**
     * @param RetrieveUserQuery $query
     * @return RetrieveUserReadModel
     * @throws \Zend_Db_Statement_Exception
     */
    public function handle($query)
    {
        $connection = DbUtils::getDefaultConnection();

        $sql = sprintf(
            'SELECT * FROM %1$s WHERE %2$s = %3$s',
            \Eplane\Payment\Infrastructure\Domain\Model\User\Magento\ResourceModel\Tables::TABLE_PAYMENT_USER,
            \Eplane\Payment\Infrastructure\Domain\Model\User\Magento\UserPayment::USER_ID,
            $query->getUserId()
        );

        $paymentUser = $connection->query($sql)->fetchAll(\PDO::FETCH_ASSOC)[0] ?? null;

        if ($paymentUser) {
            return new RetrieveuserReadModel(
                $paymentUser['user_id'],
                $paymentUser['is_active'],
                $paymentUser['platform_pay'],
                $paymentUser['created_at'],
                $paymentUser['last_modify_at']
            );
        }

        return new RetrieveuserReadModel(
            $query->getUserId(),
            false,
            false,
            '1970-01-01 00:00:00',
            '1970-01-01 00:00:00'
        );
    }
}
