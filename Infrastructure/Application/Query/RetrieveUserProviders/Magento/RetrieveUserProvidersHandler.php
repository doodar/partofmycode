<?php

namespace Eplane\Payment\Infrastructure\Application\Query\RetrieveUserProviders\Magento;

use Eplane\Base\Utils\DbUtils;
use Eplane\Base\Utils\UrlUtils;
use Eplane\Payment\Application\Query\RetrieveUserProviders\RetrieveUserProvidersHandlerInterface;
use Eplane\Payment\Application\Query\RetrieveUserProviders\RetrieveUserProvidersItemReadModel;
use Eplane\Payment\Application\Query\RetrieveUserProviders\RetrieveUserProvidersQuery;
use Eplane\Payment\Domain\ValueObject\Fee;
use Eplane\Payment\Infrastructure\Domain\Model\User\Magento\UserPaymentProviders;
use Eplane\Payment\Infrastructure\Domain\Model\Provider\Magento\Provider;
use Eplane\Payment\Infrastructure\Domain\Model\Method\Magento\Method;
use Eplane\Payment\Infrastructure\Domain\Model\Method\Magento\ResourceModel\Tables as MethodTables;
use Eplane\Payment\Infrastructure\Domain\Model\User\Magento\UserPaymentMethods;
use Eplane\Payment\Infrastructure\Domain\Model\User\Magento\ResourceModel\Tables as UserTables;

class RetrieveUserProvidersHandler implements RetrieveUserProvidersHandlerInterface
{
    public function buildConnectLink($code)
    {
        return UrlUtils::makeUrl($code . '/connect');
    }

    /**
     * @param RetrieveUserProvidersQuery $query
     * @return RetrieveUserProvidersItemReadModel[]
     * @throws \Zend_Db_Statement_Exception
     */
    public function handle($query)
    {
        $connection = DbUtils::getDefaultConnection();

        $result = [];

        $sql = sprintf(
            'SELECT %1$s FROM %2$s WHERE %3$s = %4$s',
            \Eplane\Payment\Infrastructure\Domain\Model\User\Magento\UserPayment::PLATFORM_PAY,
            \Eplane\Payment\Infrastructure\Domain\Model\User\Magento\ResourceModel\Tables::TABLE_PAYMENT_USER,
            \Eplane\Payment\Infrastructure\Domain\Model\User\Magento\UserPayment::USER_ID,
            $connection->quote($query->getUserId())
        );

        $isPlatformUser = ($connection->query($sql)->fetchAll())[0] ?? null;

        if (($isPlatformUser[\Eplane\Payment\Infrastructure\Domain\Model\User\Magento\UserPayment::PLATFORM_PAY] ?? false)) {
            $sql = sprintf(
                'SELECT *
                        FROM %1$s
                        WHERE %2$s IN (SELECT DISTINCT %3$s FROM %4$s WHERE %5$s = %6$s) AND %7$s = %6$s',
                \Eplane\Payment\Infrastructure\Domain\Model\Provider\Magento\ResourceModel\Tables::TABLE_PAYMENT_PROVIDERS,
                \Eplane\Payment\Infrastructure\Domain\Model\Provider\Magento\Provider::ID,
                \Eplane\Payment\Infrastructure\Domain\Model\Method\Magento\Method::PROVIDER_ID,
                \Eplane\Payment\Infrastructure\Domain\Model\Method\Magento\ResourceModel\Tables::TABLE_PAYMENT_METHODS,
                \Eplane\Payment\Infrastructure\Domain\Model\Method\Magento\Method::PLATFORM_ENABLED,
                true,
                \Eplane\Payment\Infrastructure\Domain\Model\Provider\Magento\Provider::ENABLED
            );

            $platformProviders = $connection->query($sql)->fetchAll();

            foreach ($platformProviders as $platformProvider) {
                $fee        = null;
                $defaultFee = new Fee(
                    $platformProvider['default_fee'],
                    $platformProvider['is_percent'],
                    $platformProvider['currency']
                );

                $sqlMethods = sprintf(
                    'SELECT * FROM %1$s WHERE %2$s = %3$s AND %4$s = %5$s AND %6$s = %5$s',
                    \Eplane\Payment\Infrastructure\Domain\Model\Method\Magento\ResourceModel\Tables::TABLE_PAYMENT_METHODS,
                    \Eplane\Payment\Infrastructure\Domain\Model\Method\Magento\Method::PROVIDER_ID,
                    $platformProvider[\Eplane\Payment\Infrastructure\Domain\Model\Provider\Magento\Provider::ID],
                    \Eplane\Payment\Infrastructure\Domain\Model\Method\Magento\Method::PLATFORM_ENABLED,
                    true,
                    \Eplane\Payment\Infrastructure\Domain\Model\Method\Magento\Method::ENABLED
                );

                $platformMethods = $connection->query($sqlMethods)->fetchAll();

                $methods = [];

                foreach ($platformMethods as $platformMethod) {
                    $methods[] = new \Eplane\Payment\Application\Query\RetrieveUserProviders\Method(
                        $platformMethod['code'],
                        $platformMethod['title'],
                        true
                    );
                }

                $userProviderSql = sprintf(
                    'SELECT *
                        FROM %1$s
                        WHERE %2$s = %3$s AND %4$s = %5$s',
                    \Eplane\Payment\Infrastructure\Domain\Model\User\Magento\ResourceModel\Tables::TABLE_PAYMENT_USER_PROVIDERS,
                    \Eplane\Payment\Infrastructure\Domain\Model\User\Magento\UserPaymentProviders::USER_ID,
                    $connection->quote($query->getUserId()),
                    \Eplane\Payment\Infrastructure\Domain\Model\User\Magento\UserPaymentProviders::PAYMENT_PROVIDER_ID,
                    $platformProvider[\Eplane\Payment\Infrastructure\Domain\Model\Provider\Magento\Provider::ID]
                );

                $userProvider = $connection->query($userProviderSql)->fetchAll()[0] ?? null;

                $userProviderIsAssigned = $userProvider[\Eplane\Payment\Infrastructure\Domain\Model\User\Magento\UserPaymentProviders::IS_ASSIGNED] ?? false;

                $result[$platformProvider['code']] = new RetrieveUserProvidersItemReadModel(
                    $platformProvider['code'],
                    $platformProvider['title'],
                    $fee,
                    $defaultFee,
                    $userProviderIsAssigned,
                    (!$userProviderIsAssigned ? $this->buildConnectLink($platformProvider['code']) : null),
                    empty($userProvider['last_modify_at']) ? null : $userProvider['last_modify_at'],
                    $methods
                );
            }
        } else {
            $sql = sprintf(
                'SELECT %1$s.%9$s, 
                            %1$s.%4$s as `provider_id`,
                            %1$s.%10$s, 
                            %2$s.%6$s, 
                            %2$s.%8$s, 
                            %2$s.%11$s as `fee`, 
                            %2$s.%12$s as `is_percent`, 
                            %2$s.%13$s as `currency`,
                            %1$s.%16$s as `default_fee`,
                            %1$s.%17$s as `default_is_percent`,
                            %1$s.%18$s as `default_currency`
                    FROM %1$s 
                    LEFT OUTER JOIN %2$s 
                    ON %2$s.%3$s = %1$s.%4$s  AND %2$s.%14$s = %15$s
                    WHERE %1$s.%5$s = %7$s',
                \Eplane\Payment\Infrastructure\Domain\Model\Provider\Magento\ResourceModel\Tables::TABLE_PAYMENT_PROVIDERS,
                \Eplane\Payment\Infrastructure\Domain\Model\User\Magento\ResourceModel\Tables::TABLE_PAYMENT_USER_PROVIDERS,
                UserPaymentProviders::PAYMENT_PROVIDER_ID,
                Provider::ID,
                Provider::ENABLED,
                UserPaymentProviders::IS_ASSIGNED,
                true,
                UserPaymentProviders::LAST_MODIFY_AT,
                Provider::CODE,
                Provider::TITLE,
                UserPaymentProviders::FEE,
                UserPaymentProviders::IS_PERCENT,
                UserPaymentProviders::CURRENCY,
                UserPaymentProviders::USER_ID,
                $connection->quote($query->getUserId()),
                Provider::DEFAULT_FEE,
                Provider::IS_PERCENT,
                Provider::CURRENCY
            );

            $userProviders = $connection->query($sql)->fetchAll();

            foreach ($userProviders as $userProvider) {
                $fee = (null === $userProvider['fee']
                    ? null
                    : new Fee(
                        $userProvider['fee'],
                        $userProvider['is_percent'],
                        $userProvider['currency']
                    ));

                $defaultFee = new Fee(
                    $userProvider['default_fee'],
                    $userProvider['default_is_percent'],
                    $userProvider['default_currency']
                );

                $sqlMethods = sprintf(
                    'SELECT %1$s.%11$s, %6$s.%9$s, %6$s.%10$s FROM %1$s LEFT JOIN %6$s ON %6$s.%7$s = %1$s.%8$s WHERE %2$s = %3$s AND %4$s = %5$s',
                    UserTables::TABLE_PAYMENT_USER_METHODS,
                    UserPaymentMethods::PAYMENT_PROVIDER_ID,
                    $connection->quote($userProvider['provider_id']),
                    UserPaymentMethods::USER_ID,
                    $connection->quote($query->getUserId()),
                    MethodTables::TABLE_PAYMENT_METHODS,
                    Method::ID,
                    UserPaymentMethods::PAYMENT_METHOD_ID,
                    Method::CODE,
                    Method::TITLE,
                    UserPaymentMethods::IS_ASSIGNED
                );

                $userMethods = $connection->query($sqlMethods)->fetchAll();

                $methods = [];

                foreach ($userMethods as $userMethod) {
                    $methods[] = new \Eplane\Payment\Application\Query\RetrieveUserProviders\Method(
                        $userMethod['code'],
                        $userMethod['title'],
                        $userMethod['is_assigned']
                    );
                }

                $result[$userProvider['code']] = new RetrieveUserProvidersItemReadModel(
                    $userProvider['code'],
                    $userProvider['title'],
                    $fee,
                    $defaultFee,
                    (bool)$userProvider['is_assigned'],
                    (!((bool)$userProvider['is_assigned']) ? $this->buildConnectLink($userProvider['code']) : null),
                    (empty((string)$userProvider['last_modify_at']) ? null : ((string)$userProvider['last_modify_at'])),
                    $methods
                );
            }
        }

        return $result;
    }
}
