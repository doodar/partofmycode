<?php

namespace Eplane\Payment\Infrastructure\Application\Query\RetrievePayment\Magento;

use Eplane\Base\Php\BigNumber\Decimal;
use Eplane\Base\Utils\DbUtils;
use Eplane\Payment\Application\Query\RetrievePayment\RetrievePaymentHandlerInterface;
use Eplane\Payment\Application\Query\RetrievePayment\RetrievePaymentQuery;
use Eplane\Payment\Application\Query\RetrievePayment\RetrievePaymentReadModel;
use Eplane\Payment\Infrastructure\Domain\Model\Payment\Magento\Payment;
use Eplane\Payment\Infrastructure\Domain\Model\Payment\Magento\ResourceModel\Tables;

class RetrievePaymentHandler implements RetrievePaymentHandlerInterface
{
    /**
     * @param RetrievePaymentQuery $query
     * @throws \Zend_Db_Statement_Exception
     */
    public function handle($query)
    {
        $connection = DbUtils::getDefaultConnection();

        $sql = sprintf(
            'SELECT * FROM %1$s WHERE %1$s.%2$s = %3$s',
            Tables::TABLE_PAYMENT,
            Payment::ID,
            $query->getPaymentId()

        );

        $payment = $connection->query($sql)->fetch();
        return new RetrievePaymentReadModel(
            $payment['id'],
            $payment['order_id'],
            $payment['request_id'],
            $payment['seller_id'],
            $payment['buyer_id'],
            $payment['customer_id'],
            $payment['message_id'],
            Decimal::create($payment['price']),
            $payment['fee'] ? (float)$payment['fee'] : null,
            $payment['provider'],
            $payment['method'],
            $payment['payment_data_encoded'],
            $payment['currency'],
            $payment['is_platform']
        );
    }
}
