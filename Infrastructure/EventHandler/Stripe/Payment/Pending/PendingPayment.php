<?php
namespace Eplane\Payment\Infrastructure\EventHandler\Stripe\Payment\Pending;

use Eplane\ApiCore\Infrastructure\Domain\Event\EventHandlerInterface;
use Eplane\ApiCore\Service\Stripe\Event\Payment\Pending;
use Eplane\Payment\Application\Command\PendingPayment\PendingPaymentCommand;
use Eplane\Payment\Application\Command\PendingPayment\PendingPaymentHandlerInterface;
use Eplane\Payment\Application\Exception\CommandInvalidParameters;
use Eplane\Payment\Infrastructure\Logger\Magento\LoggerAwareTrait;

/** @noinspection PhpUnused */
class PendingPayment implements EventHandlerInterface
{
    use LoggerAwareTrait;

    /**
     * @var PendingPaymentHandlerInterface
     */
    private $pendingPaymentHandler;

    public function __construct(PendingPaymentHandlerInterface $pendingPaymentHandler)
    {
        $this->pendingPaymentHandler = $pendingPaymentHandler;
        $this->setLogger();
    }

    /**
     * @return Pending
     */
    public function getEventObject()
    {
        return new Pending();
    }

    /**
     * @param Pending $event
     * @throws CommandInvalidParameters
     */
    public function handle($event)
    {
        $this->logger->info(
            sprintf('Payment %s pending',
                $event->getId()
            )
        );

        $command = new PendingPaymentCommand($event->getId(), $event->getPaymentDataUpdate());
        $this->pendingPaymentHandler->handle($command);
    }
}
