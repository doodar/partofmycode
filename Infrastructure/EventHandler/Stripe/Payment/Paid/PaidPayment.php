<?php
namespace Eplane\Payment\Infrastructure\EventHandler\Stripe\Payment\Paid;

use Eplane\ApiCore\Infrastructure\Domain\Event\EventHandlerInterface;
use Eplane\ApiCore\Service\Stripe\Event\Payment\Paid;
use Eplane\Payment\Application\Command\PaidPayment\PaidPaymentCommand;
use Eplane\Payment\Application\Command\PaidPayment\PaidPaymentHandlerInterface;
use Eplane\Payment\Application\Exception\CommandInvalidParameters;
use Eplane\Payment\Infrastructure\Logger\Magento\LoggerAwareTrait;

/** @noinspection PhpUnused */
class PaidPayment implements EventHandlerInterface
{
    use LoggerAwareTrait;

    /**
     * @var PaidPaymentHandlerInterface
     */
    private $paidPaymentHandler;

    public function __construct(PaidPaymentHandlerInterface $paidPaymentHandler)
    {
        $this->paidPaymentHandler = $paidPaymentHandler;
        $this->setLogger();
    }

    /**
     * @return Paid
     */
    public function getEventObject()
    {
        return new Paid();
    }

    /**
     * @param Paid $event
     * @throws CommandInvalidParameters
     */
    public function handle($event)
    {
        $this->logger->info(
            sprintf('Payment %s paid',
                $event->getId()
            )
        );


        $command = new PaidPaymentCommand($event->getId());

            $this->paidPaymentHandler->handle($command);
    }
}
