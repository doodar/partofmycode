<?php
namespace Eplane\Payment\Infrastructure\EventHandler\Stripe\Payment\Failed;

use Eplane\ApiCore\Infrastructure\Domain\Event\EventHandlerInterface;
use Eplane\ApiCore\Service\Stripe\Event\Payment\Failed;
use Eplane\Payment\Application\Command\FailPayment\FailPaymentCommand;
use Eplane\Payment\Application\Command\FailPayment\FailPaymentHandlerInterface;
use Eplane\Payment\Application\Exception\CommandInvalidParameters;
use Eplane\Payment\Infrastructure\Logger\Magento\LoggerAwareTrait;

/** @noinspection PhpUnused */
class FailPayment implements EventHandlerInterface
{
    use LoggerAwareTrait;

    /**
     * @var FailPaymentHandlerInterface
     */
    private $failPaymentHandler;

    public function __construct(FailPaymentHandlerInterface $failPaymentHandler)
    {
        $this->failPaymentHandler = $failPaymentHandler;
        $this->setLogger();
    }

    /**
     * @return Failed
     */
    public function getEventObject()
    {
        return new Failed();
    }

    /**
     * @param Failed $event
     * @throws CommandInvalidParameters
     */
    public function handle($event)
    {
        $this->logger->alert(
            sprintf('Payment %s failed. Reason: %s',
                $event->getId(), $event->getReason()
            )
        );

        $command = new FailPaymentCommand($event->getId(), $event->getReason());
        $this->failPaymentHandler->handle($command);
    }
}
