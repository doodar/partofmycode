<?php
namespace Eplane\Payment\Infrastructure\EventHandler\Stripe\User\Connected;

use Eplane\ApiCore\Infrastructure\Domain\Event\EventHandlerInterface;
use Eplane\ApiCore\Service\Stripe\Event\Seller\Connected;
use Eplane\Payment\Infrastructure\Logger\Magento\LoggerAwareTrait;
use Eplane\Payment\Application\Command\AssignProviderToUser\AssignProviderToUserCommand;
use Eplane\Payment\Application\Command\AssignProviderToUser\AssignProviderToUserHandlerInterface;

/** @noinspection PhpUnused */
class AssignProviderToUser implements EventHandlerInterface
{
    use LoggerAwareTrait;

    /**
     * @var AssignProviderToUserHandlerInterface
     */
    private $assignProviderToUserHandler;

    public function __construct(AssignProviderToUserHandlerInterface $assignProviderToUserHandler)
    {
        $this->assignProviderToUserHandler = $assignProviderToUserHandler;

        $this->setLogger();
    }

    /**
     * @return Connected
     */
    public function getEventObject()
    {
        return new Connected();
    }

    /**
     * @param Connected $event
     */
    public function handle($event)
    {
        $userId      = $event->getUserId();
        $providerCode = $event->getProviderCode();
        $connectedAt = $event->getConnectedAt()->toDateTime();

        try {
            $command = new AssignProviderToUserCommand(
                $providerCode,
                $userId,
                $connectedAt
            );

            $this->assignProviderToUserHandler->handle($command);
        } catch (\Exception $exception){
            $this->logger->alert($exception);
        }
    }
}
