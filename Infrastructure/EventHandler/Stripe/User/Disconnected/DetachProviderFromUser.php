<?php
namespace Eplane\Payment\Infrastructure\EventHandler\Stripe\User\Disconnected;

use Eplane\ApiCore\Infrastructure\Domain\Event\EventHandlerInterface;
use Eplane\ApiCore\Service\Stripe\Event\Seller\Disconnected;
use Eplane\Payment\Infrastructure\Logger\Magento\LoggerAwareTrait;
use Eplane\Payment\Application\Command\DetachProviderFromUser\DetachProviderFromUserCommand;
use Eplane\Payment\Application\Command\DetachProviderFromUser\DetachProviderFromUserHandlerInterface;

/** @noinspection PhpUnused */
class DetachProviderFromUser implements EventHandlerInterface
{
    use LoggerAwareTrait;

    /**
     * @var DetachProviderFromUserHandlerInterface
     */
    private $detachProviderFromUserHandler;

    public function __construct(DetachProviderFromUserHandlerInterface $detachProviderFromUser)
    {
        $this->detachProviderFromUserHandler = $detachProviderFromUser;

        $this->setLogger();
    }

    /**
     * @return Disconnected
     */
    public function getEventObject()
    {
        return new Disconnected();
    }

    /**
     * @param Disconnected $event
     */
    public function handle($event)
    {
        $userId      = $event->getUserId();
        $providerCode = $event->getProviderCode();
        $disconnectedAt = $event->getDisconnectedAt()->toDateTime();

        try {
            $command = new DetachProviderFromUserCommand(
                $providerCode,
                $userId,
                $disconnectedAt
            );

            $this->detachProviderFromUserHandler->handle($command);
        } catch (\Exception $exception){
            $this->logger->alert($exception);
        }
    }
}
