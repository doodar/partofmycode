<?php
namespace Eplane\Payment\Infrastructure\EventHandler\Users\UserSettings;

use Eplane\ApiCore\Infrastructure\Domain\Event\EventHandlerInterface;
use Eplane\ApiCore\Service\Users\Event\UserSettings\PlatformPayModified as ProtoPlatformPayModified;
use Eplane\Payment\Application\Command\PlatformPayModified\PlatformPayModifiedCommand;
use Eplane\Payment\Application\Command\PlatformPayModified\PlatformPayModifiedHandlerInterface;
use Eplane\Payment\Infrastructure\Logger\Magento\LoggerAwareTrait;

/** @noinspection PhpUnused */
class PlatformPayModified implements EventHandlerInterface
{
    use LoggerAwareTrait;

    /**
     * @var PlatformPayModifiedHandlerInterface
     */
    private $platformPayModifiedHandler;

    public function __construct(PlatformPayModifiedHandlerInterface $platformPayModifiedHandler)
    {
        $this->platformPayModifiedHandler = $platformPayModifiedHandler;

        $this->setLogger();
    }

    /**
     * @return ProtoPlatformPayModified
     */
    public function getEventObject()
    {
        return new ProtoPlatformPayModified();
    }

    /**
     * @param ProtoPlatformPayModified $event
     */
    public function handle($event)
    {
        $userId = $event->getUserId();
        $platformPay = $event->getPlatformPay();

        try {
            $command = new PlatformPayModifiedCommand(
                $userId,
                $platformPay
            );

            $this->platformPayModifiedHandler->handle($command);
        } catch (\Exception $exception){
            $this->logger->alert($exception);
        }
    }
}
