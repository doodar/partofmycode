<?php
namespace Eplane\Payment\Infrastructure\EventHandler\Sales\PaymentRequest\Created;

use Eplane\ApiCore\Infrastructure\Domain\Event\EventHandlerInterface;
use Eplane\ApiCore\Service\Sales\Event\PaymentRequest\Created;
use Eplane\Base\Php\BigNumber\Decimal;
use Eplane\Payment\Application\Command\ConfirmPaymentRequest\ConfirmPaymentRequestCommand;
use Eplane\Payment\Application\Command\ConfirmPaymentRequest\ConfirmPaymentRequestHandlerInterface;
use Eplane\Payment\Infrastructure\Logger\Magento\LoggerAwareTrait;

/** @noinspection PhpUnused */
class ConfirmPaymentRequest implements EventHandlerInterface
{
    use LoggerAwareTrait;

    /**
     * @var ConfirmPaymentRequestHandlerInterface
     */
    private $confirmPaymentRequestHandler;

    public function __construct(
        ConfirmPaymentRequestHandlerInterface $confirmPaymentRequestHandler
    ) {
        $this->confirmPaymentRequestHandler = $confirmPaymentRequestHandler;
        $this->setLogger();
    }

    /**
     * @return Created
     */
    public function getEventObject()
    {
        return new Created();
    }

    /**
     * @param Created $event
     */
    public function handle($event)
    {
        $total        = $event->getTotal();
        $totalDecimal = Decimal::create($total->getAmount()->getValue(), $total->getAmount()->getPrecision());

        $this->logger->info(
            sprintf('Payment request %s (%s, %s %s) to be confirmed',
                $event->getRequestId(),
                $event->getRfqIncrementId(),
                $total->getAmount()->getValue(),
                $total->getCurrency()
            )
        );


        $this->confirmPaymentRequestHandler->handle(
            new ConfirmPaymentRequestCommand(
                $event->getRequestId(),
                $event->getRfqIncrementId(),
                $event->getUserId(),
                $event->getCustomerId(),
                $totalDecimal,
                $total->getCurrency()
            )
        );
    }
}
