<?php
namespace Eplane\Payment\Infrastructure\EventHandler\Sales\Payment\Created;

use Eplane\ApiCore\Infrastructure\Domain\Event\EventHandlerInterface;
use Eplane\ApiCore\Service\Sales\Event\Payment\Created;
use Eplane\Payment\Application\Command\CreatePayment\CreatePaymentCommand;
use Eplane\Payment\Application\Command\CreatePayment\CreatePaymentHandlerInterface;
use Eplane\Payment\Application\Exception\CommandInvalidParameters;
use Eplane\Payment\Infrastructure\Logger\Magento\LoggerAwareTrait;

/** @noinspection PhpUnused */
class CreatePayment implements EventHandlerInterface
{
    use LoggerAwareTrait;

    /**
     * @var CreatePaymentHandlerInterface
     */
    private $createPaymentHandler;

    public function __construct(CreatePaymentHandlerInterface $createPaymentHandler)
    {
        $this->createPaymentHandler = $createPaymentHandler;
        $this->setLogger();
    }

    /**
     * @return Created
     */
    public function getEventObject()
    {
        return new Created();
    }

    /**
     * @param Created $event
     * @throws CommandInvalidParameters
     */
    public function handle($event)
    {
        $this->logger->info(
            sprintf('Payment %s on payment request %s (order %s) to be created with %s %s',
                $event->getPaymentId(),
                $event->getPaymentRequestId(),
                $event->getOrderId(),
                $event->getProvider(),
                $event->getMethod()
            )
        );

        $command = new CreatePaymentCommand(
            $event->getPaymentId(),
            $event->getPaymentRequestId(),
            $event->getOrderId(),
            $event->getPaymentDataEncoded(),
            $event->getProvider(),
            $event->getMethod(),
            $event->getSellerId(),
            $event->getBuyerId(),
            $event->getCustomerId(),
            $event->getMessageId()
        );

        $this->createPaymentHandler->handle($command);
    }
}
