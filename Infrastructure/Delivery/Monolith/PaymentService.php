<?php

namespace Eplane\Payment\Infrastructure\Delivery\Monolith;

use Eplane\ApiCore\Common\Message\Decimal;
use Eplane\ApiCore\Common\Message\Fee;
use Eplane\ApiCore\Common\Message\Money;
use Eplane\ApiCore\Common\Message\Result;
use Eplane\ApiCore\Service\Payment\Message\DetachMethodFromUser\DetachMethodFromUserRequest\UserMethod;
use Eplane\ApiCore\Service\Payment\Message\ResetUserProviderFeeToDefault\ResetUserProviderFeeToDefaultRequest;
use Eplane\ApiCore\Service\Payment\Message\ResetUserProviderFeeToDefault\ResetUserProviderFeeToDefaultResponse;
use Eplane\ApiCore\Service\Payment\Message\RetrieveOrderPaymentData\RetrieveOrderPaymentDataRequest;
use Eplane\ApiCore\Service\Payment\Message\RetrieveOrderPaymentData\RetrieveOrderPaymentDataResponse\Method;
use Eplane\ApiCore\Service\Payment\Message\RetrievePayment\RetrievePaymentRequest;
use Eplane\ApiCore\Service\Payment\Message\RetrievePayment\RetrievePaymentResponse;
use Eplane\ApiCore\Service\Payment\Message\RetrieveProvider\RetrieveProviderRequest;
use Eplane\ApiCore\Service\Payment\Message\RetrieveProvider\RetrieveProviderResponse;
use Eplane\ApiCore\Service\Payment\Message\RetrieveUserProviders\RetrieveUserProvidersRequest;
use Eplane\ApiCore\Service\Payment\Message\RetrieveUserProviders\RetrieveUserProvidersResponse;
use Eplane\ApiCore\Service\Payment\Message\RetrieveUserProviders\RetrieveUserProvidersResponse\Provider;
use Eplane\ApiCore\Service\Payment\PaymentServiceInterface;
use Eplane\Payment\Application\Command\AssignMethodToUser\AssignMethodToUserCommand;
use Eplane\Payment\Application\Command\AssignMethodToUser\AssignMethodToUserHandlerInterface;
use Eplane\Payment\Application\Command\DetachMethodFromUser\DetachMethodFromUserCommand;
use Eplane\Payment\Application\Command\DetachMethodFromUser\DetachMethodFromUserHandlerInterface;
use Eplane\Payment\Application\Command\ResetUserProviderFeeToDefault\ResetUserProviderFeeToDefaultCommand;
use Eplane\Payment\Application\Command\ResetUserProviderFeeToDefault\ResetUserProviderFeeToDefaultHandlerInterface;
use Eplane\Payment\Application\Query\RetrievePayment\RetrievePaymentQuery;
use Eplane\Payment\Application\Query\RetrieveProvider\RetrieveProviderHandlerInterface;
use Eplane\Payment\Application\Query\RetrieveProvider\RetrieveProviderQuery;
use Eplane\Payment\Application\Query\RetrievePayment\RetrievePaymentHandlerInterface;
use Eplane\Payment\Application\Query\RetrieveUser\RetrieveUserHandlerInterface;
use Eplane\Payment\Application\Query\RetrieveUser\RetrieveUserQuery;
use Eplane\Payment\Application\Query\RetrieveUserProviders\RetrieveUserProvidersHandlerInterface;
use Eplane\Payment\Application\Query\RetrieveUserProviders\RetrieveUserProvidersQuery;
use Eplane\Base\Consts\ErrorCodes;
use Eplane\ApiCore\Service\Payment\Message\RetrieveAllProviders\RetrieveAllProvidersResponse;
use Eplane\ApiCore\Service\Payment\Message\RetrieveAllProviders\RetrieveAllProvidersResponse\Provider as AllProviders;
use Eplane\Payment\Application\Query\RetrieveAllProviders\RetrieveAllProvidersHandlerInterface;
use Eplane\ApiCore\Service\Payment\Message\RegisterProvider\RegisterProviderRequest;
use Eplane\ApiCore\Service\Payment\Message\RegisterProvider\RegisterProviderResponse;
use Eplane\ApiCore\Service\Payment\Message\RegisterProvider\RegisterProviderResponse\Provider as RegisterProvider;
use Eplane\Payment\Application\Command\RegisterProvider\RegisterProviderCommand;
use Eplane\Payment\Application\Command\RegisterProvider\RegisterProviderHandlerInterface;
use Eplane\ApiCore\Service\Payment\Message\RegisterMethod\RegisterMethodRequest;
use Eplane\ApiCore\Service\Payment\Message\RegisterMethod\RegisterMethodResponse;
use Eplane\ApiCore\Service\Payment\Message\RegisterMethod\RegisterMethodResponse\Method as RegisterMethod;
use Eplane\Payment\Application\Command\RegisterMethod\RegisterMethodCommand;
use Eplane\Payment\Application\Command\RegisterMethod\RegisterMethodHandlerInterface;
use Eplane\ApiCore\Service\Payment\Message\AssignMethodToUser\AssignMethodToUserRequest;
use Eplane\ApiCore\Service\Payment\Message\AssignMethodToUser\AssignMethodToUserResponse;
use Eplane\ApiCore\Service\Payment\Message\AssignMethodToUser\AssignMethodToUserResponse\UserMethod as AssignUserMethod;
use Eplane\ApiCore\Service\Payment\Message\DetachMethodFromUser\DetachMethodFromUserRequest;
use Eplane\ApiCore\Service\Payment\Message\DetachMethodFromUser\DetachMethodFromUserResponse;
use Eplane\ApiCore\Service\Payment\Message\DetachMethodFromUser\DetachMethodFromUserResponse\UserMethod as DetachUserMethod;
use Eplane\ApiCore\Service\Payment\Message\ChangeProviderFee\ChangeProviderFeeRequest;
use Eplane\ApiCore\Service\Payment\Message\ChangeProviderFee\ChangeProviderFeeResponse;
use Eplane\ApiCore\Service\Payment\Message\ChangeProviderFee\ChangeProviderFeeResponse\Provider as ChangeFeeProvider;
use Eplane\Payment\Application\Command\ChangeProviderFee\ChangeProviderFeeCommand;
use Eplane\Payment\Application\Command\ChangeProviderFee\ChangeProviderFeeHandlerInterface;
use Eplane\ApiCore\Service\Payment\Message\ChangeUserProviderFee\ChangeUserProviderFeeRequest;
use Eplane\ApiCore\Service\Payment\Message\ChangeUserProviderFee\ChangeUserProviderFeeResponse;
use Eplane\ApiCore\Service\Payment\Message\ChangeUserProviderFee\ChangeUserProviderFeeResponse\Provider as ChangeFeeUserProvider;
use Eplane\Payment\Application\Command\ChangeUserProviderFee\ChangeUserProviderFeeCommand;
use Eplane\Payment\Application\Command\ChangeUserProviderFee\ChangeUserProviderFeeHandlerInterface;
use Eplane\Payment\Application\Query\RetrieveOrderPaymentData\RetrieveOrderPaymentDataQuery;
use Eplane\Payment\Application\Query\RetrieveOrderPaymentData\RetrieveOrderPaymentDataHandlerInterface;
use Eplane\ApiCore\Service\Payment\Message\RetrieveOrderPaymentData\RetrieveOrderPaymentDataResponse;
use Google\Protobuf\GPBEmpty;

/** @noinspection PhpUnused */

class PaymentService implements PaymentServiceInterface
{
    /**
     * @var RetrieveUserProvidersHandlerInterface
     */
    private $retrieveUserProvidersHandler;

    /**
     * @var RetrieveProviderHandlerInterface
     */
    private $retrieveProviderHandler;

    /**
     * @var RetrieveAllProvidersHandlerInterface
     */
    private $retrieveAllProvidersHandler;

    /**
     * @var RegisterProviderHandlerInterface
     */
    private $registerProvidersHandler;

    /**
     * @var RegisterMethodHandlerInterface
     */
    private $registerMethodsHandler;

    /**
     * @var AssignMethodToUserHandlerInterface
     */
    private $assignMethodToUserHandler;

    /**
     * @var DetachMethodFromUserHandlerInterface
     */
    private $detachMethodFromUserHandler;

    /**
     * @var ChangeProviderFeeHandlerInterface
     */
    private $changeProviderFeeHandler;

    /**
     * @var ChangeUserProviderFeeHandlerInterface
     */
    private $changeUserProviderFeeHandler;

    /**
     * @var RetrieveOrderPaymentDataHandlerInterface
     */
    private $retrieveOrderPaymentDataHandler;

    /**
     * @var RetrievePaymentHandlerInterface
     */
    private $retrievePaymentHandler;

    /**
     * @var ResetUserProviderFeeToDefaultHandlerInterface
     */
    private $resetUserProviderFeeToDefaultHandler;

    /**
     * @var RetrieveUserHandlerInterface
     */
    private $retrieveUserHandler;

    public function __construct(
        RetrieveProviderHandlerInterface $retrieveProviderHandler,
        RetrieveUserProvidersHandlerInterface $retrieveUserProvidersHandler,
        RetrieveAllProvidersHandlerInterface $retrieveAllProvidersHandler,
        RegisterProviderHandlerInterface $registerProvidersHandler,
        RegisterMethodHandlerInterface $registerMethodsHandler,
        AssignMethodToUserHandlerInterface $assignMethodToUserHandler,
        DetachMethodFromUserHandlerInterface $detachMethodFromUserHandler,
        ChangeProviderFeeHandlerInterface $changeProviderFeeHandler,
        ChangeUserProviderFeeHandlerInterface $changeUserProviderFeeHandler,
        RetrieveOrderPaymentDataHandlerInterface $retrieveOrderPaymentDataHandler,
        RetrievePaymentHandlerInterface $retrievePaymentHandler,
        ResetUserProviderFeeToDefaultHandlerInterface $resetUserProviderFeeToDefaultHandler,
        RetrieveUserHandlerInterface $retrieveUserHandler
    ) {
        $this->retrieveProviderHandler       = $retrieveProviderHandler;
        $this->retrieveUserProvidersHandler  = $retrieveUserProvidersHandler;
        $this->retrieveAllProvidersHandler   = $retrieveAllProvidersHandler;
        $this->registerProvidersHandler      = $registerProvidersHandler;
        $this->registerMethodsHandler        = $registerMethodsHandler;
        $this->assignMethodToUserHandler     = $assignMethodToUserHandler;
        $this->detachMethodFromUserHandler   = $detachMethodFromUserHandler;
        $this->changeProviderFeeHandler      = $changeProviderFeeHandler;
        $this->changeUserProviderFeeHandler  = $changeUserProviderFeeHandler;
        $this->retrieveOrderPaymentDataHandler  = $retrieveOrderPaymentDataHandler;
        $this->retrievePaymentHandler = $retrievePaymentHandler;
        $this->resetUserProviderFeeToDefaultHandler = $resetUserProviderFeeToDefaultHandler;
        $this->retrieveUserHandler = $retrieveUserHandler;
    }

    /**
     * @param RegisterProviderRequest $request
     * @return RegisterProviderResponse
     */
    public function registerProvider(RegisterProviderRequest $request): RegisterProviderResponse
    {
        try {
            $resultProviders = [];

            /** @var RegisterProviderRequest\Provider[] $providers */
            $providers = $request->getProviders();

            foreach ($providers as $provider) {
                $command = new RegisterProviderCommand(
                    $provider->getCode(),
                    $provider->getTitle(),
                    $provider->getEnabled(),
                    $provider->getDefaultFee()->getAmount(),
                    $provider->getDefaultFee()->getIsPercent(),
                    $provider->getDefaultFee()->getCurrency()
                );

                $aggregateId = $this->registerProvidersHandler->handle($command);

                if ($aggregateId) {
                    $resultProviders[] = (new RegisterProvider())
                        ->setId($aggregateId)
                        ->setCode($provider->getCode())
                        ->setTitle($provider->getTitle())
                        ->setEnabled($provider->getEnabled())
                        ->setDefaultFee(
                            (new Fee())
                                ->setAmount($provider->getDefaultFee()->getAmount())
                                ->setIsPercent($provider->getDefaultFee()->getIsPercent())
                                ->setCurrency($provider->getDefaultFee()->getCurrency())
                        );
                }
            }
        } catch (\Exception $e) {
            return (new RegisterProviderResponse())->setResult(
                (new Result())
                    ->setCode(ErrorCodes::ERR_API_BASE)
                    ->setMessage($e->getMessage())
                    ->setTrace($e->getTraceAsString())
                    ->setDetails([])
            );
        }

        return (new RegisterProviderResponse())->setProviders($resultProviders);
    }

    /**
     * @param ChangeProviderFeeRequest $request
     * @return ChangeProviderFeeResponse
     */
    public function changeProviderFee(ChangeProviderFeeRequest $request): ChangeProviderFeeResponse
    {
        try {
            $changedProviders = [];

            /** @var ChangeProviderFeeRequest\Provider[] $providers */
            $providers = $request->getProviders();

            foreach ($providers as $provider) {
                $command = new ChangeProviderFeeCommand(
                    $provider->getCode(),
                    $provider->getFee()->getAmount(),
                    $provider->getFee()->getIsPercent(),
                    $provider->getFee()->getCurrency()
                );

                $this->changeProviderFeeHandler->handle($command);

                $changedProviders[] = (new ChangeFeeProvider())
                    ->setCode($provider->getCode())
                    ->setFee($provider->getFee());
            }
        } catch (\Exception $e) {
            return (new ChangeProviderFeeResponse())->setResult(
                (new Result())
                    ->setCode(ErrorCodes::ERR_API_BASE)
                    ->setMessage($e->getMessage())
                    ->setTrace($e->getTraceAsString())
                    ->setDetails([])
            );
        }

        return (new ChangeProviderFeeResponse())->setProviders($changedProviders);
    }

    /**
     * @param ChangeUserProviderFeeRequest $request
     * @return ChangeUserProviderFeeResponse
     */
    public function changeUserProviderFee(ChangeUserProviderFeeRequest $request): ChangeUserProviderFeeResponse
    {
        try {
            $changedProviders = [];

            /** @var ChangeUserProviderFeeRequest\Provider[] $providers */
            $providers = $request->getProviders();

            foreach ($providers as $provider) {
                $command = new ChangeUserProviderFeeCommand(
                    $provider->getProviderCode(),
                    $provider->getUserId(),
                    $provider->getFee()->getAmount(),
                    $provider->getFee()->getIsPercent(),
                    $provider->getFee()->getCurrency()
                );

                $this->changeUserProviderFeeHandler->handle($command);

                $changedProviders[] = (new ChangeFeeUserProvider())
                    ->setProviderCode($provider->getProviderCode())
                    ->setUserId($provider->getUserId())
                    ->setFee($provider->getFee());
            }
        } catch (\Exception $e) {
            return (new ChangeUserProviderFeeResponse())->setResult(
                (new Result())
                    ->setCode(ErrorCodes::ERR_API_BASE)
                    ->setMessage($e->getMessage())
                    ->setTrace($e->getTraceAsString())
                    ->setDetails([])
            );
        }

        return (new ChangeUserProviderFeeResponse())->setProviders($changedProviders);
    }

    /**
     * @param RetrieveUserProvidersRequest $request
     * @return RetrieveUserProvidersResponse
     */
    public function retrieveUserProviders(RetrieveUserProvidersRequest $request): RetrieveUserProvidersResponse
    {
        try {
            $providers = $this->retrieveUserProvidersHandler->handle(
                new RetrieveUserProvidersQuery(
                    $request->getUserId()
                )
            );

            $resultProviders = [];
            foreach ($providers as $provider) {
                $fee = ($provider->getFee() === null
                    ? null
                    : (new Fee())
                        ->setAmount($provider->getFee()->getAmount())
                        ->setIsPercent($provider->getFee()->getIsPercent())
                        ->setCurrency($provider->getFee()->getCurrency()));

                $defaultFee = (new Fee())
                    ->setAmount($provider->getDefaultFee()->getAmount())
                    ->setIsPercent($provider->getDefaultFee()->getIsPercent())
                    ->setCurrency($provider->getDefaultFee()->getCurrency());

                $methods = [];

                foreach ($provider->getMethods() as $method){
                    $methods[] = (new \Eplane\ApiCore\Service\Payment\Message\RetrieveUserProviders\RetrieveUserProvidersResponse\Method())
                                  ->setCode($method->getCode())
                                  ->setTitle($method->getTitle())
                                  ->setEnabled($method->isIsAssigned());
                }

                $resultProviders[] = (new Provider())
                    ->setCode($provider->getCode())
                    ->setTitle($provider->getTitle())
                    ->setFee($fee)
                    ->setDefaultFee($defaultFee)
                    ->setConnected($provider->isConnected())
                    ->setConnectLinkUnwrapped($provider->getConnectLink())
                    ->setLastModifyUnwrapped($provider->getLastModify())
                    ->setMethods($methods);
            }
        } catch (\Exception $e) {
            return (new RetrieveUserProvidersResponse())->setResult(
                (new Result())
                    ->setCode(ErrorCodes::ERR_API_BASE)
                    ->setMessage($e->getMessage())
                    ->setTrace($e->getTraceAsString())
                    ->setDetails([])
            );
        }

        return (new RetrieveUserProvidersResponse())->setProviders($resultProviders);
    }

    public function retrieveProvider(RetrieveProviderRequest $request)
    {
        try {
            $retrieveProviderQuery = new RetrieveProviderQuery($request->getProviderId());
            $provider              = $this->retrieveProviderHandler->handle($retrieveProviderQuery);

            return (new RetrieveProviderResponse())
                ->setCode($provider->getCode())
                ->setTitle($provider->getTitle())
                ->setEnabled($provider->getEnabled())
                ->setDefaultFee(
                    (new Fee())
                        ->setAmount($provider->getDefaultFee()->getAmount())
                        ->setCurrency($provider->getDefaultFee()->getCurrency())
                        ->setIsPercent($provider->getDefaultFee()->getIsPercent())
                );
        } catch (\Exception $ex) {
            return (new RetrieveProviderResponse())->setResult(
                (new Result())
                    ->setCode(ErrorCodes::ERR_API_BASE)
                    ->setMessage($ex->getMessage())
                    ->setTrace($ex->getTraceAsString())
            );
        }
    }

    /**
     * @param GPBEmpty|null $request
     * @return RetrieveAllProvidersResponse
     */
    public function retrieveAllProviders(GPBEmpty $request = null): RetrieveAllProvidersResponse
    {
        try {
            $providers = $this->retrieveAllProvidersHandler->handle();

            $resultProviders = [];
            foreach ($providers as $provider) {
                $methods = [];

                foreach ($provider->getMethods() as $method) {
                    $methods[] = (new AllProviders\Method(
                    ))
                        ->setCode($method->getCode())
                        ->setTitle($method->getTitle())
                        ->setEnabled($method->getEnabled());
                }

                $resultProviders[] = (new AllProviders())
                    ->setCode($provider->getCode())
                    ->setTitle($provider->getTitle())
                    ->setEnabled($provider->getEnabled())
                    ->setDefaultFee(
                        (new Fee())
                            ->setAmount($provider->getDefaultFee()->getAmount())
                            ->setIsPercent($provider->getDefaultFee()->getIsPercent())
                            ->setCurrency($provider->getDefaultFee()->getCurrency())
                    )
                    ->setMethods($methods);
            }
        } catch (\Exception $e) {
            return (new RetrieveAllProvidersResponse())->setResult(
                (new Result())
                    ->setCode(ErrorCodes::ERR_API_BASE)
                    ->setMessage($e->getMessage())
                    ->setTrace($e->getTraceAsString())
                    ->setDetails([])
            );
        }

        return (new RetrieveAllProvidersResponse())->setProviders($resultProviders);
    }

    /**
     * @param RegisterMethodRequest $request
     * @return RegisterMethodResponse
     */
    public function registerMethod(RegisterMethodRequest $request): RegisterMethodResponse
    {
        try {
            $resultMethods = [];

            /** @var RegisterMethodRequest\Method[] $methods */
            $methods = $request->getMethods();

            foreach ($methods as $method) {
                $command = new RegisterMethodCommand(
                    $method->getCode(),
                    $method->getTitle(),
                    $method->getProviderCode(),
                    $method->getEnabled()
                );

                if ($aggregateId = $this->registerMethodsHandler->handle($command)) {
                    $resultMethods[] = (new RegisterMethod())
                        ->setId($aggregateId)
                        ->setCode($method->getCode())
                        ->setTitle($method->getTitle())
                        ->setProviderCode($method->getProviderCode())
                        ->setEnabled($method->getEnabled());
                }
            }
        } catch (\Exception $e) {
            return (new RegisterMethodResponse())->setResult(
                (new Result())
                    ->setCode(ErrorCodes::ERR_API_BASE)
                    ->setMessage($e->getMessage())
                    ->setTrace($e->getTraceAsString())
                    ->setDetails([])
            );
        }

        return (new RegisterMethodResponse())->setMethods($resultMethods);
    }

    /**
     * @param AssignMethodToUserRequest $request
     * @return AssignMethodToUserResponse
     */
    public function assignMethodToUser(AssignMethodToUserRequest $request): AssignMethodToUserResponse
    {
        try {
            $assignedMethods = [];

            /** @var AssignMethodToUserRequest\UserMethod[] $methods */
            $methods = $request->getUserMethods();

            foreach ($methods as $method) {
                $command = new AssignMethodToUserCommand(
                    $method->getProviderCode(),
                    $method->getMethodCode(),
                    $method->getUserId()
                );

                $this->assignMethodToUserHandler->handle($command);

                $assignedMethods[] = (new AssignUserMethod())
                    ->setProviderCode($method->getProviderCode())
                    ->setMethodCode($method->getMethodCode())
                    ->setUserId($method->getUserId())
                    ->setIsAssigned(true);
            }
        } catch (\Exception $e) {
            return (new AssignMethodToUserResponse())->setResult(
                (new Result())
                    ->setCode(ErrorCodes::ERR_API_BASE)
                    ->setMessage($e->getMessage())
                    ->setTrace($e->getTraceAsString())
                    ->setDetails([])
            );
        }

        return (new AssignMethodToUserResponse())->setUserMethods($assignedMethods);
    }

    /**
     * @param DetachMethodFromUserRequest $request
     * @return DetachMethodFromUserResponse
     */
    public function detachMethodFromUser(DetachMethodFromUserRequest $request): DetachMethodFromUserResponse
    {
        try {
            $detachedMethods = [];

            /** @var UserMethod[] $methods */
            $methods = $request->getUserMethods();

            foreach ($methods as $method) {
                $command = new DetachMethodFromUserCommand(
                    $method->getProviderCode(),
                    $method->getMethodCode(),
                    $method->getUserId()
                );

                $this->detachMethodFromUserHandler->handle($command);

                $detachedMethods[] = (new DetachUserMethod())
                    ->setProviderCode($method->getProviderCode())
                    ->setMethodCode($method->getMethodCode())
                    ->setUserId($method->getUserId())
                    ->setIsAssigned(false);
            }
        } catch (\Exception $e) {
            return (new DetachMethodFromUserResponse())->setResult(
                (new Result())
                    ->setCode(ErrorCodes::ERR_API_BASE)
                    ->setMessage($e->getMessage())
                    ->setTrace($e->getTraceAsString())
                    ->setDetails([])
            );
        }

        return (new DetachMethodFromUserResponse())->setUserMethods($detachedMethods);
    }

    /**
     * @param RetrieveOrderPaymentDataRequest $request
     * @return RetrieveOrderPaymentDataResponse
     */
    public function retrieveOrderPaymentData(RetrieveOrderPaymentDataRequest $request){
        try {
            $sellerProviders = ($this->retrieveOrderPaymentDataHandler->handle(
                new RetrieveOrderPaymentDataQuery(
                    $request->getUserId()
                )
            ))->getProviders();

            $resultProviders = [];
            foreach ($sellerProviders as $provider) {
                $fee = (new Fee())
                    ->setAmount($provider->getFee()->getAmount())
                    ->setIsPercent($provider->getFee()->getIsPercent())
                    ->setCurrency($provider->getFee()->getCurrency());

                $methods = [];

                foreach ($provider->getMethods() as $method) {
                    $methods[] = (new Method())
                                 ->setCode($method->getCode())
                                 ->setTitle($method->getTitle());
                }

                $resultProviders[] = (new RetrieveOrderPaymentDataResponse\Provider())
                    ->setCode($provider->getCode())
                    ->setTitle($provider->getTitle())
                    ->setFee($fee)
                    ->setMethods($methods);
            }
        } catch (\Exception $e) {
            return (new RetrieveOrderPaymentDataResponse())->setResult(
                (new Result())
                    ->setCode(ErrorCodes::ERR_API_BASE)
                    ->setMessage($e->getMessage())
                    ->setTrace($e->getTraceAsString())
                    ->setDetails([])
            );
        }

        return (new RetrieveOrderPaymentDataResponse())->setProviders($resultProviders);
    }

    /**
     * @param RetrievePaymentRequest $request
     * @return RetrievePaymentResponse
     */
    public function retrievePayment(RetrievePaymentRequest $request): RetrievePaymentResponse
    {
        try {
            $query = new RetrievePaymentQuery($request->getId());
            $payment = $this->retrievePaymentHandler->handle($query);

            $resultPayment = (new RetrievePaymentResponse\Payment())
                ->setId($payment->getId())
                ->setOrderId($payment->getOrderId())
                ->setRequestId($payment->getRequestId())
                ->setSellerId($payment->getSellerId())
                ->setBuyerId($payment->getBuyerId())
                ->setCustomerId($payment->getCustomerId())
                ->setChatMessageId($payment->getChatMessageId())
                ->setTotal(
                    (new Money())
                        ->setAmount(
                            (new Decimal())
                                ->setValue($payment->getPrice()->getInternalValue())
                                ->setPrecision($payment->getPrice()->getScale())
                        )
                        ->setCurrency($payment->getCurrency())
                )
                ->setFee((new Fee())->setAmount($payment->getFee())->setCurrency($payment->getCurrency()))
                ->setProviderCode($payment->getProviderCode())
                ->setMethodCode($payment->getMethodCode())
                ->setPaymentData($payment->getPaymentData())
                ->setPlatformPayment($payment->getIsPlatform());
        } catch (\Exception $e){

            return (new RetrievePaymentResponse())->setResult(
                (new Result())
                    ->setCode(ErrorCodes::ERR_API_BASE)
                    ->setMessage($e->getMessage())
                    ->setTrace($e->getTraceAsString())
                    ->setDetails([])
            );
        }

        return (new RetrievePaymentResponse())->setPayment($resultPayment);
    }

    /**
     * @param ResetUserProviderFeeToDefaultRequest $request
     * @return ResetUserProviderFeeToDefaultResponse
     */
    public function resetUserProviderFeeToDefault(ResetUserProviderFeeToDefaultRequest $request)
    {
        try {
            $changedProviders = [];

            /** @var ChangeUserProviderFeeRequest\Provider[] $providers */
            $providers = $request->getProviders();

            foreach ($providers as $provider) {
                $command = new ResetUserProviderFeeToDefaultCommand(
                    $provider->getProviderCode(),
                    $provider->getUserId()
                );

                $this->resetUserProviderFeeToDefaultHandler->handle($command);

                $changedProviders[] = (new ResetUserProviderFeeToDefaultResponse\Provider())
                    ->setProviderCode($provider->getProviderCode())
                    ->setUserId($provider->getUserId())
                    ->setFee(new Fee());
            }
        } catch (\Exception $e) {
            return (new ResetUserProviderFeeToDefaultResponse())->setResult(
                (new Result())
                    ->setCode(ErrorCodes::ERR_API_BASE)
                    ->setMessage($e->getMessage())
                    ->setTrace($e->getTraceAsString())
                    ->setDetails([])
            );
        }

        return (new ResetUserProviderFeeToDefaultResponse())->setProviders($changedProviders);
    }

    /**
     * @param \Eplane\ApiCore\Service\Payment\Message\RetrieveUser\RetrieveUserRequest $request
     * @return \Eplane\ApiCore\Service\Payment\Message\RetrieveUser\RetrieveUserResponse
     */
    public function retrieveUser(\Eplane\ApiCore\Service\Payment\Message\RetrieveUser\RetrieveUserRequest $request){
        try {
            $query = new RetrieveUserQuery(
                $request->getUserId()
            );

            $result = $this->retrieveUserHandler->handle($query);

            $user = (new \Eplane\ApiCore\Service\Payment\Message\RetrieveUser\RetrieveUserResponse\User())
                ->setUserId($result->getUserId())
                ->setIsActive($result->isActive())
                ->setPlatformPay($result->isPlatformPay())
                ->setCreatedAt($result->getCreatedAt())
                ->setLastModifyAt($result->getLastModifyAt());
        } catch (\Exception $e) {
            return (new \Eplane\ApiCore\Service\Payment\Message\RetrieveUser\RetrieveUserResponse())->setResult(
                (new Result())
                    ->setCode(ErrorCodes::ERR_API_BASE)
                    ->setMessage($e->getMessage())
                    ->setTrace($e->getTraceAsString())
                    ->setDetails([])
            );
        }

        return (new \Eplane\ApiCore\Service\Payment\Message\RetrieveUser\RetrieveUserResponse())->setUser($user);
    }
}
