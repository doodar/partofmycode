<?php
namespace Eplane\Payment\Infrastructure\Domain\Model\User\Event;

use Eplane\ApiCore\Infrastructure\Domain\Event\EventConverterInterface;
use Eplane\Ddd\Domain\Event\DomainEventInterface;
use Eplane\Payment\Domain\Model\User\Event\ProviderAttached;
use Google\Protobuf\Internal\Message;

/** @noinspection PhpUnused */
class ProviderAttachedConverter implements EventConverterInterface
{
    public function getEventClass(): string
    {
        return ProviderAttached::class;
    }

    /**
     * @param ProviderAttached|DomainEventInterface $event
     * @return \Eplane\ApiCore\Service\Payment\Event\User\ProviderAttached
     */
    public function convert(DomainEventInterface $event): Message
    {
        return (new \Eplane\ApiCore\Service\Payment\Event\User\ProviderAttached())
            ->setUserId($event->getUserId()->getValue())
            ->setProviderCode($event->getProviderCode());
    }
}
