<?php
namespace Eplane\Payment\Infrastructure\Domain\Model\User\Event;

use Eplane\ApiCore\Infrastructure\Domain\Event\EventConverterInterface;
use Eplane\Ddd\Domain\Event\DomainEventInterface;
use Eplane\Payment\Domain\Model\User\Event\ProviderDetached;
use Google\Protobuf\Internal\Message;

/** @noinspection PhpUnused */
class ProviderDetachedConverter implements EventConverterInterface
{
    public function getEventClass(): string
    {
        return ProviderDetached::class;
    }

    /**
     * @param ProviderDetached|DomainEventInterface $event
     * @return \Eplane\ApiCore\Service\Payment\Event\User\ProviderDetached
     */
    public function convert(DomainEventInterface $event): Message
    {
        return (new \Eplane\ApiCore\Service\Payment\Event\User\ProviderDetached())
            ->setUserId($event->getUserId()->getValue())
            ->setProviderId($event->getProviderId()->getValue());
    }
}
