<?php
namespace Eplane\Payment\Infrastructure\Domain\Model\User\Event;

use Eplane\ApiCore\Infrastructure\Domain\Event\EventConverterInterface;
use Eplane\Ddd\Domain\Event\DomainEventInterface;
use Eplane\Payment\Domain\Model\User\Event\ProviderAssigned;
use Google\Protobuf\Internal\Message;

/** @noinspection PhpUnused */
class ProviderAssignedConverter implements EventConverterInterface
{
    public function getEventClass(): string
    {
        return ProviderAssigned::class;
    }

    /**
     * @param ProviderAssigned|DomainEventInterface $event
     * @return \Eplane\ApiCore\Service\Payment\Event\User\ProviderAssigned
     */
    public function convert(DomainEventInterface $event): Message
    {
        return (new \Eplane\ApiCore\Service\Payment\Event\User\ProviderAssigned())
            ->setUserId($event->getUserId()->getValue())
            ->setProviderId($event->getProviderId()->getValue());
    }
}
