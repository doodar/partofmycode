<?php

namespace Eplane\Payment\Infrastructure\Domain\Model\User\Magento;

use Eplane\Base\Utils\MageDi;
use Eplane\Ddd\Infrastructure\Service\Hydrator;
use Eplane\Payment\Domain\Model\Method\MethodId;
use Eplane\Payment\Domain\Model\Provider\ProviderId;
use Eplane\Payment\Domain\Model\User\AssignedMethod;
use Eplane\Payment\Domain\Model\User\AssignedProvider;
use Eplane\Payment\Domain\Model\User\User;
use Eplane\Payment\Domain\Model\User\UserId;
use Eplane\Payment\Domain\Model\User\UserRepositoryInterface;
use Eplane\Payment\Domain\ValueObject\Fee;
use Eplane\Payment\Infrastructure\Domain\Model\User\Magento\ResourceModel\UserPaymentMethodsCollection;
use Eplane\Payment\Infrastructure\Domain\Model\User\Magento\ResourceModel\UserPaymentProvidersCollection;
use Magento\Framework\App\ResourceConnection;
use Eplane\Payment\Domain\Model\User\Exception\NoSuchUserException;

class UserRepository implements UserRepositoryInterface
{
    private $connection;

    /**
     * @param ResourceConnection $connection
     * @param Hydrator $hydrator
     */
    public function __construct(
        ResourceConnection $connection
    )
    {
        $this->connection = $connection;
    }

    /**
     * @param User $aggregateRoot
     * @return $this
     * @throws \Zend_Filter_Exception
     * @throws \Exception
     */
    public function add($aggregateRoot)
    {
        $user = MageDi::create(UserPayment::class,
            ['data' =>
                [
                    UserPayment::USER_ID => $aggregateRoot->getId()->getValue(),
                    UserPayment::IS_ACTIVE => $aggregateRoot->getIsActive(),
                    UserPayment::CREATED_AT => $aggregateRoot->getCreatedAt()->format(UserPayment::DEFAULT_DATE_FORMAT),
                    UserPayment::LAST_MODIFY_AT => $aggregateRoot->getLastModifyAt()->format(UserPayment::DEFAULT_DATE_FORMAT),
                    UserPayment::PLATFORM_PAY => $aggregateRoot->getPlatformPay()
                ]
            ]
        );

        $user->isObjectNew(true);
        $user->save();

        foreach ($aggregateRoot->getAssignedProviders() as $assignedProvider){
            $lastModify = $assignedProvider->getLastModifyAt();

            MageDi::create(UserPaymentProviders::class,
                ['data' =>
                    [
                        UserPaymentProviders::USER_ID => $aggregateRoot->getId()->getValue(),
                        UserPaymentProviders::PAYMENT_PROVIDER_ID => $assignedProvider->getProviderId()->getValue(),
                        UserPaymentProviders::IS_ASSIGNED => $assignedProvider->getIsAssigned(),
                        UserPaymentProviders::FEE => $assignedProvider->getFee()->getAmount(),
                        UserPaymentProviders::IS_PERCENT => $assignedProvider->getFee()->getIsPercent(),
                        UserPaymentProviders::CURRENCY => $assignedProvider->getFee()->getCurrency(),
                        UserPaymentProviders::LAST_MODIFY_AT => $lastModify !== null ? $lastModify->format(UserPaymentProviders::DEFAULT_DATE_FORMAT) : null
                    ]
                ]
            )->save();
        }

        foreach ($aggregateRoot->getAssignedMethods() as $assignedMethod){
            MageDi::create(UserPaymentMethods::class,
                ['data' =>
                     [
                        UserPaymentMethods::USER_ID => $aggregateRoot->getId()->getValue(),
                        UserPaymentMethods::PAYMENT_PROVIDER_ID => $assignedMethod->getProviderId()->getValue(),
                        UserPaymentMethods::PAYMENT_METHOD_ID => $assignedMethod->getMethodId()->getValue(),
                        UserPaymentMethods::IS_ASSIGNED => $assignedMethod->getIsAssigned(),
                        UserPaymentMethods::LAST_MODIFY_AT => $assignedMethod->getLastModifyAt()->format(UserPaymentMethods::DEFAULT_DATE_FORMAT)
                    ]
                ]
            )->save();
        }
    }

    /**
     * @param UserId $id
     * @return User
     * @throws NoSuchUserException
     * @throws \Exception
     */
    public function get($id)
    {
        $userPayment = MageDi::create(UserPayment::class)->load($id->getValue());

        if (!$userPayment->hasData()) {
            throw NoSuchUserException::singleField(
                UserPayment::USER_ID,
                $id->getValue()
            );
        }

        $userPaymentProviders = MageDi::create(UserPaymentProvidersCollection::class)->addFieldToFilter(
            UserPaymentProviders::USER_ID,
            $id->getValue()
        );

        $userPaymentMethods = MageDi::create(UserPaymentMethodsCollection::class)->addFieldToFilter(
            UserPaymentMethods::USER_ID,
            $id->getValue()
        );

        return $this->assemblyAggregate($userPayment, $userPaymentProviders, $userPaymentMethods);
    }

    /**
     * @param User $aggregateRoot
     * @return User
     * @throws \Zend_Filter_Exception
     * @throws \Exception
     */
    public function save($aggregateRoot)
    {
        $user = MageDi::create(UserPayment::class,
            ['data' =>
                 [
                     UserPayment::USER_ID => $aggregateRoot->getId()->getValue(),
                     UserPayment::IS_ACTIVE => $aggregateRoot->getIsActive(),
                     UserPayment::CREATED_AT => $aggregateRoot->getCreatedAt()->format(UserPayment::DEFAULT_DATE_FORMAT),
                     UserPayment::LAST_MODIFY_AT => $aggregateRoot->getLastModifyAt()->format(UserPayment::DEFAULT_DATE_FORMAT),
                     UserPayment::PLATFORM_PAY => $aggregateRoot->getPlatformPay()
                 ]
            ]
        )->save();


        $assignedProviderCollection = MageDi::create(UserPaymentProvidersCollection::class);
        $assignedProviderCollection->setForUpdate(true);
        $assignedProviderCollection->addFieldToFilter(UserPaymentProviders::USER_ID, $aggregateRoot->getId()->getValue());
        $assignedProviderCollection->walk('delete');

        foreach ($aggregateRoot->getAssignedProviders() as $assignedProvider){
            $lastModify = $assignedProvider->getLastModifyAt();
            MageDi::create(UserPaymentProviders::class,
                ['data' =>
                     [
                         UserPaymentProviders::USER_ID => $aggregateRoot->getId()->getValue(),
                         UserPaymentProviders::PAYMENT_PROVIDER_ID => $assignedProvider->getProviderId()->getValue(),
                         UserPaymentProviders::IS_ASSIGNED => $assignedProvider->getIsAssigned(),
                         UserPaymentProviders::FEE => $assignedProvider->getFee()->getAmount(),
                         UserPaymentProviders::IS_PERCENT => $assignedProvider->getFee()->getIsPercent(),
                         UserPaymentProviders::CURRENCY => $assignedProvider->getFee()->getCurrency(),
                         UserPaymentProviders::LAST_MODIFY_AT => $lastModify !== null ? $lastModify->format(UserPaymentProviders::DEFAULT_DATE_FORMAT) : null
                     ]
                ]
            )->save();
        }

        $assignedMethodCollection = MageDi::create(UserPaymentMethodsCollection::class);
        $assignedMethodCollection->setForUpdate(true);
        $assignedMethodCollection->addFieldToFilter(UserPaymentMethods::USER_ID, $aggregateRoot->getId()->getValue());
        $assignedMethodCollection->walk('delete');

        foreach ($aggregateRoot->getAssignedMethods() as $assignedMethod){
            MageDi::create(UserPaymentMethods::class,
                ['data' =>
                     [
                         UserPaymentMethods::USER_ID => $aggregateRoot->getId()->getValue(),
                         UserPaymentMethods::PAYMENT_PROVIDER_ID => $assignedMethod->getProviderId()->getValue(),
                         UserPaymentMethods::PAYMENT_METHOD_ID => $assignedMethod->getMethodId()->getValue(),
                         UserPaymentMethods::IS_ASSIGNED => $assignedMethod->getIsAssigned(),
                         UserPaymentMethods::LAST_MODIFY_AT => $assignedMethod->getLastModifyAt()->format(UserPaymentMethods::DEFAULT_DATE_FORMAT)
                     ]
                ]
            )->save();
        }
    }

    /**
     * @param UserPayment $userPayment
     * @param UserPaymentProvidersCollection $userPaymentProviders
     * @param UserPaymentMethodsCollection $userPaymentMethods
     * @return User
     * @throws \Exception
     */
    private function assemblyAggregate(
        UserPayment $userPayment,
        UserPaymentProvidersCollection $userPaymentProviders,
        UserPaymentMethodsCollection $userPaymentMethods
    ) {
        $paymentProviders = [];
        $paymentMethods = [];

        $paymentHydrator = new Hydrator([
            'id'                  => 'id',
            'isActive'            => 'isActive',
            'assignedProviders'   => 'assignedProviders',
            'assignedMethods'     => 'assignedMethods',
            'createdAt'           => 'createdAt',
            'lastModifyAt'        => 'lastModifyAt',
            'platformPay' => 'platformPay'
        ]);

        $paymentProvidersHydrator = new Hydrator([
            'providerId'   => 'providerId',
            'isAssigned'   => 'isAssigned',
            'fee'          => 'fee',
            'lastModifyAt' => 'lastModifyAt'
        ]);

        $paymentMethodsHydrator = new Hydrator([
            'providerId'   => 'providerId',
            'methodId'     => 'methodId',
            'isAssigned'   => 'isAssigned',
            'lastModifyAt' => 'lastModifyAt'
        ]);

        foreach ($userPaymentProviders->getItems() as $item) {
            $fee = $item->getFee();

            $paymentProvider = $item->getPaymentProvider();
            $lastModify = $item->getLastModifyAt();
            $paymentProviders[$paymentProvider] = $paymentProvidersHydrator->hydrate(
                [
                    'providerId'   => new ProviderId($paymentProvider),
                    'isAssigned'   => $item->getIsAssigned(),
                    'fee'          => new Fee($fee ? $fee->toFloat() : null, $item->getIsPercent(), $item->getCurrency()),
                    'lastModifyAt' => $lastModify ?? null

                ],
                AssignedProvider::class
            );
        }

        foreach ($userPaymentMethods->getItems() as $item) {
            $paymentMethod                  = $item->getPaymentMethod();
            $paymentMethods[$paymentMethod] = $paymentMethodsHydrator->hydrate(
                [
                    'providerId'   => new ProviderId($item->getPaymentProvider()),
                    'methodId'     => new MethodId($paymentMethod),
                    'isAssigned'   => $item->getIsAssigned(),
                    'lastModifyAt' => $item->getLastModifyAt()
                ],
                AssignedMethod::class
            );
        }

        return $paymentHydrator->hydrate(
            [
                'id'                  => new UserId($userPayment->getUserId()),
                'isActive'            => $userPayment->getIsActive(),
                'assignedProviders'   => $paymentProviders,
                'assignedMethods'     => $paymentMethods,
                'lastModifyAt'        => $userPayment->getLastModifyAt(),
                'createdAt'           => $userPayment->getCreatedAt(),
                'platformPay' => $userPayment->getPlatformPay()
            ],
            User::class
        );
    }
}
