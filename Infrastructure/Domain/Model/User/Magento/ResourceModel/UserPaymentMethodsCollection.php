<?php
namespace Eplane\Payment\Infrastructure\Domain\Model\User\Magento\ResourceModel;

use Eplane\Base\Model\Collection as EplaneCollection;
use Eplane\Payment\Infrastructure\Domain\Model\User\Magento\UserPaymentMethods;

/**
 * @method UserPaymentMethods[] getItems()
 */
class UserPaymentMethodsCollection extends EplaneCollection
{
    /**
     * Initialize resource collection
     *
     * @return void
     */
    public function _construct()
    {
        $this->_init(UserPaymentMethods::class, UserPaymentMethodsResource::class);
    }
}
