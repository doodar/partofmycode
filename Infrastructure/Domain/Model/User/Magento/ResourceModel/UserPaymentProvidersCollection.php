<?php
namespace Eplane\Payment\Infrastructure\Domain\Model\User\Magento\ResourceModel;

use Eplane\Base\Model\Collection as EplaneCollection;
use Eplane\Payment\Infrastructure\Domain\Model\User\Magento\UserPaymentProviders;

/**
 * @method UserPaymentProviders[] getItems()
 */
class UserPaymentProvidersCollection extends EplaneCollection
{
    /**
     * Initialize resource collection
     *
     * @return void
     */
    public function _construct()
    {
        $this->_init(UserPaymentProviders::class, UserPaymentProvidersResource::class);
    }
}
