<?php
namespace Eplane\Payment\Infrastructure\Domain\Model\User\Magento\ResourceModel;

use Eplane\Base\Model\ResourceModel\AbstractDB;
use Eplane\Payment\Infrastructure\Domain\Model\User\Magento\UserPayment;

class UserPaymentResource extends AbstractDB
{

    /**
     * Initialize resource
     *
     * @return void
     */
    public function _construct()
    {
        $this->_useIsObjectNew = true;
        $this->_isPkAutoIncrement = false;
        $this->_init(Tables::TABLE_PAYMENT_USER, UserPayment::USER_ID);
    }
}