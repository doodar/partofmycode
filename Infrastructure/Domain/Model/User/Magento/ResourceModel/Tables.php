<?php
namespace Eplane\Payment\Infrastructure\Domain\Model\User\Magento\ResourceModel;

class Tables
{
    const TABLE_PAYMENT_USER = 'eplane_payment_user';
    const TABLE_PAYMENT_USER_METHODS = 'eplane_payment_user_method';
    const TABLE_PAYMENT_USER_PROVIDERS = 'eplane_payment_user_provider';
}