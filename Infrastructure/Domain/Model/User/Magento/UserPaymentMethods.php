<?php
namespace Eplane\Payment\Infrastructure\Domain\Model\User\Magento;

use Eplane\Base\Model\AbstractModel;
use Eplane\Base\Model\Filter\BigIntId;
use Eplane\Base\Model\Filter\StringField;
use Eplane\Base\Model\Filter\DateTime;
use Eplane\Base\Model\Filter\Boolean;
use Eplane\Payment\Infrastructure\Domain\Model\User\Magento\ResourceModel\UserPaymentMethodsResource;

class UserPaymentMethods extends AbstractModel
{
    const ID                  = 'id';
    const USER_ID             = 'user_id';
    const PAYMENT_PROVIDER_ID = 'payment_provider_id';
    const PAYMENT_METHOD_ID   = 'payment_method_id';
    const IS_ASSIGNED         = 'is_assigned';
    const LAST_MODIFY_AT      = 'last_modify_at';

    const DEFAULT_DATE_FORMAT = 'Y-m-d H:i:s';

    /**
     * Initialize resource model
     * @return void
     */
    public function _construct()
    {
        $this->_init(UserPaymentMethodsResource::class);
    }

    protected static function getStaticColumnDefs(): array
    {
        $bigIntFilter  = new BigIntId();
        $stringFilter  = new StringField();
        $dateFilter    = new DateTime(false);
        $booleanFilter = new Boolean(false);

        return [
            self::ID                  => $bigIntFilter,
            self::USER_ID             => $bigIntFilter,
            self::PAYMENT_PROVIDER_ID => $stringFilter,
            self::PAYMENT_METHOD_ID   => $stringFilter,
            self::IS_ASSIGNED         => $booleanFilter,
            self::LAST_MODIFY_AT      => $dateFilter,
        ];
    }

    /**
     * @return string|null
     */
    public function getId()
    {
        return $this->_getData(self::ID);
    }

    /**
     * @return string|null
     */
    public function getUserId()
    {
        return $this->_getData(self::USER_ID);
    }

    /**
     * @return string|null
     */
    public function getPaymentProvider()
    {
        return $this->_getData(self::PAYMENT_PROVIDER_ID);
    }

    /**
     * @return string|null
     */
    public function getPaymentMethod()
    {
        return $this->_getData(self::PAYMENT_METHOD_ID);
    }

    /**
     * @return bool
     */
    public function getIsAssigned()
    {
        return $this->_getData(self::IS_ASSIGNED);
    }

    /**
     * @return string|null
     */
    public function getLastModifyAt()
    {
        return $this->_getData(self::LAST_MODIFY_AT);
    }

    /**
     * @param string $value
     * @return $this
     * @throws \Zend_Filter_Exception
     */
    public function setUserId($value){
        return $this->setData(self::USER_ID, $value);
    }

    /**
     * @param string $value
     * @return $this
     * @throws \Zend_Filter_Exception
     */
    public function setPaymentProvider($value){
        return $this->setData(self::PAYMENT_PROVIDER_ID, $value);
    }

    /**
     * @param string $value
     * @return $this
     * @throws \Zend_Filter_Exception
     */
    public function setPaymentMethod($value){
        return $this->setData(self::PAYMENT_METHOD_ID, $value);
    }

    /**
     * @param bool $value
     * @return $this
     * @throws \Zend_Filter_Exception
     */
    public function setIsAssigned($value){
        return $this->setData(self::IS_ASSIGNED, $value);
    }

    /**
     * @param string $value
     * @return $this
     * @throws \Zend_Filter_Exception
     */
    public function setLastModifyAt($value){
        return $this->setData(self::LAST_MODIFY_AT, $value);
    }
}
