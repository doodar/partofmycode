<?php
namespace Eplane\Payment\Infrastructure\Domain\Model\User\Magento;

use Eplane\Base\Model\AbstractModel;
use Eplane\Base\Model\Filter\BigIntId;
use Eplane\Base\Model\Filter\Decimal;
use Eplane\Base\Model\Filter\StringField;
use Eplane\Base\Model\Filter\DateTime;
use Eplane\Base\Model\Filter\Boolean;
use Eplane\Payment\Infrastructure\Domain\Model\User\Magento\ResourceModel\UserPaymentProvidersResource;

class UserPaymentProviders extends AbstractModel
{
    const ID                  = 'id';
    const USER_ID             = 'user_id';
    const PAYMENT_PROVIDER_ID = 'payment_provider_id';
    const IS_ASSIGNED         = 'is_assigned';
    const FEE                 = 'fee';
    const IS_PERCENT          = 'is_percent';
    const CURRENCY            = 'currency';
    const LAST_MODIFY_AT      = 'last_modify_at';

    const DEFAULT_DATE_FORMAT = 'Y-m-d H:i:s';

    /**
     * Initialize resource model
     * @return void
     */
    public function _construct()
    {
        $this->_init(UserPaymentProvidersResource::class);
    }

    protected static function getStaticColumnDefs(): array
    {
        $bigIntFilter  = new BigIntId();
        $stringFilter  = new StringField();
        $dateFilter    = new DateTime(true);
        $booleanFilter = new Boolean(false);
        $decimalFilter = new Decimal(true);

        return [
            self::ID                  => $bigIntFilter,
            self::USER_ID             => $bigIntFilter,
            self::PAYMENT_PROVIDER_ID => $stringFilter,
            self::IS_ASSIGNED         => $booleanFilter,
            self::FEE                 => $decimalFilter,
            self::IS_PERCENT          => $booleanFilter,
            self::CURRENCY            => $stringFilter,
            self::LAST_MODIFY_AT      => $dateFilter,
        ];
    }

    /**
     * @return string|null
     */
    public function getId()
    {
        return $this->_getData(self::ID);
    }

    /**
     * @return string|null
     */
    public function getUserId()
    {
        return $this->_getData(self::USER_ID);
    }

    /**
     * @return string|null
     */
    public function getPaymentProvider()
    {
        return $this->_getData(self::PAYMENT_PROVIDER_ID);
    }

    /**
     * @return bool
     */
    public function getIsAssigned()
    {
        return $this->_getData(self::IS_ASSIGNED);
    }

    /**
     * @return \Eplane\Base\Php\BigNumber\Decimal|null
     */
    public function getFee()
    {
        return $this->_getData(self::FEE);
    }

    /**
     * @return bool
     */
    public function getIsPercent()
    {
        return $this->_getData(self::IS_PERCENT);
    }

    /**
     * @return string|null
     */
    public function getCurrency()
    {
        return $this->_getData(self::CURRENCY);
    }

    /**
     * @return string|null
     */
    public function getLastModifyAt()
    {
        return $this->_getData(self::LAST_MODIFY_AT);
    }

    /**
     * @param string $value
     * @return $this
     * @throws \Zend_Filter_Exception
     */
    public function setUserId($value){
        return $this->setData(self::USER_ID, $value);
    }

    /**
     * @param string $value
     * @return $this
     * @throws \Zend_Filter_Exception
     */
    public function setPaymentProvider($value){
        return $this->setData(self::PAYMENT_PROVIDER_ID, $value);
    }

    /**
     * @param bool $value
     * @return $this
     * @throws \Zend_Filter_Exception
     */
    public function setIsAssigned($value){
        return $this->setData(self::IS_ASSIGNED, $value);
    }

    /**
     * @param string $value
     * @return $this
     * @throws \Zend_Filter_Exception
     */
    public function setFee($value){
        return $this->setData(self::FEE, $value);
    }

    /**
     * @param bool $value
     * @return $this
     * @throws \Zend_Filter_Exception
     */
    public function setIsPercent($value){
        return $this->setData(self::IS_PERCENT, $value);
    }

    /**
     * @param string $value
     * @return $this
     * @throws \Zend_Filter_Exception
     */
    public function setCurrency($value){
        return $this->setData(self::CURRENCY, $value);
    }

    /**
     * @param string $value
     * @return $this
     * @throws \Zend_Filter_Exception
     */
    public function setLastModifyAt($value){
        return $this->setData(self::LAST_MODIFY_AT, $value);
    }
}
