<?php
namespace Eplane\Payment\Infrastructure\Domain\Model\User\Magento;

use Eplane\Base\Model\AbstractModel;
use Eplane\Base\Model\Filter\BigIntId;
use Eplane\Base\Model\Filter\DateTime;
use Eplane\Base\Model\Filter\Boolean;
use Eplane\Payment\Infrastructure\Domain\Model\User\Magento\ResourceModel\UserPaymentResource;

class UserPayment extends AbstractModel
{
    const USER_ID                   = 'user_id';
    const IS_ACTIVE                 = 'is_active';
    const CREATED_AT                = 'created_at';
    const LAST_MODIFY_AT            = 'last_modify_at';
    const PLATFORM_PAY              = 'platform_pay';

    const DEFAULT_DATE_FORMAT = 'Y-m-d H:i:s';

    /**
     * Initialize resource model
     * @return void
     */
    public function _construct()
    {
        $this->_init(UserPaymentResource::class);
    }

    protected static function getStaticColumnDefs(): array
    {
        $bigIntFilter  = new BigIntId();
        $dateFilter    = new DateTime(false);
        $booleanFilter = new Boolean(false);

        return [
            self::USER_ID                => $bigIntFilter,
            self::IS_ACTIVE              => $booleanFilter,
            self::CREATED_AT             => $dateFilter,
            self::LAST_MODIFY_AT         => $dateFilter,
            self::PLATFORM_PAY => $booleanFilter,
        ];
    }

    /**
     * @return string|null
     */
    public function getUserId()
    {
        return $this->_getData(self::USER_ID);
    }

    /**
     * @return bool
     */
    public function getIsActive()
    {
        return $this->_getData(self::IS_ACTIVE);
    }

    /**
     * @return string|null
     */
    public function getCreatedAt()
    {
        return $this->_getData(self::CREATED_AT);
    }

    /**
     * @return string|null
     */
    public function getLastModifyAt()
    {
        return $this->_getData(self::LAST_MODIFY_AT);
    }

    /**
     * @return bool
     */
    public function getPlatformPay()
    {
        return $this->_getData(self::PLATFORM_PAY);
    }

    /**
     * @param string $value
     * @return $this
     * @throws \Zend_Filter_Exception
     */
    public function setUserId($value){
        return $this->setData(self::USER_ID, $value);
    }

    /**
     * @param bool $value
     * @return $this
     * @throws \Zend_Filter_Exception
     */
    public function setIsActive($value){
        return $this->setData(self::IS_ACTIVE, $value);
    }

    /**
     * @param string $value
     * @return $this
     * @throws \Zend_Filter_Exception
     */
    public function setCreatedAt($value){
        return $this->setData(self::CREATED_AT, $value);
    }

    /**
     * @param string $value
     * @return $this
     * @throws \Zend_Filter_Exception
     */
    public function setLastModifyAt($value){
        return $this->setData(self::LAST_MODIFY_AT, $value);
    }

    /**
     * @param bool $value
     * @return $this
     * @throws \Zend_Filter_Exception
     */
    public function setPlatformPay($value){
        return $this->setData(self::PLATFORM_PAY, $value);
    }
}
