<?php

namespace Eplane\Payment\Infrastructure\Domain\Model\User\InMemory;

use Eplane\Payment\Domain\Model\User\Exception\NoSuchUserException;
use Eplane\Payment\Domain\Model\User\User;
use Eplane\Payment\Domain\Model\User\UserId;
use Eplane\Payment\Domain\Model\User\UserRepositoryInterface;
use Eplane\Payment\Infrastructure\Domain\Model\User\Magento\UserPayment;

class UserRepository implements UserRepositoryInterface
{
    /**
     * @var User[]
     */
    private $items = [];
    private $identity = 0;

    /**
     * @inheritDoc
     */
    public function add($user)
    {
        if (!isset($this->items[$user->getId()->getValue()])) {
            $this->items[$user->getId()->getValue()] = $user;
        } else {
            throw new \Exception('Key exist');
        }
    }

    /**
     * @inheritDoc
     */
    public function get($id): User
    {
        if (isset($this->items[$id->getValue()])) {
            return $this->items[$id->getValue()];
        }

        throw NoSuchUserException::singleField(
            UserPayment::USER_ID,
            $id->getValue()
        );
    }

    /**
     * @inheritDoc
     */
    public function save($user)
    {
        $this->items[$user->getId()->getValue()] = $user;
    }
}
