<?php

namespace Eplane\Payment\Infrastructure\Domain\Model\Method\Magento;

use Eplane\Base\Utils\MageDi;
use Eplane\Ddd\Infrastructure\Service\Hydrator;
use Eplane\Ddd\Infrastructure\Service\RedisMysqlIdGenerator;
use Eplane\Payment\Domain\Model\Provider\ProviderId;
use Eplane\Payment\Domain\ValueObject\Fee;
use Eplane\Payment\Domain\Model\Method\Exception\NoSuchMethodException;
use Magento\Framework\App\ResourceConnection;
use Eplane\Payment\Domain\Model\Method\Method;
use Eplane\Payment\Domain\Model\Method\MethodId;
use Eplane\Payment\Domain\Model\Method\MethodRepositoryInterface;
use Eplane\Payment\Infrastructure\Domain\Model\Method\Magento\Method as MethodDataModel;
use Eplane\Payment\Infrastructure\Domain\Model\Method\Magento\ResourceModel\Tables;
use Eplane\Payment\Infrastructure\Domain\Model\Method\Magento\ResourceModel\PaymentMethodsCollection;

class MethodRepository implements MethodRepositoryInterface
{
    private $connection;

    /**
     * @param $connection
     */
    public function __construct(ResourceConnection $connection)
    {
        $this->connection = $connection;
    }

    /**
     * @param Method $aggregateRoot
     * @return $this
     */
    public function add($aggregateRoot){
        $method = MageDi::create(MethodDataModel::class,
            ['data' =>
                 [
                     MethodDataModel::ID => $aggregateRoot->getId()->getValue(),
                     MethodDataModel::CODE => $aggregateRoot->getCode(),
                     MethodDataModel::TITLE => $aggregateRoot->getTitle(),
                     MethodDataModel::PROVIDER_ID => $aggregateRoot->getProviderId()->getValue(),
                     MethodDataModel::ENABLED => $aggregateRoot->isEnabled(),
                     MethodDataModel::PLATFORM_ENABLED => $aggregateRoot->isPlatformEnabled()
                 ]
            ]
        );

        $method->isObjectNew(true);
        $method->save();
    }

    /**
     * @param MethodId $id
     * @return Method
     * @throws \Exception
     */
    public function get($id){
        $method = MageDi::create(MethodDataModel::class)->load($id->getValue());

        if($method->isEmpty()){
            throw NoSuchMethodException::singleField(
                MethodDataModel::ID,
                $id->getValue()
            );
        }

        return $this->assemblyAggregate($method);
    }

    /**
     * @param ProviderId $id
     * @return Method[]
     * @throws \Exception
     */
    public function getByProviderId($id){
        $items = [];

        $methods = MageDi::create(PaymentMethodsCollection::class)->addFieldToFilter(
            MethodDataModel::PROVIDER_ID,
            $id->getValue()
        );

        /** @var MethodDataModel $method */
        foreach ($methods->getItems() as $method) {
            $items[] = $this->assemblyAggregate($method);
        }

        return $items;
    }

    /**
     * @param ProviderId $providerId
     * @param string $code
     * @return Method|bool
     * @throws \Exception
     */
    public function getByProviderAndCode($providerId, $code){
        /** @var MethodDataModel $method */
        $method = MageDi::create(PaymentMethodsCollection::class)->addFieldToFilter(
            MethodDataModel::PROVIDER_ID,
            $providerId->getValue()
        )->addFieldToFilter(
            MethodDataModel::CODE,
            $code
        )->getFirstItem();

        if($method->isEmpty()){
            throw NoSuchMethodException::doubleField(
                MethodDataModel::PROVIDER_ID,
                $providerId->getValue(),
                MethodDataModel::CODE,
                $code
            );
        }

        return $this->assemblyAggregate($method);
    }

    /**
     * @return Method[]
     */
    public function getAll(){

    }

    /**
     * @return MethodId
     */
    public function nextIdentity(){
        return new MethodId(RedisMysqlIdGenerator::generateIdentity(Tables::TABLE_PAYMENT_METHODS));
    }


    /**
     * @param Method $aggregateRoot
     * @return Method
     */
    public function save($aggregateRoot){
        $method = MageDi::create(MethodDataModel::class,
            ['data' =>
                 [
                     MethodDataModel::ID => $aggregateRoot->getId()->getValue(),
                     MethodDataModel::CODE => $aggregateRoot->getCode(),
                     MethodDataModel::TITLE => $aggregateRoot->getTitle(),
                     MethodDataModel::PROVIDER_ID => $aggregateRoot->getProviderId()->getValue(),
                     MethodDataModel::ENABLED => $aggregateRoot->isEnabled(),
                     MethodDataModel::PLATFORM_ENABLED => $aggregateRoot->isPlatformEnabled()
                 ]
            ]
        )->save();
    }

    /**
     * @param MethodDataModel $method
     * @return Method
     * @throws \Exception
     */
    private function assemblyAggregate(
        MethodDataModel $method
    ) {
        $methodHydrator = new Hydrator([
            'id'         => 'id',
            'code'       => 'code',
            'title'      => 'title',
            'providerId' => 'providerId',
            'enabled'    => 'enabled',
            'platformEnabled' => 'platformEnabled'
        ]);

        return $methodHydrator->hydrate(
            [
                'id'         => new MethodId($method->getId()),
                'code'       => $method->getCode(),
                'title'      => $method->getTitle(),
                'providerId' => new ProviderId($method->getProviderId()),
                'enabled'    => $method->getEnabled(),
                'platformEnabled' => $method->getPlatformEnabled()
            ],
            Method::class
        );
    }

    /**
     * @return bool
     */
    public function hasPlatformPaymentMethods(){
        $methods = MageDi::create(PaymentMethodsCollection::class)->addFieldToFilter(
            MethodDataModel::PLATFORM_ENABLED,
            true
        );

        return (bool)$methods->count();
    }
}
