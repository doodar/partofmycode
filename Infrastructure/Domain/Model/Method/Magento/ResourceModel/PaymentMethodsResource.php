<?php
namespace Eplane\Payment\Infrastructure\Domain\Model\Method\Magento\ResourceModel;

use Eplane\Base\Model\ResourceModel\AbstractDB;
use Eplane\Payment\Infrastructure\Domain\Model\Method\Magento\Method;

class PaymentMethodsResource extends AbstractDB
{

    /**
     * Initialize resource
     *
     * @return void
     */
    public function _construct()
    {
        $this->_useIsObjectNew = true;
        $this->_isPkAutoIncrement = false;
        $this->_init(Tables::TABLE_PAYMENT_METHODS, Method::ID);
    }
}

