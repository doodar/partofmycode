<?php
namespace Eplane\Payment\Infrastructure\Domain\Model\Method\Magento\ResourceModel;

use Eplane\Base\Model\Collection as EplaneCollection;
use Eplane\Payment\Infrastructure\Domain\Model\Method\Magento\Method;

class PaymentMethodsCollection extends EplaneCollection
{
    /**
     * Initialize resource collection
     *
     * @return void
     */
    public function _construct()
    {
        $this->_init(Method::class, PaymentMethodsResource::class);
    }
}
