<?php
namespace Eplane\Payment\Infrastructure\Domain\Model\Method\Magento;

use Eplane\Base\Model\AbstractModel;
use Eplane\Base\Model\Filter\BigIntId;
use Eplane\Base\Model\Filter\Boolean;
use Eplane\Base\Model\Filter\StringField;
use Eplane\Payment\Infrastructure\Domain\Model\Method\Magento\ResourceModel\PaymentMethodsResource;

class Method extends AbstractModel
{
    const ID           = 'id';
    const CODE         = 'code';
    const TITLE        = 'title';
    const PROVIDER_ID  = 'provider_id';
    const ENABLED      = 'enabled';
    const PLATFORM_ENABLED = 'platform_enabled';

    /**
     * Initialize resource model
     * @return void
     */
    public function _construct()
    {
        $this->_init(PaymentMethodsResource::class);
    }

    protected static function getStaticColumnDefs(): array
    {
        $bigIntFilter  = new BigIntId();
        $booleanFilter = new Boolean(false);
        $stringFilter  = new StringField();

        return [
            self::ID          => $bigIntFilter,
            self::CODE        => $stringFilter,
            self::TITLE       => $stringFilter,
            self::PROVIDER_ID => $bigIntFilter,
            self::ENABLED     => $booleanFilter,
            self::PLATFORM_ENABLED => $booleanFilter
        ];
    }

    /**
     * @return string|null
     */
    public function getId()
    {
        return $this->_getData(self::ID);
    }

    /**
     * @return string|null
     */
    public function getCode()
    {
        return $this->_getData(self::CODE);
    }

    /**
     * @return string|null
     */
    public function getTitle()
    {
        return $this->_getData(self::TITLE);
    }

    /**
     * @return string|null
     */
    public function getProviderId()
    {
        return $this->_getData(self::PROVIDER_ID);
    }

    /**
     * @return bool
     */
    public function getEnabled()
    {
        return $this->_getData(self::ENABLED);
    }

    /**
     * @return bool
     */
    public function getPlatformEnabled()
    {
        return $this->_getData(self::PLATFORM_ENABLED);
    }

    /**
     * @param string $value
     * @return $this
     * @throws \Zend_Filter_Exception
     */
    public function setId($value){
        return $this->setData(self::ID, $value);
    }

    /**
     * @param string $value
     * @return $this
     * @throws \Zend_Filter_Exception
     */
    public function setCode($value){
        return $this->setData(self::CODE, $value);
    }

    /**
     * @param string $value
     * @return $this
     * @throws \Zend_Filter_Exception
     */
    public function setTitle($value){
        return $this->setData(self::TITLE, $value);
    }

    /**
     * @param string $value
     * @return $this
     * @throws \Zend_Filter_Exception
     */
    public function setProviderId($value){
        return $this->setData(self::PROVIDER_ID, $value);
    }

    /**
     * @param bool $value
     * @return $this
     * @throws \Zend_Filter_Exception
     */
    public function setEnabled($value){
        return $this->setData(self::ENABLED, $value);
    }

    /**
     * @param bool $value
     * @return $this
     * @throws \Zend_Filter_Exception
     */
    public function setPlatformEnabled($value){
        return $this->setData(self::PLATFORM_ENABLED, $value);
    }
}
