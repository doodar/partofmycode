<?php
namespace Eplane\Payment\Infrastructure\Domain\Model\Payment\Magento\ResourceModel;

use Eplane\Base\Model\ResourceModel\AbstractDB;
use Eplane\Payment\Infrastructure\Domain\Model\Payment\Magento\Payment;

class PaymentResource extends AbstractDB
{
    /**
     * Initialize resource collection
     *
     * @return void
     */
    public function _construct()
    {
        $this->_useIsObjectNew    = true;
        $this->_isPkAutoIncrement = false;
        $this->_init(Tables::TABLE_PAYMENT, Payment::ID);
    }
}
