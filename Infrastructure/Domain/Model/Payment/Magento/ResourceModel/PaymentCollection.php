<?php
namespace Eplane\Payment\Infrastructure\Domain\Model\Payment\Magento\ResourceModel;

use Eplane\Base\Model\Collection as EplaneCollection;
use Eplane\Payment\Infrastructure\Domain\Model\Payment\Magento\Payment;

class PaymentCollection extends EplaneCollection
{
    /**
     * Initialize resource collection
     *
     * @return void
     */
    public function _construct()
    {
        $this->_init(Payment::class, PaymentResource::class);
    }
}
