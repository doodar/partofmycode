<?php

namespace Eplane\Payment\Infrastructure\Domain\Model\Payment\Magento;

use Eplane\Base\Utils\MageDi;
use Eplane\Ddd\Infrastructure\Service\Hydrator;
use Eplane\Ddd\Infrastructure\Service\RedisMysqlIdGenerator;
use Eplane\Payment\Domain\Model\Payment\Exception\NoSuchPaymentException;
use Eplane\Payment\Domain\Model\Payment\Payment;
use Eplane\Payment\Domain\Model\Payment\PaymentId;
use Eplane\Payment\Domain\Model\Payment\PaymentRepositoryInterface;
use Eplane\Payment\Infrastructure\Domain\Model\Payment\Magento\Payment as PaymentDataModel;
use Eplane\Payment\Infrastructure\Domain\Model\Payment\Magento\ResourceModel\Tables;

class PaymentRepository implements PaymentRepositoryInterface
{

    /**
     * @inheritDoc
     * @throws \Exception
     */
    public function add($aggregateRoot)
    {
        $this->savePayment($aggregateRoot);
    }

    /**
     * @inheritDoc
     * @throws \ReflectionException
     */
    public function get($id)
    {
        $payment = MageDi::create(PaymentDataModel::class)->load($id->getValue());

        if($payment->isEmpty()){
            throw NoSuchPaymentException::singleField(
                PaymentDataModel::ID,
                $id->getValue()
            );
        }

        return $this->assemblyAggregate($payment);
    }

    /**
     * @return PaymentId
     */
    public function nextIdentity()
    {
        return new PaymentId(RedisMysqlIdGenerator::generateIdentity(Tables::TABLE_PAYMENT));
    }


    /**
     * @inheritDoc
     * @throws \Exception
     */
    public function save($aggregateRoot)
    {
        $this->savePayment($aggregateRoot, false);
    }

    /**
     * @param Payment $aggregateRoot
     * @param bool $isNew
     * @throws \Exception
     */
    private function savePayment($aggregateRoot, bool $isNew = true)
    {
        $payment = MageDi::create(
            PaymentDataModel::class,
            [
                'data' =>
                    [
                        PaymentDataModel::ID                   => $aggregateRoot->getId()->getValue(),
                        PaymentDataModel::CHAT_PAYMENT_ID      => $aggregateRoot->getChatPaymentId(),
                        PaymentDataModel::REQUEST_ID           => $aggregateRoot->getRequestId(),
                        PaymentDataModel::ORDER_ID             => $aggregateRoot->getOrderId(),
                        PaymentDataModel::PAYMENT_DATA_ENCODED => $aggregateRoot->getPaymentDataEncoded(),
                        PaymentDataModel::PROVIDER             => $aggregateRoot->getProvider(),
                        PaymentDataModel::METHOD               => $aggregateRoot->getMethod(),
                        PaymentDataModel::SELLER_ID            => $aggregateRoot->getSellerId(),
                        PaymentDataModel::BUYER_ID             => $aggregateRoot->getBuyerId(),
                        PaymentDataModel::CUSTOMER_ID          => $aggregateRoot->getCustomerId(),
                        PaymentDataModel::MESSAGE_ID           => $aggregateRoot->getMessageId(),
                        PaymentDataModel::PRICE                => $aggregateRoot->getPrice(),
                        PaymentDataModel::CURRENCY             => $aggregateRoot->getCurrency(),
                        PaymentDataModel::FEE                  => $aggregateRoot->getFee(),
                        PaymentDataModel::STATUS               => $aggregateRoot->getStatus(),
                        PaymentDataModel::IS_PLATFORM          => $aggregateRoot->getIsPlatform(),
                    ]
            ]
        );

        $payment->isObjectNew($isNew);
        $payment->save();
    }

    /**
     * @param PaymentDataModel $payment
     * @return Payment|object
     * @throws \ReflectionException
     */
    private function assemblyAggregate($payment) {
        $paymentHydrator = new Hydrator([
            'id' => 'id',
            'chatPaymentId' => 'chatPaymentId',
            'requestId' => 'requestId',
            'orderId' => 'orderId',
            'paymentDataEncoded' => 'paymentDataEncoded',
            'provider' => 'provider',
            'method' => 'method',
            'sellerId' => 'sellerId',
            'buyerId' => 'buyerId',
            'customerId' => 'customerId',
            'messageId' => 'messageId',
            'price' => 'price',
            'currency' => 'currency',
            'fee' => 'fee',
            'status' => 'status',
            'isPlatform' => 'isPlatform',
        ]);

        return $paymentHydrator->hydrate(
            [
                'id' => new PaymentId($payment->getId()),
                'chatPaymentId' => $payment->getChatPaymentId(),
                'requestId' => $payment->getRequestId(),
                'orderId' => $payment->getOrderId(),
                'paymentDataEncoded' => $payment->getPaymentDataEncoded(),
                'provider' => $payment->getProvider(),
                'method' => $payment->getMethod(),
                'sellerId' => $payment->getSellerId(),
                'buyerId' => $payment->getBuyerId(),
                'customerId' => $payment->getCustomerId(),
                'messageId' => $payment->getMessageId(),
                'price' => $payment->getPrice(),
                'currency' => $payment->getCurrency(),
                'fee' => $payment->getFee(),
                'status' => $payment->getStatus(),
                'isPlatform' => $payment->getIsPlatform(),
            ],
            Payment::class
        );
    }
}
