<?php

namespace Eplane\Payment\Infrastructure\Domain\Model\Payment\Magento;

use Eplane\Base\Model\AbstractModel;
use Eplane\Base\Model\Filter\BigIntId;
use Eplane\Base\Model\Filter\Boolean;
use Eplane\Base\Model\Filter\Decimal;
use Eplane\Base\Model\Filter\StringField;
use Eplane\Payment\Infrastructure\Domain\Model\Payment\Magento\ResourceModel\PaymentResource;

class Payment extends AbstractModel
{
    const ID                   = 'id';
    const CHAT_PAYMENT_ID      = 'chat_payment_id';
    const REQUEST_ID           = 'request_id';
    const ORDER_ID             = 'order_id';
    const PAYMENT_DATA_ENCODED = 'payment_data_encoded';
    const PROVIDER             = 'provider';
    const METHOD               = 'method';
    const SELLER_ID            = 'seller_id';
    const BUYER_ID             = 'buyer_id';
    const CUSTOMER_ID          = 'customer_id';
    const MESSAGE_ID           = 'message_id';
    const PRICE                = 'price';
    const CURRENCY             = 'currency';
    const FEE                  = 'fee';
    const STATUS               = 'status';
    const CREATED_AT           = 'created_at';
    const UPDATED_AT           = 'updated_at';
    const IS_PLATFORM          = 'is_platform';

    /**
     * Initialize resource model
     * @return void
     */
    public function _construct()
    {
        $this->_init(PaymentResource::class);
    }

    protected static function getStaticColumnDefs(): array
    {
        $bigIntFilter  = new BigIntId();
        $stringFilter  = new StringField();
        $decimalFilter = new Decimal();
        $boolFilter    = new Boolean();

        return [
            self::ID                   => $bigIntFilter,
            self::CHAT_PAYMENT_ID      => $bigIntFilter,
            self::REQUEST_ID           => $bigIntFilter,
            self::ORDER_ID             => $bigIntFilter,
            self::PAYMENT_DATA_ENCODED => $stringFilter,
            self::PROVIDER             => $stringFilter,
            self::METHOD               => $stringFilter,
            self::SELLER_ID            => $bigIntFilter,
            self::BUYER_ID             => $bigIntFilter,
            self::CUSTOMER_ID          => $bigIntFilter,
            self::MESSAGE_ID           => $bigIntFilter,
            self::PRICE                => $decimalFilter,
            self::CURRENCY             => $stringFilter,
            self::FEE                  => $decimalFilter,
            self::STATUS               => $stringFilter,
            self::IS_PLATFORM          => $boolFilter,
        ];
    }

    /**
     * @return string
     */
    public function getId(): string
    {
        return $this->_getData(self::ID);
    }

    /**
     * @return string
     */
    public function getChatPaymentId(): string
    {
        return $this->_getData(self::CHAT_PAYMENT_ID);
    }

    /**
     * @return string
     */
    public function getRequestId(): string
    {
        return $this->_getData(self::REQUEST_ID);
    }

    /**
     * @return string
     */
    public function getOrderId(): string
    {
        return $this->_getData(self::ORDER_ID);
    }

    /**
     * @return string
     */
    public function getPaymentDataEncoded(): string
    {
        return $this->_getData(self::PAYMENT_DATA_ENCODED);
    }

    /**
     * @return string
     */
    public function getProvider(): string
    {
        return $this->_getData(self::PROVIDER);
    }

    /**
     * @return string
     */
    public function getMethod(): string
    {
        return $this->_getData(self::METHOD);
    }

    /**
     * @return string
     */
    public function getSellerId(): string
    {
        return $this->_getData(self::SELLER_ID);
    }

    /**
     * @return string
     */
    public function getBuyerId(): string
    {
        return $this->_getData(self::BUYER_ID);
    }

    /**
     * @return string
     */
    public function getCustomerId(): string
    {
        return $this->_getData(self::CUSTOMER_ID);
    }

    /**
     * @return string
     */
    public function getMessageId(): string
    {
        return $this->_getData(self::MESSAGE_ID);
    }

    /**
     * @return \Eplane\Base\Php\BigNumber\Decimal
     */
    public function getPrice(): \Eplane\Base\Php\BigNumber\Decimal
    {
        return $this->_getData(self::PRICE);
    }

    /**
     * @return string
     */
    public function getCurrency(): string
    {
        return $this->_getData(self::CURRENCY);
    }

    /**
     * @return \Eplane\Base\Php\BigNumber\Decimal
     */
    public function getFee(): \Eplane\Base\Php\BigNumber\Decimal
    {
        return $this->_getData(self::FEE);
    }

    /**
     * @return string
     */
    public function getStatus(): string
    {
        return $this->_getData(self::STATUS);
    }

    /**
     * @return bool
     */
    public function getIsPlatform(): bool
    {
        return $this->_getData(self::IS_PLATFORM);
    }
}
