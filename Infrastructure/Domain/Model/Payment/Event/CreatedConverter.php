<?php
namespace Eplane\Payment\Infrastructure\Domain\Model\Payment\Event;

use Eplane\ApiCore\Infrastructure\Domain\Event\EventConverterInterface;
use Eplane\Ddd\Domain\Event\DomainEventInterface;
use Eplane\Payment\Domain\Model\Payment\Event\Created;
use Google\Protobuf\Internal\Message;

/** @noinspection PhpUnused */
class CreatedConverter implements EventConverterInterface
{
    public function getEventClass(): string
    {
        return Created::class;
    }

    /**
     * @param Created|DomainEventInterface $event
     * @return Message
     */
    public function convert(DomainEventInterface $event): Message
    {
        return (new \Eplane\ApiCore\Service\Payment\Event\Payment\Created())
            ->setId($event->getPaymentId())
            ->setChatPaymentId($event->getChatPaymentId())
            ->setProviderCode($event->getProviderCode());
    }

}
