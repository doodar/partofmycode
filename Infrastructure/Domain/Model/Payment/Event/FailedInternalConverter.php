<?php
namespace Eplane\Payment\Infrastructure\Domain\Model\Payment\Event;

use Eplane\ApiCore\Infrastructure\Domain\Event\EventConverterInterface;
use Eplane\Ddd\Domain\Event\DomainEventInterface;
use Eplane\Payment\Domain\Model\Payment\Event\Failed;
use Eplane\Payment\Domain\Model\Payment\Event\FailedInternal;
use Google\Protobuf\Internal\Message;

/** @noinspection PhpUnused */
class FailedInternalConverter implements EventConverterInterface
{
    public function getEventClass(): string
    {
        return FailedInternal::class;
    }

    /**
     * @param FailedInternal|DomainEventInterface $event
     * @return Message
     */
    public function convert(DomainEventInterface $event): Message
    {
        return (new \Eplane\ApiCore\Service\Payment\Event\Payment\FailedInternal())
            ->setId($event->getPaymentId())
            ->setChatPaymentId($event->getChatPaymentId())
            ->setReason($event->getReason());
    }

}
