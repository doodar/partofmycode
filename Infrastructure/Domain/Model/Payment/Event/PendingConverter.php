<?php
namespace Eplane\Payment\Infrastructure\Domain\Model\Payment\Event;

use Eplane\ApiCore\Infrastructure\Domain\Event\EventConverterInterface;
use Eplane\Ddd\Domain\Event\DomainEventInterface;
use Eplane\Payment\Domain\Model\Payment\Event\Pending;
use Google\Protobuf\Internal\Message;

/** @noinspection PhpUnused */
class PendingConverter implements EventConverterInterface
{
    public function getEventClass(): string
    {
        return Pending::class;
    }

    /**
     * @param Pending|DomainEventInterface $event
     * @return Message
     */
    public function convert(DomainEventInterface $event): Message
    {
        return (new \Eplane\ApiCore\Service\Payment\Event\Payment\Pending())
            ->setId($event->getPaymentId())
            ->setChatPaymentId($event->getChatPaymentId())
            ->setPaymentDataEncoded($event->getPaymentDataEncoded());
    }
}
