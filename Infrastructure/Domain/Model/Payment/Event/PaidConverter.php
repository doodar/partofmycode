<?php
namespace Eplane\Payment\Infrastructure\Domain\Model\Payment\Event;

use Eplane\ApiCore\Infrastructure\Domain\Event\EventConverterInterface;
use Eplane\Ddd\Domain\Event\DomainEventInterface;
use Eplane\Payment\Domain\Model\Payment\Event\Paid;
use Google\Protobuf\Internal\Message;

/** @noinspection PhpUnused */
class PaidConverter implements EventConverterInterface
{
    public function getEventClass(): string
    {
        return Paid::class;
    }

    /**
     * @param Paid|DomainEventInterface $event
     * @return Message
     */
    public function convert(DomainEventInterface $event): Message
    {
        return (new \Eplane\ApiCore\Service\Payment\Event\Payment\Paid())
            ->setId($event->getPaymentId())
            ->setChatPaymentId($event->getChatPaymentId());
    }

}
