<?php
namespace Eplane\Payment\Infrastructure\Domain\Model\Payment\Event;

use Eplane\ApiCore\Infrastructure\Domain\Event\EventConverterInterface;
use Eplane\Ddd\Domain\Event\DomainEventInterface;
use Eplane\Payment\Domain\Model\Payment\Event\Failed;
use Google\Protobuf\Internal\Message;

/** @noinspection PhpUnused */
class FailedConverter implements EventConverterInterface
{
    public function getEventClass(): string
    {
        return Failed::class;
    }

    /**
     * @param Failed|DomainEventInterface $event
     * @return Message
     */
    public function convert(DomainEventInterface $event): Message
    {
        return (new \Eplane\ApiCore\Service\Payment\Event\Payment\Failed())
            ->setId($event->getPaymentId())
            ->setChatPaymentId($event->getChatPaymentId())
            ->setReason($event->getReason());
    }

}
