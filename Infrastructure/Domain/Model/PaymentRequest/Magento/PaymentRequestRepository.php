<?php
namespace Eplane\Payment\Infrastructure\Domain\Model\PaymentRequest\Magento;

use Eplane\Base\Php\BigNumber\Decimal;
use Eplane\Ddd\Domain\Model\Currency;
use Eplane\Ddd\Domain\Model\Money;
use Eplane\Ddd\Infrastructure\Service\Hydrator;
use Eplane\Ddd\Infrastructure\Service\RedisMysqlIdGenerator;
use Eplane\Payment\Domain\Model\PaymentRequest\Exception\NoSuchPaymentRequestException;
use Eplane\Payment\Domain\Model\PaymentRequest\PaymentRequest;
use Eplane\Payment\Domain\Model\PaymentRequest\PaymentRequestId;
use Eplane\Payment\Domain\Model\PaymentRequest\PaymentRequestRepositoryInterface;
use Eplane\Payment\Domain\Model\PaymentRequest\Status;
use Eplane\Payment\Domain\Model\User\UserId;
use Eplane\Payment\Infrastructure\Domain\Model\PaymentRequest\Magento\ResourceModel\Tables;
use Magento\Framework\App\ResourceConnection;
use Magento\Framework\DB\Select;

/** @noinspection PhpUnused */

class PaymentRequestRepository implements PaymentRequestRepositoryInterface
{
    const HYDRATOR_KEY_TOTAL = 'total';

    /**
     * @var ResourceConnection
     */
    private $resourceConnection;

    public function __construct(
        ResourceConnection $resourceConnection
    ) {
        $this->resourceConnection = $resourceConnection;
    }

    /**
     * @return PaymentRequestId
     */
    public function nextIdentity()
    {
        return new PaymentRequestId(RedisMysqlIdGenerator::generateIdentity(Tables::TABLE_PAYMENT_REQUEST));
    }

    /**
     * @param PaymentRequest $aggregateRoot
     * @return PaymentRequest
     * @throws \ReflectionException
     */
    public function add(PaymentRequest $aggregateRoot)
    {
        $data = $this->extractFromPaymentRequest($aggregateRoot);

        $this->resourceConnection->getConnection()
            ->insert(Tables::TABLE_PAYMENT_REQUEST, $data);

        return $aggregateRoot;
    }

    /**
     * @param PaymentRequestId $id
     * @return PaymentRequest
     * @throws NoSuchPaymentRequestException
     * @throws \Zend_Db_Statement_Exception
     * @throws \ReflectionException
     */
    public function get(PaymentRequestId $id): PaymentRequest
    {
        return $this->getOneBy(PaymentRequestInterface::ID, $id->getValue());
    }

    /**
     * @param string $chatRequestId
     * @return PaymentRequest
     * @throws NoSuchPaymentRequestException
     * @throws \Zend_Db_Statement_Exception
     * @throws \ReflectionException
     */
    public function getByChatRequestId(string $chatRequestId): PaymentRequest
    {
        return $this->getOneBy(PaymentRequestInterface::CHAT_REQUEST_ID, $chatRequestId);
    }

    /**
     * @param PaymentRequest $aggregateRoot
     * @return PaymentRequest
     * @throws \ReflectionException
     */
    public function save(PaymentRequest $aggregateRoot)
    {
        $data = $this->extractFromPaymentRequest($aggregateRoot);
        unset($data[PaymentRequestInterface::ID]);

        $conn = $this->resourceConnection->getConnection();
        $this->resourceConnection->getConnection()
            ->update(
                Tables::TABLE_PAYMENT_REQUEST,
                $data,
                $conn->quoteInto(PaymentRequestInterface::ID . ' = ?', $aggregateRoot->getId()->getValue())
            );

        return $aggregateRoot;
    }

    /**
     * @return Select
     */
    private function createSelect()
    {
        return $this->resourceConnection->getConnection()
            ->select()
            ->from(['pr' => Tables::TABLE_PAYMENT_REQUEST]);
    }

    /**
     * @param string $column
     * @param string $value
     * @return PaymentRequest
     * @throws \ReflectionException
     * @throws \Zend_Db_Statement_Exception
     */
    private function getOneBy(string $column, string $value)
    {
        $select = $this->createSelect()
            ->where(\sprintf('`%s` = ?', $column), $value)
            ->limit(1);

        if (!$row = $select->query()->fetch()) {
            throw new NoSuchPaymentRequestException();
        }

        return $this->assemblePaymentRequest($row);
    }

    /**
     * @param array $row
     * @return PaymentRequest
     * @throws \ReflectionException
     */
    private function assemblePaymentRequest($row)
    {
        $hydrator = $this->getPaymentRequestHydrator();

        $total = new Money(
            Decimal::create($row[PaymentRequestInterface::TOTAL_AMOUNT]),
            new Currency($row[PaymentRequestInterface::TOTAL_CURRENCY])
        );

        $paymentRequest = $hydrator->hydrate(
            [
                PaymentRequestInterface::ID               => new PaymentRequestId($row[PaymentRequestInterface::ID]),
                PaymentRequestInterface::CHAT_REQUEST_ID  => $row[PaymentRequestInterface::CHAT_REQUEST_ID],
                PaymentRequestInterface::RFQ_INCREMENT_ID => $row[PaymentRequestInterface::RFQ_INCREMENT_ID],
                PaymentRequestInterface::USER_ID          => $row[PaymentRequestInterface::USER_ID],
                PaymentRequestInterface::CUSTOMER_ID      => $row[PaymentRequestInterface::CUSTOMER_ID],
                self::HYDRATOR_KEY_TOTAL                  => $total,
                PaymentRequestInterface::STATUS           => $row[PaymentRequestInterface::STATUS],
                PaymentRequestInterface::REJECT_REASON    => $row[PaymentRequestInterface::REJECT_REASON],
            ],
            PaymentRequest::class
        );

        return $paymentRequest;
    }

    /**
     * @param PaymentRequest $paymentRequest
     * @return array
     * @throws \ReflectionException
     */
    private function extractFromPaymentRequest(PaymentRequest $paymentRequest)
    {
        $data = $this->getPaymentRequestHydrator()->extract($paymentRequest);

        /** @var PaymentRequestId $id */
        $id                                = $data[PaymentRequestInterface::ID];
        $data[PaymentRequestInterface::ID] = $id->getValue();

        /** @var UserId $userId */
        $userId                                 = $data[PaymentRequestInterface::USER_ID];
        $data[PaymentRequestInterface::USER_ID] = $userId->getValue();

        /** @var Money $total */
        $total = $data[self::HYDRATOR_KEY_TOTAL];
        unset($data[self::HYDRATOR_KEY_TOTAL]);
        $data[PaymentRequestInterface::TOTAL_AMOUNT]   = $total->getAmount()->getInternalValue();
        $data[PaymentRequestInterface::TOTAL_CURRENCY] = $total->getCurrency()->getValue();

        /** @var Status $status */
        $status                                = $data[PaymentRequestInterface::STATUS];
        $data[PaymentRequestInterface::STATUS] = $status->getValue();

        return $data;
    }

    /**
     * @return Hydrator
     */
    private function getPaymentRequestHydrator()
    {
        static $hydrator;
        if ($hydrator === null) {
            $hydrator = new Hydrator(
                [
                    PaymentRequestInterface::ID               => 'id',
                    PaymentRequestInterface::CHAT_REQUEST_ID  => 'chatRequestId',
                    PaymentRequestInterface::RFQ_INCREMENT_ID => 'rfqIncrementId',
                    PaymentRequestInterface::USER_ID          => 'userId',
                    PaymentRequestInterface::CUSTOMER_ID      => 'customerId',
                    self::HYDRATOR_KEY_TOTAL                  => 'total',
                    PaymentRequestInterface::STATUS           => 'status',
                    PaymentRequestInterface::REJECT_REASON    => 'rejectReason',
                ]
            );
        }

        return $hydrator;
    }
}
