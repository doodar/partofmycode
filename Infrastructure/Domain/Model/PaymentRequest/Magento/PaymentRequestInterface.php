<?php

namespace Eplane\Payment\Infrastructure\Domain\Model\PaymentRequest\Magento;

interface PaymentRequestInterface
{
    const ID               = 'id';
    const CHAT_REQUEST_ID  = 'chat_request_id';
    const RFQ_INCREMENT_ID = 'rfq_increment_id';
    const USER_ID          = 'user_id';
    const CUSTOMER_ID      = 'customer_id';
    const TOTAL_AMOUNT     = 'total_amount';
    const TOTAL_CURRENCY   = 'total_currency';
    const STATUS           = 'status';
    const REJECT_REASON    = 'reject_reason';
}
