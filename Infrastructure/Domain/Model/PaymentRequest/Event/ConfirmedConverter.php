<?php
namespace Eplane\Payment\Infrastructure\Domain\Model\PaymentRequest\Event;

use Eplane\ApiCore\Infrastructure\Domain\Event\EventConverterInterface;
use Eplane\Ddd\Domain\Event\DomainEventInterface;
use Eplane\Payment\Domain\Model\PaymentRequest\Event\Confirmed;
use Google\Protobuf\Internal\Message;

/** @noinspection PhpUnused */
class ConfirmedConverter implements EventConverterInterface
{
    public function getEventClass(): string
    {
        return Confirmed::class;
    }

    /**
     * @param Confirmed|DomainEventInterface $event
     * @return Message
     */
    public function convert(DomainEventInterface $event): Message
    {
        return (new \Eplane\ApiCore\Service\Payment\Event\PaymentRequest\Confirmed())
            ->setRequestId($event->getRequestId()->getValue())
            ->setChatRequestId($event->getChatRequestId())
            ->setRfqIncrementId($event->getRfqIncrementId());
    }
}
