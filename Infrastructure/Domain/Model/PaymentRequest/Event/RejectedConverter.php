<?php
namespace Eplane\Payment\Infrastructure\Domain\Model\PaymentRequest\Event;

use Eplane\ApiCore\Infrastructure\Domain\Event\EventConverterInterface;
use Eplane\Ddd\Domain\Event\DomainEventInterface;
use Eplane\Payment\Domain\Model\PaymentRequest\Event\Rejected;
use Google\Protobuf\Internal\Message;

/** @noinspection PhpUnused */
class RejectedConverter implements EventConverterInterface
{
    public function getEventClass(): string
    {
        return Rejected::class;
    }

    /**
     * @param Rejected|DomainEventInterface $event
     * @return Message
     */
    public function convert(DomainEventInterface $event): Message
    {
        return (new \Eplane\ApiCore\Service\Payment\Event\PaymentRequest\Rejected())
            ->setRequestId($event->getRequestId()->getValue())
            ->setChatRequestId($event->getChatRequestId())
            ->setRfqIncrementId($event->getRfqIncrementId())
            ->setReason($event->getReason());
    }
}
