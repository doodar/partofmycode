<?php

namespace Eplane\Payment\Infrastructure\Domain\Model\Provider\Magento;

use Eplane\Base\Utils\MageDi;
use Eplane\Ddd\Infrastructure\Service\Hydrator;
use Eplane\Ddd\Infrastructure\Service\RedisMysqlIdGenerator;
use Eplane\Payment\Domain\Model\Method\MethodId;
use Eplane\Payment\Domain\Model\Provider\Exception\NoSuchProviderException;
use Eplane\Payment\Infrastructure\Domain\Model\Provider\Magento\ResourceModel\PaymentProvidersCollection;
use Eplane\Payment\Infrastructure\Domain\Model\Provider\Magento\ResourceModel\Tables;
use Eplane\Payment\Infrastructure\Domain\Model\Provider\Magento\Provider as ProviderDataModel;
use Eplane\Payment\Domain\Model\Provider\ProviderId;
use Eplane\Payment\Domain\Model\Provider\Provider;
use Eplane\Payment\Domain\Model\Provider\ProviderRepositoryInterface;
use Magento\Framework\App\ResourceConnection;
use Eplane\Payment\Infrastructure\Domain\Model\Method\Magento\ResourceModel\PaymentMethodsCollection;
use Eplane\Payment\Infrastructure\Domain\Model\Method\Magento\Method as MethodDataModel;
use Eplane\Payment\Domain\ValueObject\Fee;

class ProviderRepository implements ProviderRepositoryInterface
{
    private $connection;

    /**
     * MysqlSellerPaymentProvidersRepository constructor.
     * @param $connection
     */
    public function __construct(ResourceConnection $connection)
    {
        $this->connection = $connection;
    }

    /**
     * @param Provider $aggregateRoot
     * @return $this
     */
    public function add($aggregateRoot){
        $provider = MageDi::create(ProviderDataModel::class,
            ['data' =>
                 [
                     ProviderDataModel::ID => $aggregateRoot->getId()->getValue(),
                     ProviderDataModel::CODE => $aggregateRoot->getCode(),
                     ProviderDataModel::TITLE => $aggregateRoot->getTitle(),
                     ProviderDataModel::ENABLED => $aggregateRoot->isEnabled(),
                     ProviderDataModel::DEFAULT_FEE => $aggregateRoot->getDefaultFee()->getAmount(),
                     ProviderDataModel::IS_PERCENT => $aggregateRoot->getDefaultFee()->getIsPercent(),
                     ProviderDataModel::CURRENCY => $aggregateRoot->getDefaultFee()->getCurrency()
                 ]
            ]
        );

        $provider->isObjectNew(true);
        $provider->save();
    }

    /**
     * @param ProviderId $id
     * @return Provider
     * @throws \Exception
     */
    public function get(ProviderId $id): Provider {
        $provider = MageDi::create(ProviderDataModel::class)->load($id->getValue());

        if($provider->isEmpty()){
            throw NoSuchProviderException::singleField(
                ProviderDataModel::ID,
                $id->getValue()
            );
        }

        $providerMethods = MageDi::create(PaymentMethodsCollection::class)->addFieldToFilter(
            MethodDataModel::PROVIDER_ID,
            $id->getValue()
        );

        return $this->assemblyAggregate($provider, $providerMethods);
    }

    /**
     * @param string $providerCode
     * @return Provider|bool
     * @throws \Exception
     */
    public function getByCode($providerCode): Provider {
        /** @var ProviderDataModel $provider */
        $provider = MageDi::create(PaymentProvidersCollection::class)->addFieldToFilter(
            ProviderDataModel::CODE,
            $providerCode
        )->getFirstItem();

        if($provider->isEmpty()){
            throw NoSuchProviderException::singleField(
                ProviderDataModel::CODE,
                $providerCode
            );
        }

        $providerMethods = MageDi::create(PaymentMethodsCollection::class)->addFieldToFilter(
            MethodDataModel::PROVIDER_ID,
            $provider->getId()
        );

        return $this->assemblyAggregate($provider, $providerMethods);
    }

    /**
     * @return Provider[]
     */
    public function getAll(){

    }

    /**
     * @return ProviderId
     */
    public function nextIdentity(){
        return new ProviderId(RedisMysqlIdGenerator::generateIdentity(Tables::TABLE_PAYMENT_PROVIDERS));
    }

    /**
     * @param Provider $aggregateRoot
     * @return Provider
     */
    public function save($aggregateRoot){
        $provider = MageDi::create(ProviderDataModel::class,
            ['data' =>
                 [
                     ProviderDataModel::ID => $aggregateRoot->getId()->getValue(),
                     ProviderDataModel::CODE => $aggregateRoot->getCode(),
                     ProviderDataModel::TITLE => $aggregateRoot->getTitle(),
                     ProviderDataModel::ENABLED => $aggregateRoot->isEnabled(),
                     ProviderDataModel::DEFAULT_FEE => $aggregateRoot->getDefaultFee()->getAmount(),
                     ProviderDataModel::IS_PERCENT => $aggregateRoot->getDefaultFee()->getIsPercent(),
                     ProviderDataModel::CURRENCY => $aggregateRoot->getDefaultFee()->getCurrency()
                 ]
            ]
        )->save();
    }

    /**
     * @param ProviderDataModel $provider
     * @param PaymentMethodsCollection $providerMethods
     * @return Provider
     * @throws \Exception
     */
    private function assemblyAggregate(
        ProviderDataModel $provider,
        PaymentMethodsCollection $providerMethods
    ) {
        $providerMethodsIds = [];

        $providerHydrator = new Hydrator([
            'id'         => 'id',
            'code'       => 'code',
            'title'      => 'title',
            'enabled'    => 'enabled',
            'defaultFee' => 'defaultFee',
            'methodIds'  => 'methodIds',
        ]);

        foreach ($providerMethods->getItems() as $item) {
            $providerMethodsIds[] = new MethodId($item->getId());
        }

        $fee = $provider->getDefaultFee();

        return $providerHydrator->hydrate(
            [
                'id'         => new ProviderId($provider->getId()),
                'code'       => $provider->getCode(),
                'title'      => $provider->getTitle(),
                'enabled'    => $provider->getEnabled(),
                'defaultFee' => new Fee(
                    $fee ? $fee->toFloat() : null,
                    $provider->getIsPercent(),
                    $provider->getCurrency()
                ),
                'methodIds'  => $providerMethodsIds,
            ],
            Provider::class
        );
    }
}
