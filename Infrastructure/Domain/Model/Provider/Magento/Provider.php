<?php
namespace Eplane\Payment\Infrastructure\Domain\Model\Provider\Magento;

use Eplane\Base\Model\AbstractModel;
use Eplane\Base\Model\Filter\BigIntId;
use Eplane\Base\Model\Filter\Boolean;
use Eplane\Base\Model\Filter\Decimal;
use Eplane\Base\Model\Filter\StringField;
use Eplane\Payment\Infrastructure\Domain\Model\Provider\Magento\ResourceModel\PaymentProvidersResource;

class Provider extends AbstractModel
{
    const ID          = 'id';
    const CODE        = 'code';
    const TITLE       = 'title';
    const ENABLED     = 'enabled';
    const DEFAULT_FEE = 'default_fee';
    const IS_PERCENT  = 'is_percent';
    const CURRENCY    = 'currency';

    /**
     * Initialize resource model
     * @return void
     */
    public function _construct()
    {
        $this->_init(PaymentProvidersResource::class);
    }

    protected static function getStaticColumnDefs(): array
    {
        $bigIntFilter  = new BigIntId();
        $booleanFilter = new Boolean(false);
        $stringFilter  = new StringField();
        $decimalFilter = new Decimal(false);

        return [
            self::ID          => $bigIntFilter,
            self::CODE        => $stringFilter,
            self::TITLE       => $stringFilter,
            self::ENABLED     => $booleanFilter,
            self::DEFAULT_FEE => $decimalFilter,
            self::IS_PERCENT  => $booleanFilter,
            self::CURRENCY    => $stringFilter,
        ];
    }

    /**
     * @return string|null
     */
    public function getId()
    {
        return $this->_getData(self::ID);
    }

    /**
     * @return string|null
     */
    public function getCode()
    {
        return $this->_getData(self::CODE);
    }

    /**
     * @return string|null
     */
    public function getTitle()
    {
        return $this->_getData(self::TITLE);
    }

    /**
     * @return bool
     */
    public function getEnabled()
    {
        return $this->_getData(self::ENABLED);
    }

    /**
     * @return \Eplane\Base\Php\BigNumber\Decimal|null
     */
    public function getDefaultFee()
    {
        return $this->_getData(self::DEFAULT_FEE);
    }

    /**
     * @return bool
     */
    public function getIsPercent()
    {
        return $this->_getData(self::IS_PERCENT);
    }

    /**
     * @return string|null
     */
    public function getCurrency()
    {
        return $this->_getData(self::CURRENCY);
    }

    /**
     * @param string $value
     * @return $this
     * @throws \Zend_Filter_Exception
     */
    public function setId($value){
        return $this->setData(self::ID, $value);
    }

    /**
     * @param string $value
     * @return $this
     * @throws \Zend_Filter_Exception
     */
    public function setCode($value){
        return $this->setData(self::CODE, $value);
    }

    /**
     * @param string $value
     * @return $this
     * @throws \Zend_Filter_Exception
     */
    public function setTitle($value){
        return $this->setData(self::TITLE, $value);
    }

    /**
     * @param bool $value
     * @return $this
     * @throws \Zend_Filter_Exception
     */
    public function setEnabled($value){
        return $this->setData(self::ENABLED, $value);
    }

    /**
     * @param \Eplane\Base\Php\BigNumber\Decimal $value
     * @return $this
     * @throws \Zend_Filter_Exception
     */
    public function setDefaultFee($value){
        return $this->setData(self::DEFAULT_FEE, $value);
    }

    /**
     * @param bool $value
     * @return $this
     * @throws \Zend_Filter_Exception
     */
    public function setIsPercent($value){
        return $this->setData(self::IS_PERCENT, $value);
    }

    /**
     * @param string $value
     * @return $this
     * @throws \Zend_Filter_Exception
     */
    public function setCurrency($value){
        return $this->setData(self::CURRENCY, $value);
    }
}
