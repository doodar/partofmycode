<?php
namespace Eplane\Payment\Infrastructure\Domain\Model\Provider\Magento\ResourceModel;

use Eplane\Base\Model\Collection as EplaneCollection;
use Eplane\Payment\Infrastructure\Domain\Model\Provider\Magento\Provider;

/**
 * @method Provider[] getItems()
 */
class PaymentProvidersCollection extends EplaneCollection
{
    /**
     * Initialize resource collection
     *
     * @return void
     */
    public function _construct()
    {
        $this->_init(Provider::class, PaymentProvidersResource::class);
    }
}
