<?php
namespace Eplane\Payment\Infrastructure\Domain\Model\Provider\Magento\ResourceModel;

use Eplane\Base\Model\ResourceModel\AbstractDB;
use Eplane\Payment\Infrastructure\Domain\Model\Provider\Magento\Provider;

class PaymentProvidersResource extends AbstractDB
{

    /**
     * Initialize resource
     *
     * @return void
     */
    public function _construct()
    {
        $this->_useIsObjectNew = true;
        $this->_isPkAutoIncrement = false;
        $this->_init(Tables::TABLE_PAYMENT_PROVIDERS, Provider::ID);
    }
}

