<?php

namespace Eplane\Payment\Infrastructure\Domain\Model\Provider\InMemory;

use Eplane\Payment\Domain\Model\Provider\Exception\NoSuchProviderException;
use Eplane\Payment\Domain\Model\Provider\Provider;
use Eplane\Payment\Domain\Model\Provider\ProviderId;
use Eplane\Payment\Domain\Model\Provider\ProviderRepositoryInterface;
use Eplane\Payment\Infrastructure\Domain\Model\Provider\Magento\Provider as ProviderDataModel;

class ProviderRepository implements ProviderRepositoryInterface
{
    /**
     * @var Provider[]
     */
    private $items = [];
    private $identity = 0;

    /**
     * @inheritDoc
     */
    public function add($provider)
    {
        if (!isset($this->items[$provider->getId()->getValue()])) {
            $this->items[$provider->getId()->getValue()] = $provider;
        } else {
            throw new \Exception('Key exist');
        }
    }

    /**
     * @inheritDoc
     */
    public function get(ProviderId $id): Provider
    {
        return $this->items[$id->getValue()] ?? null;
    }

    /**
     * @inheritDoc
     */
    public function getByCode(/*string*/ $providerCode): Provider
    {
        foreach ($this->items as $item) {
            if ($item->getCode() === $providerCode) {
                return $item;
            }
        }

        throw NoSuchProviderException::singleField(
            ProviderDataModel::CODE,
            $providerCode
        );
    }

    /**
     * @inheritDoc
     */
    public function getAll()
    {
        return $this->items;
    }

    /**
     * @inheritDoc
     */
    public function nextIdentity()
    {
        return new ProviderId(++$this->identity);
    }

    /**
     * @inheritDoc
     */
    public function save($aggregateRoot)
    {
        // TODO: Implement save() method.
    }
}
