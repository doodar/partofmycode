<?php
namespace Eplane\Payment\Infrastructure\Logger\Magento;

use Eplane\Base\Utils\MageDi;

trait LoggerAwareTrait
{
    use \Eplane\Logger\Model\LoggerAwareTrait {
        setLogger as protected __setLogger;
        setLogTag as protected __setLogTag;
    }

    public function setLogger()
    {
        $logger = MageDi::get(Logger::class);

        $this->__setLogger($logger);
        $this->__setLogTag($logger->getName());
    }
}
