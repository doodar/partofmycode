<?php

namespace Eplane\Payment\Infrastructure\Logger\Magento;

use Eplane\Logger\Api\SlackChannels;
use Eplane\Logger\Model\Logger\SlackAndSd;

class Logger extends SlackAndSd
{
    protected static function getSlackChannel()
    {
        return SlackChannels::CHANNEL_PAYMENT_ERRORS;
    }
}
