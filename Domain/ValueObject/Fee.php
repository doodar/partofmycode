<?php

namespace Eplane\Payment\Domain\ValueObject;

use Eplane\Ddd\Domain\Contract\ValueObjectInterface;
use Eplane\Ddd\Domain\Traits\EqualsTrait;
use Eplane\Sales\Model\Currency;

class Fee implements ValueObjectInterface
{
    use EqualsTrait;

    private $amount;

    private $currency;

    private $isPercent;

    /**
     * Fee constructor.
     * @param $amount
     * @param $currency
     * @param $isPercent
     */
    public function __construct($amount, $isPercent, $currency = Currency::USD)
    {
        $this->amount    = $amount;
        $this->isPercent = $isPercent;
        $this->currency  = $currency;
    }

    public function getAmount(){
        return $this->amount;
    }

    public function getCurrency(){
        return $this->currency;
    }

    public function getIsPercent(){
        return $this->isPercent;
    }
}
