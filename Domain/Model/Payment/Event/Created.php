<?php

namespace Eplane\Payment\Domain\Model\Payment\Event;

use Eplane\Ddd\Domain\Event\DomainEventInterface;

class Created implements DomainEventInterface
{
    /**
     * @var string
     */
    protected $providerCode;
    /** @var string */
    private $paymentId;
    /**
     * @var \DateTimeImmutable
     */
    private $occurredOn;
    /** @var string */
    private $chatPaymentId;

    /**
     * Failed constructor.
     * @param string $paymentId
     * @param string $chatPaymentId
     * @param string $providerCode
     * @throws \Exception
     */
    public function __construct(string $paymentId, string $chatPaymentId, string $providerCode)
    {
        $this->paymentId = $paymentId;
        $this->chatPaymentId = $chatPaymentId;
        $this->providerCode = $providerCode;
        $this->occurredOn = new \DateTimeImmutable();
    }

    /**
     * @return string
     */
    public function getPaymentId(): string
    {
        return $this->paymentId;
    }

    /**
     * @return string
     */
    public function getChatPaymentId(): string
    {
        return $this->chatPaymentId;
    }

    /**
     * @return string
     */
    public function getProviderCode(): string
    {
        return $this->providerCode;
    }

    /**
     * @return \DateTimeImmutable
     */
    public function getOccurredOn(): \DateTimeImmutable
    {
        return $this->occurredOn;
    }
}
