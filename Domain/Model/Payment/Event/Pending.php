<?php

namespace Eplane\Payment\Domain\Model\Payment\Event;

use Eplane\Ddd\Domain\Event\DomainEventInterface;

class Pending implements DomainEventInterface
{
    /** @var string */
    private $paymentDataEncoded;
    /** @var string */
    private $paymentId;
    /**
     * @var \DateTimeImmutable
     */
    private $occurredOn;
    /** @var string */
    private $chatPaymentId;

    /**
     * Failed constructor.
     * @param string $paymentId
     * @param string $chatPaymentId
     * @param string $paymentData
     * @throws \Exception
     */
    public function __construct(string $paymentId, string $chatPaymentId, string $paymentDataEncoded)
    {
        $this->paymentId = $paymentId;
        $this->chatPaymentId = $chatPaymentId;
        $this->occurredOn = new \DateTimeImmutable();
        $this->paymentDataEncoded = $paymentDataEncoded;
    }

    /**
     * @return string
     */
    public function getPaymentId(): string
    {
        return $this->paymentId;
    }

    /**
     * @return string
     */
    public function getChatPaymentId(): string
    {
        return $this->chatPaymentId;
    }

    /**
     * @return \DateTimeImmutable
     */
    public function getOccurredOn(): \DateTimeImmutable
    {
        return $this->occurredOn;
    }

    /**
     * @return string
     */
    public function getPaymentDataEncoded(): string
    {
        return $this->paymentDataEncoded;
    }
}
