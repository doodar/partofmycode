<?php

namespace Eplane\Payment\Domain\Model\Payment\Event;

use Eplane\Ddd\Domain\Event\DomainEventInterface;

class FailedInternal implements DomainEventInterface
{
    /** @var string */
    private $paymentId;

    /** @var string */
    private $chatPaymentId;

    /** @var string */
    private $reason;

    /** @var \DateTimeImmutable */
    private $occurredOn;

    /**
     * Failed constructor.
     * @param string $paymentId
     * @param string $chatPaymentId
     * @param string $reason
     * @throws \Exception
     */
    public function __construct(string $paymentId, string $chatPaymentId, string $reason)
    {
        $this->paymentId = $paymentId;
        $this->chatPaymentId = $chatPaymentId;
        $this->reason = $reason;
        $this->occurredOn = new \DateTimeImmutable();
    }

    /**
     * @return string
     */
    public function getPaymentId(): string
    {
        return $this->paymentId;
    }

    /**
     * @return string
     */
    public function getChatPaymentId(): string
    {
        return $this->chatPaymentId;
    }

    /**
     * @return string
     */
    public function getReason(): string
    {
        return $this->reason;
    }

    /**
     * @return \DateTimeImmutable
     */
    public function getOccurredOn(): \DateTimeImmutable
    {
        return $this->occurredOn;
    }
}
