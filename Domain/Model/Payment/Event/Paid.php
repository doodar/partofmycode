<?php

namespace Eplane\Payment\Domain\Model\Payment\Event;

use Eplane\Ddd\Domain\Event\DomainEventInterface;

class Paid implements DomainEventInterface
{
    /** @var string */
    private $paymentId;
    /**
     * @var \DateTimeImmutable
     */
    private $occurredOn;
    /** @var string */
    private $chatPaymentId;

    /**
     * Failed constructor.
     * @param string $paymentId
     * @param string $chatPaymentId
     * @throws \Exception
     */
    public function __construct(string $paymentId, string $chatPaymentId)
    {
        $this->paymentId = $paymentId;
        $this->chatPaymentId = $chatPaymentId;
        $this->occurredOn = new \DateTimeImmutable();
    }

    /**
     * @return string
     */
    public function getPaymentId(): string
    {
        return $this->paymentId;
    }

    /**
     * @return string
     */
    public function getChatPaymentId(): string
    {
        return $this->chatPaymentId;
    }

    /**
     * @return \DateTimeImmutable
     */
    public function getOccurredOn(): \DateTimeImmutable
    {
        return $this->occurredOn;
    }
}
