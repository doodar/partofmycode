<?php

namespace Eplane\Payment\Domain\Model\Payment\Exception;

use Eplane\Payment\Domain\Exception\Exception;

class NoSuchPaymentException extends Exception
{
}
