<?php

namespace Eplane\Payment\Domain\Model\Payment;

use Eplane\Ddd\Domain\Contract\RepositoryInterface;

interface PaymentRepositoryInterface extends RepositoryInterface
{
    /**
     * @param Payment $aggregateRoot
     * @return $this
     */
    public function add($aggregateRoot);

    /**
     * @param PaymentId $id
     * @return Payment
     */
    public function get($id);

    /**
     * @return PaymentId
     */
    public function nextIdentity();

    /**
     * @param Payment $aggregateRoot
     * @return Payment
     */
    public function save($aggregateRoot);
}
