<?php
namespace Eplane\Payment\Domain\Model\Payment;

use Eplane\Base\Php\BigNumber\Decimal;
use Eplane\Ddd\Domain\Contract\AggregateRootInterface;
use Eplane\Ddd\Domain\Traits\AggregateTrait;
use Eplane\Payment\Domain\Model\Payment\Event\Created;
use Eplane\Payment\Domain\Model\Payment\Event\Failed;
use Eplane\Payment\Domain\Model\Payment\Event\FailedInternal;
use Eplane\Payment\Domain\Model\Payment\Event\Paid;
use Eplane\Payment\Domain\Model\Payment\Event\Pending;

class Payment implements AggregateRootInterface
{
    use AggregateTrait;

    /** @var PaymentId */
    private $id;
    /**
     * @var string
     */
    private $chatPaymentId;
    /**
     * @var string
     */
    private $requestId;
    /**
     * @var string
     */
    private $orderId;
    /**
     * @var string
     */
    private $paymentDataEncoded;
    /**
     * @var string
     */
    private $provider;
    /**
     * @var string
     */
    private $method;
    /**
     * @var string
     */
    private $sellerId;
    /**
     * @var string
     */
    private $buyerId;
    /**
     * @var string
     */
    private $customerId;
    /**
     * @var string
     */
    private $messageId;
    /**
     * @var Decimal
     */
    private $price;
    /**
     * @var string
     */
    private $currency;
    /**
     * @var Decimal
     */
    private $fee;
    /**
     * @var string
     */
    private $status;
    /**
     * @var bool
     */
    private $isPlatform;

    /**
     * Provider constructor.
     * @param PaymentId $id
     * @param string $chatPaymentId
     * @param string $requestId
     * @param string $orderId
     * @param string $paymentDataEncoded
     * @param string $provider
     * @param string $method
     * @param string $sellerId
     * @param string $buyerId
     * @param string $customerId
     * @param string $messageId
     * @param Decimal $price
     * @param string $currency
     * @param Decimal $fee
     * @param string $status
     * @param bool $isPlatform
     */
    public function __construct(
        PaymentId $id,
        string $chatPaymentId,
        string $requestId,
        string $orderId,
        string $paymentDataEncoded,
        string $provider,
        string $method,
        string $sellerId,
        string $buyerId,
        string $customerId,
        string $messageId,
        Decimal $price,
        string $currency,
        Decimal $fee,
        string $status,
        bool $isPlatform = false
    )
    {
        $this->id = $id;
        $this->chatPaymentId = $chatPaymentId;
        $this->requestId = $requestId;
        $this->orderId = $orderId;
        $this->paymentDataEncoded = $paymentDataEncoded;
        $this->provider = $provider;
        $this->method = $method;
        $this->sellerId = $sellerId;
        $this->buyerId = $buyerId;
        $this->customerId = $customerId;
        $this->messageId = $messageId;
        $this->price = $price;
        $this->currency = $currency;
        $this->fee = $fee;
        $this->status = $status;
        $this->isPlatform = $isPlatform;
    }

    /**
     * @return PaymentId
     */
    public function getId(): PaymentId
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getChatPaymentId(): string
    {
        return $this->chatPaymentId;
    }

    /**
     * @return string
     */
    public function getRequestId(): string
    {
        return $this->requestId;
    }

    /**
     * @return string
     */
    public function getOrderId(): string
    {
        return $this->orderId;
    }

    /**
     * @return string
     */
    public function getPaymentDataEncoded(): string
    {
        return $this->paymentDataEncoded;
    }

    /**
     * @return string
     */
    public function getProvider(): string
    {
        return $this->provider;
    }

    /**
     * @return string
     */
    public function getMethod(): string
    {
        return $this->method;
    }

    /**
     * @return string
     */
    public function getSellerId(): string
    {
        return $this->sellerId;
    }

    /**
     * @return string
     */
    public function getBuyerId(): string
    {
        return $this->buyerId;
    }

    /**
     * @return string
     */
    public function getCustomerId(): string
    {
        return $this->customerId;
    }

    /**
     * @return string
     */
    public function getMessageId(): string
    {
        return $this->messageId;
    }

    /**
     * @return Decimal
     */
    public function getPrice(): Decimal
    {
        return $this->price;
    }

    /**
     * @return string
     */
    public function getCurrency(): string
    {
        return $this->currency;
    }

    /**
     * @return Decimal
     */
    public function getFee(): Decimal
    {
        return $this->fee;
    }

    /**
     * @return string
     */
    public function getStatus(): string
    {
        return $this->status;
    }

    /**
     * @return bool
     */
    public function getIsPlatform(): bool
    {
        return $this->isPlatform;
    }

    public function isPlatformEnable()
    {
        $this->isPlatform = true;
    }

    public function isPlatformDisable()
    {
        $this->isPlatform = false;
    }

    /**
     * @param string $reason
     * @throws \Exception
     */
    public function failed(string $reason)
    {
        $this->status = Status::FAILED;
        $this->recordEvent(
            new Failed(
                $this->getId()->getValue(),
                $this->getChatPaymentId(),
                $reason
            )
        );
    }

    /**
     * @throws \Exception
     */
    public function paid()
    {
        $this->status = Status::PAID;
        $this->recordEvent(
            new Paid(
                $this->getId()->getValue(),
                $this->getChatPaymentId()
            )
        );
    }

    /**
     * @throws \Exception
     */
    public function pending(string $paymentDataUpdateEncoded)
    {
        $this->mergePaymentData($paymentDataUpdateEncoded);

        $this->status = Status::PENDING;
        $this->recordEvent(
            new Pending(
                $this->getId()->getValue(),
                $this->getChatPaymentId(),
                $this->paymentDataEncoded
            )
        );
    }

    /**
     * @param string $paymentDataUpdateEncoded
     */
    public function mergePaymentData(string $paymentDataUpdateEncoded)
    {
        $paymentDataUpdate = \json_decode($paymentDataUpdateEncoded, true);
        if (empty($paymentDataUpdate)) {
            return;
        }

        $paymentData = \json_decode($this->paymentDataEncoded, true) ?: [];
        $paymentData = \array_merge_recursive($paymentData, $paymentDataUpdate);

        $this->paymentDataEncoded = $paymentData ? \json_encode($paymentData) : '{}';
    }

    /**
     * @param PaymentId $id
     * @param string $chatPaymentId
     * @param string $requestId
     * @param string $orderId
     * @param string $paymentDataEncoded
     * @param string $provider
     * @param string $method
     * @param string $sellerId
     * @param string $buyerId
     * @param string $customerId
     * @param string $messageId
     * @param Decimal $price
     * @param string $currency
     * @param Decimal $fee
     * @param bool $isPlatform
     * @return static
     * @throws \Exception
     */
    public static function createCreated(
        PaymentId $id,
        string $chatPaymentId,
        string $requestId,
        string $orderId,
        string $paymentDataEncoded,
        string $provider,
        string $method,
        string $sellerId,
        string $buyerId,
        string $customerId,
        string $messageId,
        Decimal $price,
        string $currency,
        Decimal $fee,
        bool $isPlatform
    ): self {
        $instance = new self(
            $id,
            $chatPaymentId,
            $requestId,
            $orderId,
            $paymentDataEncoded,
            $provider,
            $method,
            $sellerId,
            $buyerId,
            $customerId,
            $messageId,
            $price,
            $currency,
            $fee,
            Status::CREATED,
            $isPlatform
        );

        $instance->recordEvent(
            new Created(
                $instance->getId()->getValue(),
                $instance->getChatPaymentId(),
                $instance->getProvider()
            )
        );

        return $instance;
    }

    /**
     * @param PaymentId $id
     * @param string $chatPaymentId
     * @param string $requestId
     * @param string $orderId
     * @param string $paymentDataEncoded
     * @param string $provider
     * @param string $method
     * @param string $sellerId
     * @param string $buyerId
     * @param string $customerId
     * @param string $messageId
     * @param Decimal $price
     * @param string $currency
     * @param Decimal $fee
     * @param string $reason
     * @return static
     * @throws \Exception
     */
    public static function createFailed(
        PaymentId $id,
        string $chatPaymentId,
        string $requestId,
        string $orderId,
        string $paymentDataEncoded,
        string $provider,
        string $method,
        string $sellerId,
        string $buyerId,
        string $customerId,
        string $messageId,
        Decimal $price,
        string $currency,
        Decimal $fee,
        string $reason
    ): self {
        $instance = new self(
            $id,
            $chatPaymentId,
            $requestId,
            $orderId,
            $paymentDataEncoded,
            $provider,
            $method,
            $sellerId,
            $buyerId,
            $customerId,
            $messageId,
            $price,
            $currency,
            $fee,
            Status::FAILED
        );

        $instance->recordEvent(
            new FailedInternal(
                $instance->getId()->getValue(),
                $instance->getChatPaymentId(),
                $reason
            )
        );

        return $instance;
    }
}
