<?php

namespace Eplane\Payment\Domain\Model\Payment;

use Eplane\Ddd\Domain\Contract\ValueObjectInterface;
use Eplane\Ddd\Domain\Traits\EqualsTrait;

class Status implements ValueObjectInterface
{
    use EqualsTrait;

    const CREATED = 'created';
    const PENDING = 'pending';
    const FAILED  = 'failed';
    const PAID    = 'paid';

    public function getValue()
    {
        // TODO: Implement getValue() method.
    }
}