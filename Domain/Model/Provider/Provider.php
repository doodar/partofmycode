<?php

namespace Eplane\Payment\Domain\Model\Provider;

use Eplane\Ddd\Domain\Contract\AggregateRootInterface;
use Eplane\Ddd\Domain\Traits\AggregateTrait;
use Eplane\Payment\Domain\Model\Method\Method;
use Eplane\Payment\Domain\Model\Method\MethodId;
use Eplane\Payment\Domain\Model\Provider\Exception\ProviderAlreadyDisabledException;
use Eplane\Payment\Domain\Model\Provider\Exception\ProviderAlreadyEnabledException;
use Eplane\Payment\Domain\ValueObject\Fee;

class Provider implements AggregateRootInterface
{
    use AggregateTrait;

    /** @var ProviderId */
    private $id;

    /** @var string */
    private $code;

    /** @var string */
    private $title;

    /** @var bool */
    private $enabled;

    /** @var Fee */
    private $defaultFee;

    /** @var MethodId[] */
    private $methodIds;

    /**
     * Provider constructor.
     * @param ProviderId $id
     * @param string $code
     * @param string $title
     * @param bool $enabled
     * @param Fee $defaultFee
     */
    public function __construct(ProviderId $id, string $code, string $title, bool $enabled, Fee $defaultFee)
    {
        $this->id = $id;
        $this->code = $code;
        $this->title = $title;
        $this->methodIds = [];
        $this->enabled = $enabled;
        $this->defaultFee = $defaultFee;
    }

    public function addMethod(Method $method)
    {
        $this->methodIds[$method->getId()->getValue()] = $method->getId();
    }

    public function removeMethod(MethodId $methodId)
    {
        unset($this->methodIds[$methodId->getValue()]);
    }

    public function enable()
    {
        if ($this->isEnabled()) {
            throw new ProviderAlreadyEnabledException();
        }

        $this->enabled = true;
    }

    public function disable()
    {
        if (!$this->isEnabled()) {
            throw new ProviderAlreadyDisabledException();
        }

        $this->enabled = false;
    }

    public function changeTitle(string $title)
    {
        $this->title = $title;
    }

    public function changeDefaultFee(Fee $newValue)
    {
        $this->defaultFee = $newValue;
    }

    /**
     * @return ProviderId
     */
    public function getId(): ProviderId
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getCode(): string
    {
        return $this->code;
    }

    /**
     * @return string
     */
    public function getTitle(): string
    {
        return $this->title;
    }

    /**
     * @return bool
     */
    public function isEnabled(): bool
    {
        return $this->enabled;
    }

    /**
     * @return Fee
     */
    public function getDefaultFee(): Fee
    {
        return $this->defaultFee;
    }

    /**
     * @return MethodId[]
     */
    public function getMethodIds(): array
    {
        return $this->methodIds;
    }
}
