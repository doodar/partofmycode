<?php

namespace Eplane\Payment\Domain\Model\Provider\Exception;

use Eplane\Payment\Domain\Exception\Exception;

class NoSuchProviderException extends Exception
{
    const MESSAGE_SINGLE_FIELD = 'No such provider with %1$s = %2$s';
    const MESSAGE_DOUBLE_FIELDS = 'No such provider with %1$s = %2$s, %3$s = %4$s';
}
