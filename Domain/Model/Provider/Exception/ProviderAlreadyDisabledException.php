<?php

namespace Eplane\Payment\Domain\Model\Provider\Exception;

use Eplane\Payment\Domain\Exception\Exception;

class ProviderAlreadyDisabledException extends Exception
{
}
