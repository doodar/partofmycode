<?php

namespace Eplane\Payment\Domain\Model\Provider;

interface ProviderRepositoryInterface
{
    /**
     * @param Provider $aggregateRoot
     * @return $this
     */
    public function add($aggregateRoot);

    /**
     * @param ProviderId $id
     * @return Provider
     */
    public function get(ProviderId $id): Provider;

    /**
     * @param string $providerCode
     * @return Provider
     */
    public function getByCode($providerCode): Provider;

    /**
     * @return Provider[]
     */
    public function getAll();

    /**
     * @return ProviderId
     */
    public function nextIdentity();

    /**
     * @param Provider $aggregateRoot
     * @return Provider
     */
    public function save($aggregateRoot);
}
