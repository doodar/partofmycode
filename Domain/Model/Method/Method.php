<?php

namespace Eplane\Payment\Domain\Model\Method;

use Eplane\Ddd\Domain\Contract\AggregateRootInterface;
use Eplane\Ddd\Domain\Traits\AggregateTrait;
use Eplane\Payment\Domain\Model\Provider\ProviderId;

class Method  implements AggregateRootInterface
{
    use AggregateTrait;

    /** @var MethodId */
    private $id;

    /** @var string */
    private $code;

    /** @var string */
    private $title;

    /** @var ProviderId */
    private $providerId;

    /** @var bool */
    private $enabled;

    /** @var bool */
    private $platformEnabled;

    /**
     * Method constructor.
     * @param MethodId $id
     * @param string $code
     * @param string $title
     * @param ProviderId $providerId
     * @throws \Exception
     */
    public function __construct(
        MethodId $id,
        string $code,
        string $title,
        ProviderId $providerId
    )
    {
        $this->id = $id;
        $this->code = $code;
        $this->title = $title;
        $this->providerId = $providerId;
        $this->enabled = true;
        $this->platformEnabled = false;
    }

    public function enable()
    {
        $this->enabled = true;
    }

    public function disable()
    {
        $this->enabled = false;
    }

    public function changeTitle(string $title)
    {
        $this->title = $title;
    }

    /**
     * @return MethodId
     */
    public function getId(): MethodId
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getCode(): string
    {
        return $this->code;
    }

    /**
     * @return string
     */
    public function getTitle(): string
    {
        return $this->title;
    }

    /**
     * @return bool
     */
    public function isEnabled(): bool
    {
        return $this->enabled;
    }

    /**
     * @return ProviderId
     */
    public function getProviderId(): ProviderId
    {
        return $this->providerId;
    }

    /**
     * @return bool
     */
    public function isPlatformEnabled(): bool
    {
        return $this->platformEnabled ?? false;
    }

    public function platformEnable()
    {
        $this->platformEnabled = true;
    }

    public function platformDisable()
    {
        $this->platformEnabled = false;
    }
}
