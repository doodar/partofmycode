<?php

namespace Eplane\Payment\Domain\Model\Method\Exception;

use Eplane\Payment\Domain\Exception\Exception;

class NoSuchMethodException extends Exception
{
    const MESSAGE_SINGLE_FIELD = 'No such method with %1$s = %2$s';
    const MESSAGE_DOUBLE_FIELDS = 'No such method with %1$s = %2$s, %3$s = %4$s';
}
