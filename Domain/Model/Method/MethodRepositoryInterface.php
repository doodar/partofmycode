<?php

namespace Eplane\Payment\Domain\Model\Method;

use Eplane\Ddd\Domain\Contract\RepositoryInterface;
use Eplane\Payment\Domain\Model\Provider\ProviderId;

interface MethodRepositoryInterface extends RepositoryInterface
{
    /**
     * @param Method $aggregateRoot
     * @return $this
     */
    public function add($aggregateRoot);

    /**
     * @param MethodId $id
     * @return Method
     */
    public function get($id);

    /**
     * @param ProviderId $providerId
     * @return Method[]
     */
    public function getByProviderId($providerId);

    /**
     * @param ProviderId $providerId
     * @param string $code
     * @return Method|bool
     * @throws \Exception
     */
    public function getByProviderAndCode($providerId, $code);

    /**
     * @return Method[]
     */
    public function getAll();

    /**
     * @return MethodId
     */
    public function nextIdentity();


    /**
     * @param Method $aggregateRoot
     * @return Method
     */
    public function save($aggregateRoot);

    /**
     * @return bool
     */
    public function hasPlatformPaymentMethods();
}
