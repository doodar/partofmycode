<?php
namespace Eplane\Payment\Domain\Model\PaymentRequest\Event;

use Eplane\Ddd\Domain\Event\DomainEventInterface;
use Eplane\Payment\Domain\Model\PaymentRequest\PaymentRequestId;

class Rejected implements DomainEventInterface
{
    /** @var PaymentRequestId */
    private $requestId;

    /** @var string */
    private $chatRequestId;

    /** @var string */
    private $rfqIncrementId;

    /** @var string */
    private $reason;

    /** @var \DateTimeImmutable */
    private $occurredOn;

    /** @noinspection PhpDocMissingThrowsInspection */
    /**
     * Confirmed constructor.
     * @param PaymentRequestId $requestId
     * @param string $chatRequestId
     * @param string $rfqIncrementId
     * @param string $reason
     */
    public function __construct(
        PaymentRequestId $requestId,
        string $chatRequestId,
        string $rfqIncrementId,
        string $reason
    ) {
        $this->requestId      = $requestId;
        $this->chatRequestId  = $chatRequestId;
        $this->rfqIncrementId = $rfqIncrementId;
        $this->reason         = $reason;

        $this->occurredOn = new \DateTimeImmutable();
    }

    /**
     * @return PaymentRequestId
     */
    public function getRequestId(): PaymentRequestId
    {
        return $this->requestId;
    }

    /**
     * @return string
     */
    public function getChatRequestId(): string
    {
        return $this->chatRequestId;
    }

    /**
     * @return string
     */
    public function getRfqIncrementId(): string
    {
        return $this->rfqIncrementId;
    }

    /**
     * @return string
     */
    public function getReason(): string
    {
        return $this->reason;
    }

    /**
     * @return \DateTimeImmutable
     */
    public function getOccurredOn(): \DateTimeImmutable
    {
        return $this->occurredOn;
    }
}
