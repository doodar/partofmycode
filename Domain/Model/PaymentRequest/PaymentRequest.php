<?php
namespace Eplane\Payment\Domain\Model\PaymentRequest;

use Eplane\Ddd\Domain\Contract\AggregateRootInterface;
use Eplane\Ddd\Domain\Model\Money;
use Eplane\Ddd\Domain\Traits\AggregateTrait;
use Eplane\Payment\Domain\Model\PaymentRequest\Event\Confirmed;
use Eplane\Payment\Domain\Model\PaymentRequest\Event\Rejected;
use Eplane\Payment\Domain\Model\User\UserId;

class PaymentRequest implements AggregateRootInterface
{
    use AggregateTrait;

    /** @var PaymentRequestId */
    private $id;

    /** @var string */
    private $chatRequestId;

    /** @var string */
    private $rfqIncrementId;

    /** @var UserId */
    private $userId;

    /** @var string */
    private $customerId;

    /** @var Money */
    private $total;

    /** @var Status */
    private $status;

    /** @var string|null */
    private $rejectReason;

    /**
     * @param PaymentRequestId $id
     * @param string $chatRequestId
     * @param string $rfqIncrementId
     * @param UserId $userId
     * @param string $customerId
     * @param Money $total
     */
    private function __construct(
        PaymentRequestId $id,
        string $chatRequestId,
        string $rfqIncrementId,
        UserId $userId,
        string $customerId,
        Money $total
    ) {
        $this->id             = $id;
        $this->chatRequestId  = $chatRequestId;
        $this->rfqIncrementId = $rfqIncrementId;
        $this->userId         = $userId;
        $this->customerId     = $customerId;
        $this->total          = $total;
    }

    public static function createConfirmed(
        PaymentRequestId $id,
        string $chatRequestId,
        string $rfqIncrementId,
        UserId $userId,
        string $customerId,
        Money $total
    ): self {
        $instance = new self(
            $id,
            $chatRequestId,
            $rfqIncrementId,
            $userId,
            $customerId,
            $total
        );

        $instance->status = new Status(Status::CONFIRMED);

        $instance->recordEvent(
            new Confirmed(
                $instance->getId(),
                $instance->getChatRequestId(),
                $instance->getRfqIncrementId()
            )
        );

        return $instance;
    }

    public static function createRejected(
        PaymentRequestId $id,
        string $chatRequestId,
        string $rfqIncrementId,
        UserId $userId,
        string $customerId,
        Money $total,
        string $reason
    ): self {
        $instance = new self(
            $id,
            $chatRequestId,
            $rfqIncrementId,
            $userId,
            $customerId,
            $total
        );

        $instance->status = new Status(Status::CONFIRMED);
        $instance->rejectReason = $reason;

        $instance->recordEvent(
            new Rejected(
                $instance->getId(),
                $instance->getChatRequestId(),
                $instance->getRfqIncrementId(),
                $instance->getRejectReason()
            )
        );

        $instance->status = new Status(Status::CONFIRMED);

        return $instance;
    }

    /**
     * @return PaymentRequestId
     */
    public function getId(): PaymentRequestId
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getChatRequestId(): string
    {
        return $this->chatRequestId;
    }

    /**
     * @return string
     */
    public function getRfqIncrementId(): string
    {
        return $this->rfqIncrementId;
    }

    /**
     * @return UserId
     */
    public function getUserId(): UserId
    {
        return $this->userId;
    }

    /**
     * @return string
     */
    public function getCustomerId(): string
    {
        return $this->customerId;
    }

    /**
     * @return Money
     */
    public function getTotal(): Money
    {
        return $this->total;
    }

    /**
     * @return Status
     */
    public function getStatus(): Status
    {
        return $this->status;
    }

    /**
     * @return string|null
     */
    public function getRejectReason(): string
    {
        return $this->rejectReason;
    }
}
