<?php
namespace Eplane\Payment\Domain\Model\PaymentRequest;

use Eplane\Payment\Domain\Model\PaymentRequest\Exception\NoSuchPaymentRequestException;

interface PaymentRequestRepositoryInterface
{
    /**
     * @return PaymentRequestId
     */
    public function nextIdentity();

    /**
     * @param PaymentRequest $aggregateRoot
     * @return PaymentRequest
     */
    public function add(PaymentRequest $aggregateRoot);

    /**
     * @param PaymentRequestId $id
     * @return PaymentRequest
     * @throws NoSuchPaymentRequestException
     */
    public function get(PaymentRequestId $id): PaymentRequest;

    /**
     * @param string $chatRequestId
     * @return PaymentRequest
     * @throws NoSuchPaymentRequestException
     */
    public function getByChatRequestId(string $chatRequestId): PaymentRequest;

    /**
     * @param PaymentRequest $aggregateRoot
     * @return PaymentRequest
     */
    public function save(PaymentRequest $aggregateRoot);
}
