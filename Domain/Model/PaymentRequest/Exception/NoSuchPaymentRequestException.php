<?php
namespace Eplane\Payment\Domain\Model\PaymentRequest\Exception;

use Eplane\Payment\Domain\Exception\Exception;

class NoSuchPaymentRequestException extends Exception
{
}
