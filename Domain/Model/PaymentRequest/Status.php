<?php
namespace Eplane\Payment\Domain\Model\PaymentRequest;

use Eplane\Ddd\Domain\Contract\ValueObjectInterface;
use Eplane\Ddd\Domain\Traits\EqualsTrait;

class Status implements ValueObjectInterface
{
    use EqualsTrait;

    const CONFIRMED = 'confirmed';
    const REJECTED = 'rejected';
    const COMPLETED = 'completed';
    const FAILED = 'failed';
    const PROCESSING = 'processing';
    const EXPIRED = 'expired';

    /** @var string */
    private $value;

    public function __construct(string $value)
    {

        $this->value = $value;
    }

    public function getValue(): string
    {
        return $this->value;
    }
}
