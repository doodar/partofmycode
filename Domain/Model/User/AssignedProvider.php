<?php

namespace Eplane\Payment\Domain\Model\User;

use Eplane\Ddd\Domain\Contract\ValueObjectInterface;
use Eplane\Ddd\Domain\Traits\EqualsTrait;
use Eplane\Payment\Domain\Model\Provider\ProviderId;
use Eplane\Payment\Domain\ValueObject\Fee;

class AssignedProvider implements ValueObjectInterface
{
    use EqualsTrait;

    /** @var ProviderId */
    private $providerId;

    /** @var bool */
    private $isAssigned;

    /** @var \DateTime */
    private $lastModifyAt;

    /** @var Fee */
    private $fee;

    /**
     * AssignedProvider constructor.
     * @param ProviderId $providerId
     * @param $isAssigned
     * @param Fee $fee
     * @param \DateTime|null $lastModify
     * @throws \Exception
     */
    public function __construct(ProviderId $providerId, bool $isAssigned, Fee $fee, $lastModify)
    {
        $this->providerId   = $providerId;
        $this->isAssigned   = $isAssigned;
        $this->fee          = $fee;
        $this->lastModifyAt = $lastModify;
    }

    public function getProviderId(){
        return $this->providerId;
    }

    public function getIsAssigned(){
        return $this->isAssigned;
    }

    public function getFee(){
        return $this->fee;
    }

    public function getLastModifyAt(){
        return $this->lastModifyAt;
    }
}
