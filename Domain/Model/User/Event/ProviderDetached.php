<?php
namespace Eplane\Payment\Domain\Model\User\Event;

use Eplane\Ddd\Domain\Event\DomainEventInterface;
use Eplane\Payment\Domain\Model\Provider\ProviderId;
use Eplane\Payment\Domain\Model\User\UserId;

class ProviderDetached implements DomainEventInterface
{
    /** @var UserId */
    private $userId;

    /** @var ProviderId */
    private $providerId;

    /** @var \DateTimeImmutable */
    private $occurredOn;

    /** @noinspection PhpDocMissingThrowsInspection */
    /**
     * @param UserId $userId
     * @param ProviderId $providerId
     * @throws \Exception
     */
    public function __construct(UserId $userId, ProviderId $providerId)
    {
        $this->userId     = $userId;
        $this->providerId = $providerId;

        $this->occurredOn = new \DateTimeImmutable();
    }

    /**
     * @return \DateTimeInterface
     */
    public function getOccurredOn()
    {
        return $this->occurredOn;
    }

    /**
     * @return UserId
     */
    public function getUserId(): UserId
    {
        return $this->userId;
    }

    /**
     * @return ProviderId
     */
    public function getProviderId(): ProviderId
    {
        return $this->providerId;
    }
}
