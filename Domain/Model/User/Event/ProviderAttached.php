<?php
namespace Eplane\Payment\Domain\Model\User\Event;

use Eplane\Ddd\Domain\Event\DomainEventInterface;
use Eplane\Payment\Domain\Model\User\UserId;

class ProviderAttached implements DomainEventInterface
{
    /** @var UserId */
    private $userId;

    /** @var string */
    private $providerCode;

    /** @var \DateTimeImmutable */
    private $occurredOn;

    /** @noinspection PhpDocMissingThrowsInspection */
    /**
     * @param UserId $userId
     * @param string $providerCode
     * @throws \Exception
     */
    public function __construct(UserId $userId, string $providerCode)
    {
        $this->userId = $userId;
        $this->providerCode = $providerCode;

        $this->occurredOn = new \DateTimeImmutable();
    }

    /**
     * @return \DateTimeInterface
     */
    public function getOccurredOn()
    {
        return $this->occurredOn;
    }

    /**
     * @return UserId
     */
    public function getUserId(): UserId
    {
        return $this->userId;
    }

    /**
     * @return string
     */
    public function getProviderCode(): string
    {
        return $this->providerCode;
    }
}
