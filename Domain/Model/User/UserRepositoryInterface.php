<?php

namespace Eplane\Payment\Domain\Model\User;

use Eplane\Ddd\Domain\Contract\AggregateRootInterface;
use Eplane\Ddd\Domain\Contract\IdInterface;
use Eplane\Ddd\Domain\Contract\RepositoryInterface;

interface UserRepositoryInterface
{
    /**
     * @param User $aggregateRoot
     * @return $this
     */
    public function add($aggregateRoot);

    /**
     * @param UserId $id
     * @return User
     */
    public function get($id);

    /**
     * @param User $aggregateRoot
     * @return User
     */
    public function save($aggregateRoot);
}
