<?php

namespace Eplane\Payment\Domain\Model\User;

use DateTime;
use Eplane\Ddd\Domain\Contract\AggregateRootInterface;
use Eplane\Ddd\Domain\Traits\AggregateTrait;
use Eplane\Payment\Domain\Model\Method\Method;
use Eplane\Payment\Domain\Model\Method\MethodId;
use Eplane\Payment\Domain\Model\Provider\Provider;
use Eplane\Payment\Domain\Model\Provider\ProviderId;
use Eplane\Payment\Domain\Model\User\Event\ProviderAssigned;
use Eplane\Payment\Domain\Model\User\Event\ProviderAttached;
use Eplane\Payment\Domain\Model\User\Event\ProviderDetached;
use Eplane\Payment\Domain\Model\User\Exception\MethodAlreadyAssignedException;
use Eplane\Payment\Domain\Model\User\Exception\MethodNotAssignedException;
use Eplane\Payment\Domain\Model\User\Exception\ProviderAlreadyAssignedException;
use Eplane\Payment\Domain\Model\User\Exception\ProviderNotAssignedException;
use Eplane\Payment\Domain\ValueObject\Fee;
use Exception;

class User implements AggregateRootInterface
{
    use AggregateTrait;

    /** @var UserId */
    private $id;

    /** @var bool */
    private $isActive;

    /** @var AssignedProvider[] */
    private $assignedProviders;

    /** @var AssignedMethod[] */
    private $assignedMethods;

    /** @var DateTime */
    private $createdAt;

    /** @var DateTime */
    private $lastModifyAt;

    /** @var bool */
    private $platformPay;

    /**
     * User constructor.
     * @param UserId $id
     * @param DateTime|null $lastModify
     * @param DateTime|null $createdAt
     * @throws Exception
     */
    public function __construct(UserId $id, DateTime $lastModify = null, DateTime $createdAt = null)
    {
        $this->id                  = $id;
        $this->isActive            = true;
        $this->assignedProviders   = [];
        $this->assignedMethods     = [];
        $this->lastModifyAt        = $lastModify ?? new DateTime();
        $this->createdAt           = $createdAt ?? new DateTime();
        $this->platformPay = false;
    }

    public function isProviderAssigned(ProviderId $providerId)
    {
        return (
            isset($this->assignedProviders[$providerId->getValue()])
            && ($this->assignedProviders[$providerId->getValue()])->getIsAssigned()
        );
    }

    /**
     * @param Provider $provider
     * @param Method[] $providerMethods
     * @param DateTime $connectedAt
     * @throws ProviderAlreadyAssignedException
     * @throws Exception
     */
    public function assignProvider(Provider $provider, DateTime $connectedAt = null, Method ...$providerMethods)
    {
        if ($this->isProviderAssigned($provider->getId())) {
            throw new ProviderAlreadyAssignedException(
                sprintf(
                    'Provider id = %s is assigned to user %s already',
                    $provider->getId()->getValue(),
                    $this->id->getValue()
                )
            );
        }

        $this->assignedProviders[$provider->getId()->getValue()] = new AssignedProvider(
            $provider->getId(),
            true,
            (isset($this->assignedProviders[$provider->getId()->getValue()])
                ? ($this->assignedProviders[$provider->getId()->getValue()])->getFee()
                : new Fee(null, false, '')
            ),
            $connectedAt
        );

        foreach ($providerMethods as $method) {
            $this->assignMethod($method);
        }

        $this->recordEvent(new ProviderAssigned($this->getId(), $provider->getId()));
    }

    /**
     * @param ProviderId $providerId
     * @param DateTime $disconnectedAt
     * @throws ProviderNotAssignedException
     * @throws Exception
     */
    public function detachProvider(ProviderId $providerId, DateTime $disconnectedAt = null)
    {
        if (!$this->isProviderAssigned($providerId)) {
            throw new ProviderNotAssignedException(
                sprintf('Provider id = %s is not assigned to user %s', $providerId->getValue(), $this->id->getValue())
            );
        }

        $this->assignedProviders[$providerId->getValue()] = new AssignedProvider(
            $providerId,
            false,
            ($this->assignedProviders[$providerId->getValue()])->getFee(),
            $disconnectedAt
        );

        foreach ($this->getAssignedMethods() as $method) {
            if ($method->getIsAssigned() && $method->getProviderId()->equals($providerId)) {
                $this->detachMethod($method->getMethodId());
            }
        }

        $this->recordEvent(new ProviderDetached($this->getId(), $providerId));
    }

    public function activate()
    {
        $this->isActive     = true;
        $this->lastModifyAt = new DateTime();
    }

    public function deactivate()
    {
        $this->isActive     = false;
        $this->lastModifyAt = new DateTime();
    }

    public function isMethodAssigned(MethodId $methodId)
    {
        return (
            isset($this->assignedMethods[$methodId->getValue()])
            && ($this->assignedMethods[$methodId->getValue()])->getIsAssigned()
        );
    }

    /**
     * @param Method $method
     * @throws MethodAlreadyAssignedException
     * @throws ProviderNotAssignedException
     */
    public function assignMethod(Method $method)
    {
        if (!$this->isProviderAssigned($method->getProviderId())) {
            throw new ProviderNotAssignedException(
                sprintf(
                    'Method id = %s can not be assigned to user %s because provider id = %s is not assigned yet',
                    $method->getId()->getValue(),
                    $this->id->getValue(),
                    $method->getProviderId()->getValue()
                )
            );
        }

        $this->assignedMethods[$method->getId()->getValue()] = new AssignedMethod(
            $method->getProviderId(),
            $method->getId(),
            true
        );
    }

    /**
     * @param MethodId $methodId
     * @throws MethodNotAssignedException
     */
    public function detachMethod(MethodId $methodId)
    {
        if (!$this->isMethodAssigned($methodId)) {
            throw new MethodNotAssignedException(
                sprintf('Method id = %s is not assigned to user %s', $methodId->getValue(), $this->id->getValue())
            );
        }

        $this->assignedMethods[$methodId->getValue()] = new AssignedMethod(
            ($this->assignedMethods[$methodId->getValue()])->getProviderId(),
            $methodId,
            false
        );
    }

    /**
     * @param ProviderId $providerId
     * @param Fee $fee
     * @param string $providerCode
     * @throws Exception
     */
    public function setFeeToProvider(ProviderId $providerId, Fee $fee, string $providerCode)
    {
        $this->attachProvider($providerId, $providerCode);

        $this->assignedProviders[$providerId->getValue()] = new AssignedProvider(
            $providerId,
            ($this->assignedProviders[$providerId->getValue()])->getIsAssigned(),
            $fee,
            ($this->assignedProviders[$providerId->getValue()])->getLastModifyAt()
        );
    }

    /**
     * @param ProviderId $providerId
     * @return Fee|null
     */
    public function getProviderSpecificFee(ProviderId $providerId)
    {
        if (!$this->isProviderAssigned($providerId)) {
            return null;
        }

        $assignedProvider = $this->assignedProviders[$providerId->getValue()];
        $fee = $assignedProvider->getFee();

        return $fee->getAmount() !== null ? $fee : null;
    }

    public function canReceivePayments(): bool
    {
        foreach ($this->assignedMethods as $assignedMethod) {
            if ($assignedMethod->getIsAssigned()
                && $this->isProviderAssigned($assignedMethod->getProviderId())
            ) {
                return true;
            }
        }

        return false;
    }

    public function getId(): UserId
    {
        return $this->id;
    }

    public function getIsActive()
    {
        return $this->isActive;
    }

    /**
     * @return AssignedProvider[]
     */
    public function getAssignedProviders()
    {
        return $this->assignedProviders;
    }

    public function getAssignedMethods()
    {
        return $this->assignedMethods;
    }

    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    public function getLastModifyAt()
    {
        return $this->lastModifyAt;
    }

    public function getPlatformPay()
    {
        return $this->platformPay;
    }

    /** @param bool $platformPay */
    public function setPlatformPay($platformPay)
    {
        $this->platformPay = $platformPay;
    }

    /**
     * @param ProviderId $providerId
     * @param string $providerCode
     * @throws Exception
     */
    public function attachProvider(ProviderId $providerId, $providerCode)
    {
        if(!isset($this->assignedProviders[$providerId->getValue()])){
            $this->assignedProviders[$providerId->getValue()] = new AssignedProvider(
                $providerId,
                false,
                new Fee(null, false, ''),
                null
            );

            $this->recordEvent(new ProviderAttached($this->getId(), $providerCode));
        }
    }
}
