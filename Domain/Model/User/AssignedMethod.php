<?php

namespace Eplane\Payment\Domain\Model\User;

use Eplane\Ddd\Domain\Contract\ValueObjectInterface;
use Eplane\Ddd\Domain\Traits\EqualsTrait;
use Eplane\Payment\Domain\Model\Method\MethodId;
use Eplane\Payment\Domain\Model\Provider\ProviderId;

class AssignedMethod implements ValueObjectInterface
{
    use EqualsTrait;

    /** @var ProviderId */
    private $providerId;

    /** @var MethodId */
    private $methodId;

    /** @var bool */
    private $isAssigned;

    /** @var \DateTime */
    private $lastModifyAt;

    /**
     * AssignedMethod constructor.
     * @param ProviderId $providerId
     * @param MethodId $methodId
     * @param $isAssigned
     * @param $lastModify
     */
    public function __construct(ProviderId $providerId, MethodId $methodId, bool $isAssigned, \DateTime $lastModify = null)
    {
        $this->providerId   = $providerId;
        $this->methodId     = $methodId;
        $this->isAssigned   = $isAssigned;
        $this->lastModifyAt = $lastModify ?? new \DateTime();
    }

    public function getProviderId(){
        return $this->providerId;
    }

    public function getMethodId(){
        return $this->methodId;
    }

    public function getIsAssigned(){
        return $this->isAssigned;
    }

    public function getLastModifyAt(){
        return $this->lastModifyAt;
    }
}
