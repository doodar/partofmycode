<?php

namespace Eplane\Payment\Domain\Model\User\Exception;

use Eplane\Payment\Domain\Exception\Exception;

class NoSuchUserException extends Exception
{
    const MESSAGE_SINGLE_FIELD = 'No such user with %1$s = %2$s';
    const MESSAGE_DOUBLE_FIELDS = 'No such user with %1$s = %2$s, %3$s = %4$s';
}
