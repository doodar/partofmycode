<?php

namespace Eplane\Payment\Domain\Model\User\Exception;

use Eplane\Payment\Domain\Exception\Exception;

class ProviderAlreadyAssignedException extends Exception
{

}
